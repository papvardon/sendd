import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sendd/utils/custColors.dart';

class ImagePreviewPage extends StatelessWidget{
  ImagePreviewPage({Key key, this.imageFile}) : super(key: key);

  final File imageFile;
  final TextEditingController _textController = TextEditingController();
//  final PickedFile imageFile;
//  final dynamic pickImageError;
//  String retrieveDataError;

  void _submit(BuildContext context){
    if(_textController.text.isEmpty){
      Navigator.pop(context, '1001');
    }
    else{
      Navigator.pop(context, _textController.text);
    }

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          "Preview",
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, '1002');
          },
        ),
      ),
      body: Container(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              _previewImage(),
              messageInputArea(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget _previewImage() {
    if (imageFile != null) {
        return Expanded(
          flex: 2,
          child: Image.file(imageFile),
        );
      }
    }

  Padding messageInputArea(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 16.0,
        top: 16.0,
        left: 10.0
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 5,
            child: TextField(
              controller: _textController,
              keyboardType: TextInputType.multiline,
              minLines: 1,
              maxLines: 4,
              cursorColor: Colors.black,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color : Colors.white)
                ),
                hintText: 'Add Caption'
              ),
            ),
          ),
          SizedBox(
            width: 2.0,
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: Colors.blue,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      _submit(context);
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }


}
