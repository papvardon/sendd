/*
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sva/common/chatInputArea.dart';
import 'package:sva/data/databaseHelper.dart';
import 'package:sva/data/services.dart';
import 'package:sva/data/sharedpreferenceshelper.dart';
import 'package:sva/model/ChatDbModel.dart';
import 'package:sva/model/chatMessageModel.dart';
import 'package:sva/model/eventObject.dart';

import 'constants.dart';

class SubChatMessage extends StatefulWidget{
  SubChatMessage({Key key, this.msgModel})
      : super(key: key);

  final ChatDbModel msgModel;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SubChatState(msgModel);
  }

}

class _SubChatState extends State<SubChatMessage>{

  final ScrollController _controller = ScrollController();
  List<ChatDbModel> model = [];
  ChatDbModel msgModel;
  final TextEditingController _textController = TextEditingController();
  var db = DatabaseHelper();
  Timer timer;
  String userID;
  bool reCall = true;
  String _currentMessage;

  _SubChatState(this.msgModel);


  @override
  void initState() {
    super.initState();
    getUserId();
    getLocalSubChat();
    getChats();
    timer = Timer.periodic(Duration(seconds: 10), (Timer t) => reCall ? getChats() : null);
  }

  Future<void> getUserId() async {
    userID = await SharedPreferencesHelper.getUserId();
  }

  Future<void> getChats() async {
    reCall = false;
    var result = await Services.getSubChatMessages(msgModel.channelId, msgModel.id);
    reCall = result != null ? true : false;
    print('my reslt is ${result.data.length}');
    if(result.data.length > 0) {
      var result1 = db.saveSubChatFromServer(result.data, msgModel.channelId);
      getLocalSubChat();
      print('from result get chat $result.toString()');
    }
  }

  void onSaved(String value){
    postSubChat(value, '1000');
  }
  
  Future<void> postSubChat(String currentMessage, String category) async {

    var result = await Services.postSubChatMessage(msgModel.channelId, currentMessage, category, msgModel.id);
    if (result.status == "true") {
      var result1 = await db.saveChatAfterPost(result.data);
      getLocalSubChat();
    }
  }

  Future<void> getLocalSubChat() async {
    EventObject eventObject = await db.getSubChat(msgModel.channelId, msgModel.id);
    if (this.mounted) {
      switch (eventObject.id) {
        case Events.READ_LOGS_SUCCESSFUL:
          setState(() {
            print('i have messages ${model.length}');
            model = eventObject.object;
            _controller.jumpTo(_controller.position.maxScrollExtent);
          });
          break;

        case Events.NO_CHATS:
          print('i am empty messages ');
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          "Chat",
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            SafeArea(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: messageTile(msgModel),
                        )),
                  ),
                  Expanded(
                    flex: 4,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: ListView.builder(
                          padding: EdgeInsets.all(8.0),
                          controller: _controller,
                          itemCount: model.length,
                          itemBuilder: (context, index) {
                            return messageTile(model[index]);
                          }
                      ),
                    ),
                  ),
                  Container(
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: ChatInputArea(onSaved, context)),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget messageTile(ChatDbModel model){

      DateFormat dateFormat = DateFormat("dd-MMM-yyyy hh:mm a");
      var dt = dateFormat.format(
          new DateTime.fromMillisecondsSinceEpoch(model.createAt));

      return Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CircleAvatar(
                radius: 25.0,
                child: Image.network(model.icon),
              ),
              SizedBox(width: 10.0,),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(model.sender, style: TextStyle(fontSize: 17.0, color: Colors.black),),
                  SizedBox(height: 3.0,),
                  Text(dt, style: TextStyle(fontSize: 14.0, color: Colors.black45), ),
                  SizedBox(height: 5.0,),
                  Text(model.message, style: TextStyle(fontSize: 14.0, color: Colors.black45),)
                ],
              )
            ],
          ),
        ),
      );

  }

  */
/*Padding messageInputArea() {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 16.0,
        top: 16.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: Colors.blue,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      //pickImageFromGallery(ImageSource.gallery);
                      //_modalBottomSheetMenu();
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: TextField(
              controller: _textController,
              onChanged: (value) {
                _currentMessage = value;
              },
              keyboardType: TextInputType.multiline,
              minLines: 1,
              maxLines: 4,
              cursorColor: Colors.black,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                border: new OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                hintText: "Enter Message",
                hintStyle: TextStyle(color: Colors.black),
                labelStyle: TextStyle(color: Colors.black),
              ),
            ),
          ),
          SizedBox(
            width: 2.0,
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: Colors.blue,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      _textController.clear();
                      //_cometChat.sendMessage(userMessage: _currentMessage);
                      postSubChat(_currentMessage, '1000');
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }*//*


  @override
  void dispose() {
    // TODO: implement dispose
    db.close();
    timer.cancel();
    super.dispose();
  }


}*/
