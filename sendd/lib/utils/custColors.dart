

import 'dart:ui';

const PrimaryColor =  Color(0xFF165BAC);

const loginGradient =  Color(0xFF2163B9);
const loginBottomGradient =  Color(0xFF090768);


const buttonGradientEnd =  Color(0xFF153999);
const buttonGradientCenter =  Color(0xFF195AAA);
const buttonGradientStart =  Color(0xFF1E92C5);

