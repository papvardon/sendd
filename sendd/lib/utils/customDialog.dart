import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/model/groupRecentChatListModel.dart';
import 'package:sendd/utils/custColors.dart';

class CustomDialog extends StatelessWidget {
  final String title, description, buttonTextAccept, buttonTextDeny;
  final String image;
  final Function accept;
  final Groups data;

  CustomDialog({
    @required this.title,
    @required this.description,
    @required this.buttonTextAccept,
    @required this.buttonTextDeny,
    this.image,
    this.accept,
    this.data
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: 66.0 + 16.0,
            bottom: 16.0,
            left: 16.0,
            right: 16.0,
          ),
          margin: EdgeInsets.only(top: 66.0),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(16.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
              SizedBox(height: 16.0),
              Text(
                description,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              SizedBox(height: 24.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FlatButton(
                    onPressed: () {
                      accept(data);
                      Navigator.of(context).pop(); // To close the dialog
                    },
                    child: Text(buttonTextAccept, style: TextStyle(color: PrimaryColor, fontWeight: FontWeight.bold),),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(); // To close the dialog
                    },
                    child: Text(buttonTextDeny, style: TextStyle(color: Colors.red),),
                  ),
                ],
              ),
            ],
          ),
        ),
        Positioned(
          left: 16.0,
          right: 16.0,
          child: CircleAvatar(
            radius: 60.0,
            child: ClipOval(
              child: CachedNetworkImage(imageUrl: image, fit: BoxFit.cover, height: 100, width: 100,),
            ),
          ),
        ),
      ],
    );
  }


}