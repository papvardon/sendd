import 'package:encrypt/encrypt.dart';

class DataEncrypt {

  Future<String> encryptionFromChat(String senderId, String msg) async{
    final key = Key.fromUtf8(senderId.substring(0,16));
    final iv = IV.fromUtf8('conttextandroidi');
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));

    final encrypted = encrypter.encrypt(msg, iv: iv);
    String encrypMsg = encrypted.base64;

    return encrypMsg;
  }

  Future<String> decryptFromChat(String senderId, String msg) async{
    final key = Key.fromUtf8(senderId.substring(0,16));
    final iv = IV.fromUtf8('conttextandroidi');
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));

    final decrypted = encrypter.decrypt(Encrypted.fromBase64(msg), iv: iv,);
    String deCryptMsg = decrypted;

    return deCryptMsg;
  }


}