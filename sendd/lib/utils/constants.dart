

import 'package:flutter/painting.dart';

const String DATABASE_NAME = "sendd.db";



class LoginUser{
  static String TABLE_NAME = 'User';
  static String USERID = 'id';
  static String IP = 'ip';
  static String TOKEN = 'token';
}

class Chat{
  static String TABLE_NAME = 'Chat';
  static String CHATID = 'id';
  static String SENDERNAME = 'sender';
  static String SENDER_ID = 'sender_id';
  static String NO_OF_REPLY = 'no_of_reply';
  static String NO_Of_LIKES = 'no_of_likes';
  static String CREATE_AT = 'create_at';
  static String UPDATE_AT = 'update_at';
  static String DELETE_AT = 'delete_at';
  static String CHANNEL_ID = 'channel_id';
  static String ROOT_ID = 'root_id';
  static String PARENT_ID = 'parent_id';
  static String ORGINAL_ID = 'original_id';
  static String MESSAGE = 'message';
  static String TYPE = 'type';
  static String HASHTAG = 'hashtags';
  static String PROPS = 'props';
  static String FILENAMES = 'filenames';
  static String ICON = 'icon';
  static String MSGREADBY = 'msgReadBy';
  static String GROUP_ID = 'group_id';
  static String CATEGORY = 'Category';
  static String STATSTATUS = 'StatStatus';
  static String ATTACHMENTNAME = 'AttachmentName';
  static String USERSTATUS = 'userStatus';
  static String REPLIEDNAME = 'replied_name';
  static String REPLIEDIMAGE = 'replied_image';
  static String REPLIEDMESSAGE = 'replied_message';
  static String REPLIEDSENDERID = 'replied_sender_id';
  static String REPLIEDDATE = 'replied_date';
  static String REPLIEDCATEGORY = 'replied_category';
  static String REPLIEDPOSTIMAGE = 'replied_post_image';
  static String DISPLAYNAME = 'display_name';
  static String MID = 'mId';
  static String LOCALFILE = 'SetLocalFile';
  static String LOADING = 'loading';
  static String COLOR = 'color';
}

class DashBoard{
  static String TABLE_NAME = 'Dashboard';
  static String ID = 'id';
  static String NAME = 'name';
  static String ICON = 'icon';
  static String DATA = 'data';
}

class DashBoardLinks{
  static String TABLE_NAME = 'Dashboard_Link';
  static String DASHBOARD_ID = 'dash_id';
  static String ID = 'id';
  static String ICON = 'icon';
  static String TITLE = 'title';
  static String CHANNELID = 'channelId';
  static String LINKID = 'linkid';
}

class RecentChat{
  static String TABLE_NAME = 'RecentChat';
  static String TITLE = 'title';
  static String ICON = 'icon';
  static String CHANNELID = 'ChannelId';
  static String MESSAGE = 'Message';
  static String CREATEAT = 'CreateAt';
  static String USERID = 'userId';
  static String UNREADCOUNT = 'unreadCount';
  static String AVAILABLE = 'Available';
  static String CATEGORY = 'Category';
  static String SENDERID = 'senderId';
  static String ACCEPTSTATUS = 'acceptStatus';
  static String BLOCKSTATUS = 'blockStatus';
  static String PHONENO = 'phoneno';
  static String PROFILEID = 'profileId';
  static String ISPRIVATE = 'isPrivate';
}

class GroupRecentChat{
  static String TABLE_NAME = 'GroupRecentChat';
  static String ID = 'id';
  static String TITLE = 'title';
  static String ICON = 'icon';
  static String LOCATION = 'location';
}

class ChatGroup{
  static String TABLE_NAME = 'ChatGroups';
  static String ID = 'id';
  static String TITLE = 'title';
  static String ICON = 'icon';
  static String MEMBERSCOUNT = 'members_count';
  static String UNREADCOUNT = 'unreadCount';
  static String ADMINNAME = 'adminName';
  static String ACCEPTSTATUS = 'acceptStatus';
  static String ADMINICON = 'adminIcon';
  static String RECENTCHATID = 'recentChatId';
}

class ContactDb{
  static String TABLE_NAME = 'Contacts';
  static String ID = 'id';
  static String TITLE = 'title';
  static String ICON = 'icon';
  static String FIRSTNAME = 'FirstName';
  static String PROFILEID = 'profileid';
}

class GroupSenderColor{
  static String TABLE_NAME = 'GroupSenderColor';
  static String SENDERID = 'senderId';
  static String COLOR = 'color';
}


class CreateTableQueries{
  static String CREATE_LOGIN_USER = "CREATE TABLE " +
      LoginUser.TABLE_NAME +
      "(" +
      LoginUser.USERID +
      " VARCHAR(66) PRIMARY KEY ," +
      LoginUser.IP +
      " VARCHAR(124) NOT NULL," +
      LoginUser.TOKEN +
      " VARCHAR(124) NOT NULL);";

  static String CREATE_CHAT = "CREATE TABLE " +
      Chat.TABLE_NAME +
      "(" +
      Chat.CHATID + " VARCHAR(66) ," +
      Chat.SENDERNAME + " VARCHAR(124) ," +
      Chat.SENDER_ID + " VARCHAR(66) ," +
      Chat.NO_OF_REPLY + " INTEGER ," +
      Chat.NO_Of_LIKES + " INTEGER ," +
      Chat.CREATE_AT + " INTEGER ," +
      Chat.UPDATE_AT + " INTEGER ," +
      Chat.DELETE_AT + " INTEGER ," +
      Chat.CHANNEL_ID + " TEXT NOT NULL," +
      Chat.ROOT_ID + " VARCHAR(66) ," +
      Chat.PARENT_ID + " VARCHAR(66) ," +
      Chat.ORGINAL_ID + " VARCHAR(66) ," +
      Chat.MESSAGE + " TEXT ," +
      Chat.TYPE + " VARCHAR(124) ," +
      Chat.HASHTAG + " VARCHAR(124) ," +
      Chat.PROPS + " VARCHAR(124) ," +
      Chat.FILENAMES + " VARCHAR(124) ," +
      Chat.ICON + " VARCHAR(124) ," +
      Chat.MSGREADBY + " VARCHAR(124) ," +
      Chat.GROUP_ID + " VARCHAR(66) ," +
      Chat.CATEGORY + " VARCHAR(124) ," +
      Chat.STATSTATUS + " VARCHAR(124) ," +
      Chat.ATTACHMENTNAME + " VARCHAR(124) ," +
      Chat.USERSTATUS + " VARCHAR(124) ," +
      Chat.REPLIEDNAME + " VARCHAR(124) ," +
      Chat.REPLIEDIMAGE + " VARCHAR(124) ," +
      Chat.REPLIEDMESSAGE + " TEXT ," +
      Chat.REPLIEDSENDERID + " VARCHAR(66) ," +
      Chat.REPLIEDDATE + " INTEGER ," +
      Chat.REPLIEDCATEGORY + " VARCHAR(124) ," +
      Chat.REPLIEDPOSTIMAGE + " VARCHAR(124) ," +
      Chat.DISPLAYNAME + " VARCHAR(124) ," +
      Chat.MID + " VARCHAR(124) PRIMARY KEY ," +
      Chat.LOCALFILE + " VARCHAR(124) ," +
      Chat.COLOR + " VARCHAR(124) ," +
      Chat.LOADING + " VARCHAR(124)  );";

  static String CREATE_DASHBOARD = "CREATE TABLE " +
      DashBoard.TABLE_NAME +
      "(" +
      DashBoard.ID +
      " VARCHAR(66) PRIMARY KEY ," +
      DashBoard.NAME +
      " VARCHAR(124) NOT NULL," +
      DashBoard.DATA +
      " VARCHAR(124) ," +
      DashBoard.ICON +
      " VARCHAR(124) NOT NULL);";

  static String CREATE_DASHBOARD_LINKS = "CREATE TABLE " +
      DashBoardLinks.TABLE_NAME +
      "(" +
      DashBoardLinks.ID +
      " VARCHAR(66) ," +
      DashBoardLinks.TITLE +
      " VARCHAR(124) NOT NULL," +
      DashBoardLinks.DASHBOARD_ID +
      " VARCHAR(124) NOT NULL," +
      DashBoardLinks.CHANNELID +
      " VARCHAR(124) NOT NULL," +
      DashBoardLinks.LINKID +
      " VARCHAR(66) NOT NULL," +
      DashBoardLinks.ICON +
      " VARCHAR(124) NOT NULL , "+
          " PRIMARY KEY ("+ DashBoardLinks.LINKID +" , "+ DashBoardLinks.DASHBOARD_ID + "));";

  static String CREATE_RECENT_CHATS = "CREATE TABLE " +
      RecentChat.TABLE_NAME +
      "(" +
      RecentChat.TITLE +
      " VARCHAR(124) ," +
      RecentChat.ICON +
      " VARCHAR(124) ," +
      RecentChat.CHANNELID +
      " TEXT NOT NULL PRIMARY KEY ," +
      RecentChat.MESSAGE +
      " TEXT ," +
      RecentChat.CREATEAT +
      " VARCHAR(124) ," +
      RecentChat.USERID +
      " TEXT ," +
      RecentChat.UNREADCOUNT +
      " VARCHAR(66) , "+
      RecentChat.AVAILABLE +
      " VARCHAR(66) , "+
      RecentChat.CATEGORY +
      " VARCHAR(66) , "+
      RecentChat.SENDERID +
      " TEXT , "+
      RecentChat.ACCEPTSTATUS +
      " VARCHAR(66) , "+
      RecentChat.BLOCKSTATUS +
      " VARCHAR(66) , "+
      RecentChat.PHONENO +
      " VARCHAR(66) , "+
      RecentChat.ISPRIVATE +
      " VARCHAR(66) , "+
      RecentChat.PROFILEID +
      " VARCHAR(66) );";

  static String CREATE_GROUP_RECENTCHAT = "CREATE TABLE " +
      GroupRecentChat.TABLE_NAME +
      "(" +
      GroupRecentChat.ID +
      " VARCHAR(66) PRIMARY KEY ," +
      GroupRecentChat.TITLE +
      " VARCHAR(124) NOT NULL," +
      GroupRecentChat.LOCATION +
      " VARCHAR(124) ," +
      DashBoard.ICON +
      " VARCHAR(124) );";

  static String CREATE_CHAT_GROUPS = "CREATE TABLE " +
      ChatGroup.TABLE_NAME +
      "(" +
      ChatGroup.ID +
      " VARCHAR(66) PRIMARY KEY," +
      ChatGroup.TITLE +
      " VARCHAR(124) NOT NULL," +
      ChatGroup.MEMBERSCOUNT +
      " VARCHAR(124) ," +
      ChatGroup.UNREADCOUNT +
      " VARCHAR(124) ," +
      ChatGroup.ADMINNAME +
      " VARCHAR(124) ," +
      ChatGroup.ACCEPTSTATUS +
      " VARCHAR(124) ," +
      ChatGroup.ADMINICON +
      " VARCHAR(124) ," +
      ChatGroup.RECENTCHATID +
      " VARCHAR(124) NOT NULL," +
      ChatGroup.ICON +
      " VARCHAR(124) );";

  static String CREATE_CONTACTS = "CREATE TABLE " +
      ContactDb.TABLE_NAME +
      "(" +
      ContactDb.ID +
      " VARCHAR(66) PRIMARY KEY ," +
      ContactDb.TITLE +
      " VARCHAR(124) NOT NULL," +
      ContactDb.ICON +
      " VARCHAR(124) ," +
      ContactDb.PROFILEID +
      " VARCHAR(124) ," +
      ContactDb.FIRSTNAME +
      " VARCHAR(124) );";

  static String CREATE_GROUP_USER_COLOR = "CREATE TABLE " +
      GroupSenderColor.TABLE_NAME +
      "(" +
      GroupSenderColor.SENDERID +
      " VARCHAR(66) PRIMARY KEY ," +
      GroupSenderColor.COLOR +
      " INTEGER NOT NULL );";

}

class Events {
  static const int NO_INTERNET_CONNECTION = 0;


  static const int READ_LOGS_SUCCESSFUL = 502;
  static const int NO_LOGS_FOUND = 503;

  static const int NO_CHATS = 504;

}


