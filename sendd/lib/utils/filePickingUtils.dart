import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sendd/utils/imagePreviewPage.dart';

class FilePickingUtils{

  final picker = ImagePicker();
  File _image;

  Future<String> getImage(BuildContext context) async {
    var finalResult;
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    _image = File(pickedFile.path);
    var result = await Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ImagePreviewPage(imageFile: _image,)));
    if(result != null && result != '1002'){
      //finalResult = "{ 'image' : _$pickedFile, 'msg' : $result }";
      //finalResult = await ChatDBHandling().saveImageToLocaldatabase((result == '1001') ? '':result, '1001', _image);
      finalResult = "{ 'image' : _$_image, 'msg' : $result }";
    }else{
      finalResult = "{'msg' : $result }";
      print('resullt is big emptyyyyyyy');
    }

  return finalResult;

  }

}