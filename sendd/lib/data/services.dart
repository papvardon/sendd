
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/LabPrescriptionModel.dart';
import 'package:sendd/model/appointmentModel.dart';
import 'package:sendd/model/bundleCommonModel.dart';
import 'package:sendd/model/chatMessageModel.dart';
import 'package:sendd/model/chatModel.dart';
import 'package:sendd/model/chatPostModel.dart';
import 'package:sendd/model/clinicListModel.dart';
import 'package:sendd/model/clinicSubMenuModel.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/model/completedAppointmentModel.dart';
import 'package:sendd/model/contactsModel.dart';
import 'package:sendd/model/countryModel.dart';
import 'package:sendd/model/createChannelIdModel.dart';
import 'package:sendd/model/dashboardmodel.dart';

import 'package:sendd/model/eventObject.dart';
import 'package:sendd/model/forgotPasswordModel.dart';
import 'package:sendd/model/groupInfoModel.dart';
import 'package:sendd/model/groupRecentChatListModel.dart';
import 'package:sendd/model/intiateOrderIdModel.dart';
import 'package:sendd/model/loginresponse.dart';
import 'package:sendd/model/medicinePrescriptionModel.dart';
import 'package:sendd/model/ouListModel.dart';
import 'package:sendd/model/recentChatListModel.dart';
import 'package:sendd/model/visitSummaryDetailModel.dart';
import 'package:sendd/utils/constants.dart';
import 'package:sendd/utils/dataEncrypt.dart';

class Services {

  //static final BASE_URL = "http://125.99.242.202:6041/MasterIota/";
  // for flutter development only
  static final BASE_URL = "http://gosendd.com/MasterIota/";
  static final BASE_URL_ALERTS = "http://157.245.108.109/";

  static Future<EventObject> doLogin(String phonenumber, String passsword ) async {
    print(phonenumber);
    print(passsword);
    var res = await http.post(
      BASE_URL+'Api/loginwithmobile',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'mobilenumber': phonenumber,
        'password': passsword,
        'device_id': "123456",
        'appxtnsn': "sendd",
        'platform': 'android',
        'version': '1.0.6',
        'username': phonenumber,
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      if(data.containsKey('data')){
        return EventObject(id: Events.READ_LOGS_SUCCESSFUL, object: Loginresponse.fromJsonMap(data));
      }
      else
        {
          return EventObject(id: Events.NO_LOGS_FOUND, object: CommonModel.fromJson(data));
        }
    }
    else
      {
        return EventObject(id: Events.NO_CHATS);
      }
  }

  static Future<CountryModel> getCountryCode() async {
    var res = await http.post(
      BASE_URL+'Api/getCountrycode',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = jsonDecode(res.body);
      return CountryModel.fromJson(data);
    }
  }

  static Future<CommonModel> doUserRegistration(String encodedJson) async {
    var res = await http.post(
      BASE_URL+'Api/userRegistration',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encodedJson,
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = jsonDecode(res.body);
      return CommonModel.fromJson(data);
    }
  }

  static Future<CommonModel> doOTPVerify(String phNo, String otp) async {

    var res = await http.post(
      BASE_URL+'Api/verifyotp',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'Phoneno': phNo,
        'otp': otp,
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<ForgotPasswordModel> doForgotPassword(String phNo) async {
    var res = await http.post(
      BASE_URL+'Api/ForgotPassword',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'phoneno': phNo
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ForgotPasswordModel.fromJson(data);
    }

  }

  static Future<DashboardModel> getData() async {

    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String userId = await SharedPreferencesHelper.getUserId();
    String profileId = await SharedPreferencesHelper.getProfileId();
    String roleType = await SharedPreferencesHelper.getRoleType();
    String token = await SharedPreferencesHelper.getToken();
    print(dynamicUrl);
    print('my profile id $roleType');
    print('my profile id $profileId');
    print('my user id $userId');
    print('my toekn id $token');
    var res = await http.post(
      dynamicUrl+'modmatter/SCR_HME_1.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_id': userId,
        'profile_id': profileId,
        'latitude': '0.0',
        'longitude': '0.0',
        'roleType': roleType,
        'platform': 'android',
        'token': token
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
//      var re = await getDynamicData().then((value) => value.heading.length>0 ? data. : print('i didnt have value'));
//      print('my re value is $re');
      return DashboardModel.fromJsonMap(data);
    }

  }

  static Future<DashboardModel> getDynamicData() async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String userId = await SharedPreferencesHelper.getUserId();
    String profileId = await SharedPreferencesHelper.getProfileId();
    String roleType = await SharedPreferencesHelper.getRoleType();
    String token = await SharedPreferencesHelper.getToken();
    print(roleType);
    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_40.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_id': userId,
        'profile_id': profileId,
        'latitude': '0.0',
        'longitude': '0.0',
        'roleType': roleType,
        'platform': 'android',
        'token': token
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return DashboardModel.fromJsonMap(data);
    }

  }

  static Future<ClinicListModel> getMyClinics() async {
    String userId = await SharedPreferencesHelper.getUserId();
    String dynamicUrl = await SharedPreferencesHelper.getChannelId();
    String phoneNumber = await SharedPreferencesHelper.getMobileNumber();
    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_26.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_id': userId,
        'mobile_number': phoneNumber
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ClinicListModel.fromJson(data);
    }

  }

  static Future<ClinicSubMenuModel> getMyClinicsSubMenu(String mrn, String clinicId) async {
    String dynamicUrl = await SharedPreferencesHelper.getChannelId();
    String phoneNumber = await SharedPreferencesHelper.getMobileNumber();
    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_41.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'mrn': mrn,
        'mobile_number': phoneNumber,
        'id': clinicId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ClinicSubMenuModel.fromJson(data);
    }

  }

  static Future<BundleCommonModel> getPatientData(String clinicId, String mrn) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    print(clinicId);
    print(mrn);

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_33.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'clinic_id': clinicId,
        'mrn': mrn,
      }),
    );
    debugPrint("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return BundleCommonModel.fromJson(data);
    }
  }

  static Future<AppointmentModel> doAppointmentBooking(String encodedJson) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_34.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encodedJson,
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return AppointmentModel.fromJson(data);
    }

  }

  static Future<CompletedAppointmentModel> getCompletedAppointments(String mrn, String clinicId, String token) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_27.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token" : token,
        "clinic_id": clinicId,
        "mrn": mrn
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CompletedAppointmentModel.fromJson(data);
    }

  }

  static Future<CompletedAppointmentModel> getPendingAppointments(String mrn, String clinicId) async {

    String token = await SharedPreferencesHelper.getToken();
    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_28.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token" : token,
        "clinic_id": clinicId,
        "mrn": mrn
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CompletedAppointmentModel.fromJson(data);
    }
  }

  static Future<VisitSummaryDetailModel> getVisitSummaryDetails(String clinicId, String visitId, String mrn, String token) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_29.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },

        body: jsonEncode(<String, String>{
          'token': token,
          'mrn': mrn,
          'visit_id': visitId,
          'clinic_id': clinicId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return VisitSummaryDetailModel.fromJson(data);
    }

  }

  static Future<MedicinePrescriptionModel> getMedicinePrescriptions(String clinicId, String visitNumber, String token) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_30.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'visit_number': visitNumber,
        'clinic_id': clinicId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return MedicinePrescriptionModel.fromJson(data);
    }

  }

  static Future<LabPrescriptionModel> getLabPrescriptions(String clinicId, String visitNumber, String token) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_31.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'visit_number': visitNumber,
        'clinic_id': clinicId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return LabPrescriptionModel.fromJson(data);
    }

  }

  static Future<CreateChannelIdModel> getChannelID(String clinicId, String receiverUserId, String token) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    print("my usrid $receiverUserId");
    print("my token $token");
    print("my clinic id $clinicId");
    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_2.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'user_id': receiverUserId,
        'clinic_id': clinicId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CreateChannelIdModel.fromJson(data);
    }

  }

  static Future<ChatModel> getChat(String channelId, String token) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    print(dynamicUrl);
    print('channle id $channelId');
    print('token $token');
    print('userid ${await SharedPreferencesHelper.getUserId()}');

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_21.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'channel_id': channelId,
        'start': '0',
        'count': '30'
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ChatModel.fromJson(data);
    }

  }

  static Future<ChatPostModel> postMessage(String channelId, String msg, String category, String mid, String token, String userID) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    print(dynamicUrl);
    print('channle id $channelId');

    String enMsg = await DataEncrypt().encryptionFromChat(userID, msg);

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_32.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'channel_id': channelId,
        'Message': enMsg,
        'category': category,
        'statstatus': '0',
        'mId':mid
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ChatPostModel.fromJson(data);
    }

  }

  static Future<ChatPostModel> postImageAndMessage(ChatMessageModel value, String token, String userID) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    print(dynamicUrl);
    print('token $token');
    print('channle id ${value.channelId}');
    print('filename ${value.attachmentName}');

    String enMsg = '';
    if(value.message != null) {
      enMsg = await DataEncrypt().encryptionFromChat(userID, value.message);
    }

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_39.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'channel_id': value.channelId,
        'Message': enMsg,
        'category': value.category,
        'statstatus': '0',
        'attachment': 'yes',
        'file':value.filenames,
        'fileextension':'',
        'filename':value.attachmentName,
        'forwardstatus':'0',
        'mId':value.mId

      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ChatPostModel.fromJson(data);
    }

  }

  static Future<ChatPostModel> postSubChatMessage(String channelId, String msg, String category, String rootId, String token, String userID) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    print(dynamicUrl);
    print('token $token');
    print('channle id $channelId');

    String enMsg = await DataEncrypt().encryptionFromChat(userID, msg);

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_32.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'channel_id': channelId,
        'root_id': rootId,
        'Message': enMsg,
        'category': category,
        'statstatus': '0'
      }),
    );
    print("this data from service class POst =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ChatPostModel.fromJson(data);
    }

  }

  static Future<ChatModel> getSubChatMessages(String channelId, String postId, String token) async {

    String dynamicUrl = await SharedPreferencesHelper.getChannelId();
    String userId = await SharedPreferencesHelper.getUserId();

    print(dynamicUrl);
    print('token $token');
    print('channle id $channelId');
    print('post id $postId');

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_27.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'channel_id': channelId,
        'post_id': postId,
        'user_id': userId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ChatModel.fromJson(data);
    }

  }

  static Future<InitiateOrderIdModel> getOrderIdForPayment(String amount) async {
    String dynamicUrl = await SharedPreferencesHelper.getChannelId();

    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_47.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'amount': amount
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return InitiateOrderIdModel.fromJson(data);
    }

  }

  static Future<CommonModel> doPaymentVerify(String amount, String paymnetId, String orderId) async {
    String dynamicUrl = await SharedPreferencesHelper.getChannelId();
    print(dynamicUrl);
    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_48.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'amount': amount,
        'paymentid': paymnetId,
        'orderid': orderId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<CommonModel> doPaymentUpdateInServer(String encodedJson) async {
    String dynamicUrl = await SharedPreferencesHelper.getChannelId();
    print(dynamicUrl);
    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_38.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encodedJson,
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<CommonModel> uploadLabReport(String encodedJson) async {
    String dynamicUrl = await SharedPreferencesHelper.getChannelId();
    print(dynamicUrl);
    var res = await http.post(
      dynamicUrl+'iota/Cards/gp/SCR_IOTAGP_35.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encodedJson
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<RecentChatListModel> getRecentChatOnetoOne() async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    var res = await http.post(
        dynamicUrl+'iota/getRecentDirectChat.php',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "token": token,
          'userId': userId
        }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return RecentChatListModel.fromJson(data);
    }

  }

  static Future<RecentChatListModel> getRecentChatPrivate() async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    var res = await http.post(
      dynamicUrl+'modmatter/getSecretChats.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token": token,
        'userId': userId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return RecentChatListModel.fromJson(data);
    }

  }

  static Future<GroupRecentChatListModel> getRecentChatForGroups() async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    print(dynamicUrl);
    print(token);
    print(userId);

    var res = await http.post(
      dynamicUrl+'modmatter/getOuGroups.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token": token,
        'userId': userId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return GroupRecentChatListModel.fromJson(data);
    }

  }

  static Future<ContactsModel> getUserContacts() async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    print(dynamicUrl);
    print(token);
    print(userId);

    var res = await http.post(
      dynamicUrl+'distributionmanager/getContactsList.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token": token,
        'userId': userId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ContactsModel.fromJson(data);
    }

  }

  static Future<CommonModel> doGroupCreation(String encodedJson) async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    print(dynamicUrl);
    print(token);
    print(userId);

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_9.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encodedJson
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<OuListModel> getOUList() async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();

    var res = await http.post(
        dynamicUrl+'modmatter/SCR_MOD_24.php',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      body: jsonEncode(<String, String>{
        "token": token
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return OuListModel.fromJson(data);
    }

  }

  static Future<CommonModel> acceptGroupChat(String encodedJson) async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_4.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
        body: encodedJson
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<GroupInfoModel> getGroupInformation(String channelId) async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    var res = await http.post(
        dynamicUrl+'iota/SCR_IOTA_12.php',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      body: jsonEncode(<String, String>{
        "token": token,
        "userId": userId,
        "channel_id": channelId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return GroupInfoModel.fromJson(data);
    }

  }

  static Future<CommonModel> exitGroup(String channelId) async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_14.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token": token,
        "userId": userId,
        "channel_id": channelId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<CommonModel> removeAMemberFromGroup(String channelId, String memberId) async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_33.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token": token,
        "userId": userId,
        "channelId": channelId,
        "memberId": memberId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<ContactsModel> getSenddContact(String channelId) async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();

    print(dynamicUrl);
    print(token);
    print(userId);

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_29.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "token": token,
        'userId': userId,
        "channel_id": channelId
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ContactsModel.fromJson(data);
    }

  }

  static Future<CommonModel> updateGrpMembers(String encodedJson) async {
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_36.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encodedJson
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CommonModel.fromJson(data);
    }

  }

  static Future<CreateChannelIdModel> createChannelIdForConverse(String receiverUserId) async {

    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();

    print("url $dynamicUrl");
    print("my usrid $receiverUserId");
    print("my token $token");
    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_17.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'user_id': receiverUserId,
        'type': "D"
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return CreateChannelIdModel.fromJson(data);
    }

  }

  ///***  for alerts get chat  **////
  static Future<ChatModel> getChatForAlert(String channelId) async {

    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    String token = await SharedPreferencesHelper.getToken();

    print('token $token');
    print('channle id $channelId');

    var res = await http.post(
      dynamicUrl+'modmatter/SCR_MOD_21.php',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'token': token,
        'channel_id': channelId,
        'start': '0',
        'count': '30'
      }),
    );
    print("this data from service class =====> "+res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      return ChatModel.fromJson(data);
    }

  }



}
