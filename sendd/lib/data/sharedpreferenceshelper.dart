import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper{

  static final String _USERID = "userId";
  static final String _PROFILEID = "profileId";
  static final String _TOKEN = "token";
  static final String _DYNAURL = "baseurl";
  static final String _ROLETYPE = "roletype";
  static final String _LOGDINSTATUS = "logdinStatus";
  static final String _TERMSSTATUS = "termsStatus";
  static final String _MOBILENUMBER = "mobileNumber";
  static final String _DYNAMICCHANNELID = "channelID";
  static final String _ALERTCHANNEL = "alertChannel";

  ///*** userid ***///
  static Future<String> getUserId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_USERID);
  }

  static Future<bool> setUserId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_USERID, value);
  }

  ///*** profileid ***///
  static Future<String> getProfileId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_PROFILEID);
  }

  static Future<bool> setProfileId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_PROFILEID, value);
  }

  ///*** token ***///
  static Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_TOKEN);
  }

  static Future<bool> setToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_TOKEN, value);
  }

  ///*** dynamicUrl ***///
  static Future<String> getDynamicUrl() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_DYNAURL);
  }

  static Future<bool> setDynamicUrl(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_DYNAURL, value);
  }

  ///*** logdinStatus ***///
  static Future<String> getLogdinStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_LOGDINSTATUS);
  }

  static Future<bool> setLogdinStatus(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_LOGDINSTATUS, value);
  }

  ///*** roletype ***///
  static Future<String> getRoleType() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_ROLETYPE);
  }

  static Future<bool> setRoleType(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_ROLETYPE, value);
  }

  ///*** terms and conditin status ***///
  static Future<String> getTermsStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_TERMSSTATUS);
  }

  static Future<bool> setTermsStatus(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_TERMSSTATUS, value);
  }

  ///*** mobile number ***///
  static Future<String> getMobileNumber() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_MOBILENUMBER);
  }

  static Future<bool> setMobileNumber(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_MOBILENUMBER, value);
  }

  ///*** channelID ***///
  static Future<String> getChannelId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_DYNAMICCHANNELID);
  }

  static Future<bool> setChannelId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_DYNAMICCHANNELID, value);
  }

  ///*** ALERT CHANNEL  ***///
  static Future<String> getAlertChannel() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_ALERTCHANNEL);
  }

  static Future<bool> setAlertChannel(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_ALERTCHANNEL, value);
  }

}