import 'dart:convert';

import 'package:encrypt/encrypt.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sendd/contacts.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/ChatDbModel.dart';
import 'package:sendd/model/chatMessageModel.dart';
import 'package:sendd/model/contactsModel.dart';
import 'package:sendd/model/dashboarddatamodel.dart';
import 'package:sendd/model/eventObject.dart';
import 'package:sendd/model/groupRecentChatListModel.dart';
import 'package:sendd/model/heading.dart';
import 'package:sendd/model/loginData.dart';
import 'package:sendd/model/recentChatListModel.dart';
import 'package:sendd/utils/constants.dart';
import 'package:sendd/utils/dataEncrypt.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math' as math;

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  // Increment this version when you need to change the schema.
  static final _databaseVersion = 2;

  static Database _database;

  DatabaseHelper.internal();

  static DatabaseHelper get() {
    return _instance;
  }

  bool didInit = false;

  Future<Database> _getDb() async {
    if (!didInit) await _initDatabase();
    return _database;
  }

  Future _initDatabase() async {
    var directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, DATABASE_NAME);
    _database = await openDatabase(path, version: _databaseVersion,
        onCreate: (Database db, int version) async {
      await _createUserTable(db);
      await _createChatTable(db);
      await _createDashBoardTable(db);
      await _createDashBoardLinkTable(db);
      await _createRecentChatTable(db);
      await _createRecentGroupChatTable(db);
      await _createChatGroupTable(db);
      await _createContactsTable(db);
      await _createSenderColorTable(db);
    }, onUpgrade: (Database db, int oldVersion, int newVersion) async {
      await db.execute("DROP TABLE ${LoginUser.TABLE_NAME}");
      await db.execute("DROP TABLE ${Chat.TABLE_NAME}");
      await db.execute("DROP TABLE ${DashBoard.TABLE_NAME}");
      await db.execute("DROP TABLE ${DashBoardLinks.TABLE_NAME}");
      await db.execute("DROP TABLE ${RecentChat.TABLE_NAME}");
      await db.execute("DROP TABLE ${GroupRecentChat.TABLE_NAME}");
      await db.execute("DROP TABLE ${ChatGroup.TABLE_NAME}");
      await db.execute("DROP TABLE ${ContactDb.TABLE_NAME}");
      await db.execute("DROP TABLE ${GroupSenderColor.TABLE_NAME}");
      await _createUserTable(db);
      await _createChatTable(db);
      await _createDashBoardTable(db);
      await _createDashBoardLinkTable(db);
      await _createRecentChatTable(db);
      await _createRecentGroupChatTable(db);
      await _createChatGroupTable(db);
      await _createContactsTable(db);
      await _createSenderColorTable(db);
    });
    didInit = true;
  }

  Future _createUserTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_LOGIN_USER);
    });
  }

  Future _createChatTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_CHAT);
    });
  }

  Future _createDashBoardTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_DASHBOARD);
    });
  }

  Future _createDashBoardLinkTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_DASHBOARD_LINKS);
    });
  }

  Future _createRecentChatTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_RECENT_CHATS);
    });
  }
  Future _createRecentGroupChatTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_GROUP_RECENTCHAT);
    });
  }
  Future _createChatGroupTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_CHAT_GROUPS);
    });
  }
  Future _createContactsTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_CONTACTS);
      txn.execute(CreateTableQueries.CREATE_GROUP_USER_COLOR);
    });
  }
  Future _createSenderColorTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute(CreateTableQueries.CREATE_GROUP_USER_COLOR);
    });
  }

  Future<int> saveLogin(LoginData data) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    var result = await dbClient.rawInsert(
        "INSERT INTO ${LoginUser.TABLE_NAME} (${LoginUser.USERID}, ${LoginUser.IP}, ${LoginUser.TOKEN})"
        " VALUES (\'${data.id}\',\'${data.IP}\',\'${data.token}\')");

    return result;
  }

  Future<List> getLogdinUser() async {
    var dbClient = await DatabaseHelper.get()._getDb();
    var result =
        await dbClient.rawQuery('SELECT * FROM $LoginUser.TABLE_NAME ');

    return result.toList();
  }

  Future<int> saveChat(String msg, String channelId, String senderId) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    //var result = await dbClient.insert(Chat.TABLE_NAME, data.toJson());
    var result = await dbClient.rawInsert(
        'INSERT INTO ${Chat.TABLE_NAME} (${Chat.CHANNEL_ID}, ${Chat.MESSAGE}, ${Chat.SENDER_ID}) VALUES (\'$channelId\', \'$msg\', \'$senderId\')');

    return result;
  }

  /// ** get normal chat from db ****/
  Future<EventObject> getChat(String channelId, String rootId) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    List<Map> chatMap = await dbClient.rawQuery(
        "SELECT * FROM ${Chat.TABLE_NAME} JOIN ${GroupSenderColor.TABLE_NAME} ON ${Chat.SENDER_ID} = ${GroupSenderColor.SENDERID}"
            " WHERE ${Chat.CHANNEL_ID} = '$channelId' AND ${Chat.ROOT_ID} = '$rootId' ORDER BY ${Chat.CREATE_AT}");

    List<ChatDbModel> chats = new List();
    if (chatMap.isNotEmpty) {
      for (int i = 0; i < chatMap.length; i++) {
        chats.add(ChatDbModel.fromJson(chatMap[i]));
      }
      return EventObject(id: Events.READ_LOGS_SUCCESSFUL, object: chats);
    } else {
      return EventObject(id: Events.NO_CHATS);
    }
//    var result = await dbClient.rawInsert(
//        'INSERT INTO $tableNote ($columnTitle, $columnDescription) VALUES (\'${note.title}\', \'${note.description}\')');

    return null;
  }


  Future<int> deleteUser() async {
    try {
      var dbClient = await DatabaseHelper.get()._getDb();
      await dbClient.transaction((txn) async {
        var batch = txn.batch();
        batch.delete(LoginUser.TABLE_NAME);
        await batch.commit();
      });
    } catch (error) {
      throw Exception('DbBase.cleanDatabase: ' + error.toString());
    }
  }


  Future<int> saveDashBoard(List<Heading> model) async{

    var dbClient = await DatabaseHelper.get()._getDb();

    Batch batch = dbClient.batch();

    model.forEach((element)  async {

      var result =  batch.rawInsert('INSERT OR REPLACE INTO ${DashBoard.TABLE_NAME} (${DashBoard.ID}, ${DashBoard.NAME}, ${DashBoard.ICON}) VALUES (\'${element.id}\', \'${element.name}\',\'${element.icon}\')');

      Batch dBatch = dbClient.batch();

      element.data.forEach((innerElement) {
        var result = dBatch.rawInsert('INSERT OR REPLACE INTO ${DashBoardLinks.TABLE_NAME} (${DashBoardLinks.ID}, ${DashBoardLinks.TITLE}, ${DashBoardLinks.ICON}, ${DashBoardLinks.CHANNELID}, ${DashBoardLinks.DASHBOARD_ID}, ${DashBoardLinks.LINKID}) '
            'VALUES (\'${innerElement.id}\', \'${innerElement.title}\',\'${innerElement.icon}\',\'${innerElement.channelId}\',\'${element.id}\',\'${innerElement.linkid}\')');

      });

      var batchResult = await dBatch.commit();

    });

    var completeBatch = await batch.commit();

 return 1;
  }

  Future<List<Heading>> getDashBoard() async {

    var dbClient = await DatabaseHelper.get()._getDb();
    String qry = 'SELECT * FROM ${DashBoard.TABLE_NAME}';

    List<Map> model = await dbClient.rawQuery(qry);
    List<Heading> model1 = new List();

    model.forEach((element) async {
      //var sd = await getDashBoardLinks(element['id']);
      Heading hd = new Heading(element['id'], element['name'], element['icon'], null);
      model1.add(hd);
    });

    return model1;
  }


  Future<List<Dashboarddatamodel>> getDashBoardLinks(String id) async {

    var dbClient = await DatabaseHelper.get()._getDb();
    String qry1 = 'SELECT * FROM ${DashBoardLinks.TABLE_NAME} WHERE ${DashBoardLinks.DASHBOARD_ID} = $id';

    var response = await dbClient.rawQuery(qry1);


    List<Dashboarddatamodel> dashDataModel = response.map((e) => Dashboarddatamodel.fromJsonMap(e)).toList();

    return dashDataModel;
  }

  Future<String> getChannelid(String id) async {

    var dbClient = await DatabaseHelper.get()._getDb();
    String qry1 = 'SELECT * FROM ${DashBoardLinks.TABLE_NAME} WHERE ${DashBoardLinks.LINKID} = $id';
    var response = await dbClient.rawQuery(qry1);
    String channelid;
    List<Dashboarddatamodel> dashDataModel = response.map((e) => Dashboarddatamodel.fromJsonMap(e)).toList();

    if(dashDataModel.length>0){
      channelid = dashDataModel[0].channelId;
    }

    return channelid;
  }


  Future<int> deleteDashboard() async {
    try {
      var dbClient = await DatabaseHelper.get()._getDb();
      await dbClient.transaction((txn) async {
        var batch = txn.batch();
        batch.delete(DashBoard.TABLE_NAME);
        batch.delete(DashBoardLinks.TABLE_NAME);
        await batch.commit();
      });
    } catch (error) {
      throw Exception('DbBase.cleanDatabase: ' + error.toString());
    }
  }

  Future<int> saveChatFromServer(List<ChatMessageModel> model, String channelId, bool isFromBob) async {

    var dbClient = await DatabaseHelper.get()._getDb();
    Batch batch = dbClient.batch();

    model.forEach((element) async {


      ChatDbModel dbModel = new ChatDbModel();

      dbModel.id = element.id;
      dbModel.sender = element.sender;
      dbModel.senderId = element.senderId;
      dbModel.noOfReply = element.noOfReply;
      dbModel.noOfLikes = element.noOfLikes;
      dbModel.createAt = element.createAt;
      dbModel.updateAt = element.updateAt;
      dbModel.deleteAt = element.deleteAt;
      dbModel.channelId = element.channelId;
      dbModel.rootId = element.rootId;
      dbModel.parentId = element.parentId;
      dbModel.originalId = element.originalId;
      dbModel.type = element.type;
      dbModel.hashtags = element.hashtags;
      dbModel.filenames = element.filenames;
      dbModel.icon = element.icon;
      dbModel.msgReadBy = element.msgReadBy;
      dbModel.category = element.category;
      dbModel.statStatus = element.statStatus;
      dbModel.attachmentName = element.attachmentName;
      dbModel.userStatus = element.userStatus;


      String query = "SELECT * FROM ${Chat.TABLE_NAME} WHERE ${Chat.CHATID} = '${element.id}' OR ${Chat.MID} = '${element.mId}'";
      List<Map> chatMap = await dbClient.rawQuery(query);

      if (chatMap.isNotEmpty && chatMap.length > 0) {
        List<ChatDbModel> chats = new List();
        chats.add(ChatDbModel.fromJson(chatMap[0]));
        if (chats[0].id == element.id || chats[0].mId == element.mId) {

          if(element.subchatmessages.length>0){
            int len = (element.subchatmessages.length - 1);
            dbModel.repliedCategory = element.subchatmessages[len].category;
            dbModel.createAt = element.subchatmessages[len].createAt;
            dbModel.repliedMessage = await DataEncrypt().decryptFromChat(element.subchatmessages[len].senderId, element.subchatmessages[len].message);
          }

          dbModel.mId = (chats[0].mId != null) ? chats[0].mId : dbModel.id;
          if(!isFromBob) {
            if (element.message != null && element.message.isNotEmpty) {
              dbModel.message =
              await DataEncrypt().decryptFromChat(
                  dbModel.senderId, element.message);
            }
          }
          else{
            dbModel.message = element.message;
          }
          if(chats[0].setLocalFile != null) {
            dbModel.setLocalFile = chats[0].setLocalFile;
          }

          batch.update(Chat.TABLE_NAME, dbModel.toJson(), where: '${Chat.CHATID} = ? OR ${Chat.MID} = ?', whereArgs: [element.id, element.mId]);
        }
        else{

          if(element.subchatmessages.length>0){
            int len = (element.subchatmessages.length - 1);
            dbModel.repliedCategory = element.subchatmessages[len].category;
            dbModel.repliedMessage = await DataEncrypt().decryptFromChat(element.subchatmessages[len].senderId, element.subchatmessages[len].message);
          }

          if(!isFromBob) {
            if (element.message != null && element.message.isNotEmpty) {
              dbModel.message =
              await DataEncrypt().decryptFromChat(
                  element.senderId, element.message);
            }
          }
          else{
            dbModel.message = element.message;
          }
          dbModel.mId = (element.mId != null) ? element.mId : element.id;
          dbModel.setLocalFile = '';

          var result = batch.insert(Chat.TABLE_NAME, dbModel.toJson());
          //var bathResult = batch.execute("INSERT INTO ${Chat.TABLE_NAME} (${Chat.CHATID}, ${Chat.MID}, ${Chat.CHANNEL_ID})VALUES (?,?,?) ",  dummyModel);
        }
      }
      else{

        saveGroupUsersColour(element.senderId);

        if(element.subchatmessages.length>0){
          int len = (element.subchatmessages.length - 1);
          dbModel.repliedCategory = element.subchatmessages[len].category;
          dbModel.repliedMessage = await DataEncrypt().decryptFromChat(element.subchatmessages[len].senderId, element.subchatmessages[len].message);
        }

        if(!isFromBob) {
          if (element.message != null && element.message.isNotEmpty) {
            dbModel.message =
            await DataEncrypt().decryptFromChat(
                element.senderId, element.message);
          }
        }
        else{
          dbModel.message = element.message;
        }

        if(element.mId !=null){
          if(element.mId.trim().length>0) {
            dbModel.mId = element.mId;
          }
          else{
            dbModel.mId = element.id;
          }
        }
        else{
          dbModel.mId = element.id;
        }
        //dbModel.mId = (element.mId != null || element.mId.length>0) ? element.mId : element.id;
        dbModel.setLocalFile = '';


        var result = batch.insert(Chat.TABLE_NAME, dbModel.toJson());
        //var bathResult = batch.execute("INSERT INTO ${Chat.TABLE_NAME} (${Chat.CHATID}, ${Chat.MID}, ${Chat.CHANNEL_ID})VALUES (?,?,?) ",  dummyModel);
      }

    });

    var result = await batch.commit();
    print(result);

    return null;
  }

  Future<dynamic> saveChatAfterPost(ChatMessageModel element, String userID) async {


    ChatDbModel dbModel = new ChatDbModel();

    dbModel.id = element.id;
    dbModel.sender = element.sender;
    dbModel.senderId = element.senderId;
    dbModel.noOfReply = element.noOfReply;
    dbModel.noOfLikes = element.noOfLikes;
    dbModel.createAt = element.createAt;
    dbModel.updateAt = element.updateAt;
    dbModel.deleteAt = element.deleteAt;
    dbModel.channelId = element.channelId;
    dbModel.rootId = element.rootId;
    dbModel.parentId = element.parentId;
    dbModel.originalId = element.originalId;
    dbModel.type = element.type;
    dbModel.hashtags = element.hashtags;
    dbModel.filenames = element.filenames;
    dbModel.icon = element.icon;
    dbModel.msgReadBy = element.msgReadBy;
    dbModel.category = element.category;
    dbModel.statStatus = element.statStatus;
    dbModel.attachmentName = element.attachmentName;
    dbModel.userStatus = element.userStatus;

    //here after posting response will come and userid is senderid
    var dbClient = await DatabaseHelper.get()._getDb();
    Batch batch = dbClient.batch();

      dbModel.message = element.message;

      dbModel.setLocalFile = element.setLocalFile;
      dbModel.mId = (element.mId != null) ? element.mId : element.id;

      batch.insert(Chat.TABLE_NAME, dbModel.toJson());


    dynamic result = await batch.commit();

    return result;
  }

  Future<int> saveSubChatFromServer(List<ChatMessageModel> model, String channelId) async {

    var dbClient = await DatabaseHelper.get()._getDb();
    Batch batch = dbClient.batch();

    model.forEach((element) async {


      ChatDbModel dbModel = new ChatDbModel();

      dbModel.id = element.id;
      dbModel.sender = element.sender;
      dbModel.senderId = element.senderId;
      dbModel.noOfReply = element.noOfReply;
      dbModel.noOfLikes = element.noOfLikes;
      dbModel.createAt = element.createAt;
      dbModel.updateAt = element.updateAt;
      dbModel.deleteAt = element.deleteAt;
      dbModel.channelId = element.channelId;
      dbModel.rootId = element.rootId;
      dbModel.parentId = element.parentId;
      dbModel.originalId = element.originalId;
      dbModel.type = element.type;
      dbModel.hashtags = element.hashtags;
      dbModel.filenames = element.filenames;
      dbModel.icon = element.icon;
      dbModel.msgReadBy = element.msgReadBy;
      dbModel.category = element.category;
      dbModel.statStatus = element.statStatus;
      dbModel.attachmentName = element.attachmentName;
      dbModel.userStatus = element.userStatus;


      String query = "SELECT * FROM ${Chat.TABLE_NAME} WHERE ${Chat.CHATID} = '${element.id}' OR ${Chat.MID} = '${element.mId}' AND ${Chat.ROOT_ID} = '${element.rootId}'";
      List<Map> chatMap = await dbClient.rawQuery(query);

      if (chatMap.isNotEmpty && chatMap.length > 0) {
        List<ChatDbModel> chats = new List();
        chats.add(ChatDbModel.fromJson(chatMap[0]));
        if (chats[0].id == element.id || chats[0].mId == element.mId) {

          dbModel.mId = (chats[0].mId != null) ? chats[0].mId : element.id;
          if(element.message != null && element.message.isNotEmpty) {
            dbModel.message =
            await DataEncrypt().decryptFromChat(element.senderId, element.message);
          }
          if(chats[0].setLocalFile != null) {
            dbModel.setLocalFile = chats[0].setLocalFile;
          }

          batch.update(Chat.TABLE_NAME, dbModel.toJson(), where: '${Chat.CHATID} = ? OR ${Chat.MID} = ?', whereArgs: [element.id, element.mId]);
        }
        else{

          if(element.message != null && element.message.isNotEmpty) {
            dbModel.message =
            await DataEncrypt().decryptFromChat(element.senderId, element.message);
          }
          dbModel.mId = (element.mId != null) ? element.mId : element.id;
          dbModel.setLocalFile = '';

          var result = batch.insert(Chat.TABLE_NAME, dbModel.toJson());
          //var bathResult = batch.execute("INSERT INTO ${Chat.TABLE_NAME} (${Chat.CHATID}, ${Chat.MID}, ${Chat.CHANNEL_ID})VALUES (?,?,?) ",  dummyModel);
        }
      }
      else{

        if(element.message != null && element.message.isNotEmpty) {
          dbModel.message =
          await DataEncrypt().decryptFromChat(element.senderId, element.message);
        }
        dbModel.mId = (element.mId != null) ? element.mId : element.id;
        dbModel.setLocalFile = '';


        var result = batch.insert(Chat.TABLE_NAME, dbModel.toJson());
        //var bathResult = batch.execute("INSERT INTO ${Chat.TABLE_NAME} (${Chat.CHATID}, ${Chat.MID}, ${Chat.CHANNEL_ID})VALUES (?,?,?) ",  dummyModel);

      }

    });

    var result = await batch.commit();

//    String query1 = "SELECT * FROM ${Chat.TABLE_NAME} WHERE ${Chat.ROOT_ID} = '${element.rootId}'";
//    List<Map> chatMapq = await dbClient.rawQuery(query1);
//    print('my db value is ${chatMapq.length}');

    return null;
  }

  Future<EventObject> getSubChat(String channelId, String rootId) async {
    var dbClient = await DatabaseHelper.get()._getDb();
    List<Map> chatMap = await dbClient.rawQuery(
        "SELECT * FROM ${Chat.TABLE_NAME} WHERE ${Chat.CHANNEL_ID} = '$channelId' AND ${Chat.ROOT_ID} = '$rootId' ORDER BY ${Chat.CREATE_AT}");
    List<ChatDbModel> chats = new List();
    if (chatMap.isNotEmpty) {
      for (int i = 0; i < chatMap.length; i++) {
        chats.add(ChatDbModel.fromJson(chatMap[i]));
      }
      return EventObject(id: Events.READ_LOGS_SUCCESSFUL, object: chats);
    } else {
      return EventObject(id: Events.NO_CHATS);
    }
//    var result = await dbClient.rawInsert(
//        'INSERT INTO $tableNote ($columnTitle, $columnDescription) VALUES (\'${note.title}\', \'${note.description}\')');

    return null;
  }

  Future<int> saveRecentChatList (List<Data> data, String from) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    Batch batch = dbClient.batch();

    data.forEach((element) async {
      String msg;
      if(element.category == '1000' || element.category == '01') {
         msg = await DataEncrypt().decryptFromChat(
            element.senderId, element.message);
      }
      else{
        msg = element.message;
      }

      var result =  batch.rawInsert('INSERT OR REPLACE INTO ${RecentChat.TABLE_NAME} (${RecentChat.TITLE}, ${RecentChat.ICON}, ${RecentChat.CHANNELID}, '
          '${RecentChat.MESSAGE}, ${RecentChat.CREATEAT}, ${RecentChat.USERID}, ${RecentChat.UNREADCOUNT}, ${RecentChat.AVAILABLE}, '
          '${RecentChat.CATEGORY}, ${RecentChat.SENDERID}, ${RecentChat.ACCEPTSTATUS}, ${RecentChat.BLOCKSTATUS}, ${RecentChat.PHONENO}, ${RecentChat.PROFILEID}, ${RecentChat.ISPRIVATE}) '
          'VALUES (\"${element.title}\", \"${element.icon}\",\"${element.channelId}\",'
          '\"$msg\",\"${element.createAt}\",\"${element.userId}\",\"${element.unreadCount}\",\"${element.available}\"'
          ',\"${element.category}\",\"${element.senderId}\",\"${element.acceptStatus}\",\"${element.blockStatus}\",\"${element.phoneno}\",\"${element.profileId}\",\"$from\")');


    });

    dynamic result = await batch.commit();
    print('my result is $result');
    return null;
  }

  Future<List<Data>> getRecentChatList(String isPrivate) async{
    var dbClient = await DatabaseHelper.get()._getDb();
    String query = "SELECT * FROM ${RecentChat.TABLE_NAME} WHERE ${RecentChat.ISPRIVATE} = '$isPrivate' ORDER BY ${RecentChat.CREATEAT} DESC ";
    List<Map> data = await dbClient.rawQuery(query);
    List<Data> mdl = new List();

    for(int i = 0; i < data.length; i++){
      mdl.add(Data.fromJson(data[i]));
    }
    print("my data size is ${mdl.length}");
    return mdl;
  }

  Future<int> saveRecentGroupChat (List<Organisation> data) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    Batch batch = dbClient.batch();

    data.forEach((element) async {

      var result =  batch.rawInsert('INSERT OR REPLACE INTO ${GroupRecentChat.TABLE_NAME} (${GroupRecentChat.TITLE}, ${GroupRecentChat.ICON}, ${GroupRecentChat.LOCATION}, '
          '${GroupRecentChat.ID}) '
          'VALUES (\'${element.title}\', \'${element.icon}\',\'${element.location}\','
          '\'${element.id}\')');

      if(element.groups != null) {
        await saveChatGroup(element.groups, element.id);
      }

    });

    dynamic result = await batch.commit();
    print('my result is $result');
    return null;
  }

  Future<List<Organisation>> getRecentGroupChatList() async{
    var dbClient = await DatabaseHelper.get()._getDb();
    String query = "SELECT * FROM ${GroupRecentChat.TABLE_NAME} ";
    List<Map> data = await dbClient.rawQuery(query);
    List<Organisation> mdl = new List();

    for(int i = 0; i < data.length; i++){
      mdl.add(Organisation.fromJson(data[i]));
    }
    return mdl;
  }

  Future<int> saveChatGroup (List<Groups> data, String recentId) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    Batch batch = dbClient.batch();

    data.forEach((element) async {

      var result =  batch.rawInsert('INSERT OR REPLACE INTO ${ChatGroup.TABLE_NAME} (${ChatGroup.ID},${ChatGroup.TITLE}, ${ChatGroup.MEMBERSCOUNT}, ${ChatGroup.ICON}, '
          '${ChatGroup.UNREADCOUNT}, ${ChatGroup.ADMINNAME}, ${ChatGroup.ADMINICON}, ${ChatGroup.RECENTCHATID}, ${ChatGroup.ACCEPTSTATUS} )'
          'VALUES (\'${element.id}\', \'${element.title}\', \'${element.membersCount}\', \'${element.icon}\', '
          '\'${element.unreadCount}\', \'${element.adminName}\', \'${element.adminIcon}\', \'$recentId\', \'${element.acceptStatus}\')');

    });

    dynamic result = await batch.commit();
    return null;
  }

  /// ** Get groups list in chat ****/
  Future<List<Groups>> getGroupChatList(String recentId) async{
    var dbClient = await DatabaseHelper.get()._getDb();
    String query = "SELECT * FROM ${ChatGroup.TABLE_NAME} WHERE ${ChatGroup.RECENTCHATID} = '$recentId'";
    List<Map> data = await dbClient.rawQuery(query);
    List<Groups> mdl = new List();

    for(int i = 0; i < data.length; i++){
      mdl.add(Groups.fromJson(data[i]));
    }
    return mdl;
  }

  /// ** Contacts Saving to db****/
  Future<int> saveContacts (List<CData> data) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    Batch batch = dbClient.batch();

    data.forEach((element) async {

      var result =  batch.rawInsert('INSERT OR REPLACE INTO ${ContactDb.TABLE_NAME} (${ContactDb.ID},${ContactDb.TITLE}, ${ContactDb.ICON}, ${ContactDb.FIRSTNAME}, '
          '${ContactDb.PROFILEID})'
          'VALUES (\'${element.id}\', \'${element.title}\', \'${element.icon}\', \'${element.firstName}\', '
          '\'${element.profileid}\')');

    });

    dynamic result = await batch.commit();
    return null;
  }

  /// ** Get contacts list  ****/
  Future<List<CData>> getContactList() async{
    var dbClient = await DatabaseHelper.get()._getDb();
    String query = "SELECT * FROM ${ContactDb.TABLE_NAME} ";
    List<Map> data = await dbClient.rawQuery(query);
    List<CData> mdl = new List();

    for(int i = 0; i < data.length; i++){
      mdl.add(CData.fromJson(data[i]));
    }
    return mdl;
  }

  /// ** save groups senders colour  ****/
  Future<int> saveGroupUsersColour (String senderId) async {
    var dbClient = await DatabaseHelper.get()._getDb();

    Batch batch = dbClient.batch();
    int color = (math.Random().nextDouble() * 0xFFFFFF).toInt();

    List<Map> sendColor = await dbClient.rawQuery(
        "SELECT * FROM ${GroupSenderColor.TABLE_NAME} WHERE ${GroupSenderColor.SENDERID} = '$senderId' ");

    if (sendColor.isEmpty) {
      var result =  batch.rawInsert('INSERT OR REPLACE INTO ${GroupSenderColor.TABLE_NAME} (${GroupSenderColor.SENDERID},${GroupSenderColor.COLOR})'
          'VALUES (\'$senderId\', \'$color\')');
    }

    dynamic result = await batch.commit();
    return null;
  }

  /// ** delete group chats ****/
  Future<int> deleteGroupRecentChat() async {
    try {
      var dbClient = await DatabaseHelper.get()._getDb();
      await dbClient.transaction((txn) async {
        var batch = txn.batch();
        batch.delete(ChatGroup.TABLE_NAME);
        await batch.commit();
      });
    } catch (error) {
      throw Exception('DbBase.cleanDatabase: ' + error.toString());
    }
  }


  /// ** delete all chats related data while logout   ****/
  Future<int> deleteAllChats() async {
    try {
      var dbClient = await DatabaseHelper.get()._getDb();
      await dbClient.transaction((txn) async {
        var batch = txn.batch();
        batch.delete(Chat.TABLE_NAME);
        batch.delete(RecentChat.TABLE_NAME);
        batch.delete(GroupRecentChat.TABLE_NAME);
        batch.delete(ChatGroup.TABLE_NAME);
        batch.delete(ContactDb.TABLE_NAME);
        await batch.commit();
      });
    } catch (error) {
      throw Exception('DbBase.cleanDatabase: ' + error.toString());
    }
  }

  Future close() async {
    var dbClient = _database;
    didInit = false;
    return dbClient.close();
  }
}
