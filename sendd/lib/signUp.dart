
import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/model/countryModel.dart';
import 'package:sendd/model/signUpUserModel.dart';
import 'package:sendd/otpverification.dart';
import 'package:sendd/utils/constants.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/dialogs.dart';
import 'common/inputtextfield.dart';
import 'data/services.dart';

class SignUp extends StatefulWidget{
  SignUp({Key key}) : super(key : key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _SignUp();
  }

}

class _SignUp extends State<SignUp>{
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  SignUpUserModel model = SignUpUserModel();
  Data selectedCountry;
  Future<CountryModel> countryModel;
  List<Data> dropMediData;
  CommonModel signUpModel;
  String passwrd;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    countryModel = Services.getCountryCode();

  }

  Future<void> _submit() async{
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
        Dialogs.showLoadingDialog(context, _keyLoader);

        Map<String, dynamic> map = new Map();
        map['FirstName'] = model.firstName;
        map['LastName'] = model.lastName;
        map['Email'] = model.emailId;
        map['Phoneno'] = model.countryCode+model.mobileNo;
        map['Password'] = model.password;
        map['appxtnsn'] = 'sendd';


        String encodedJSON = jsonEncode(map);
        signUpModel =
        await Services.doUserRegistration(encodedJSON).catchError((error) {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          print(error);
        });
        if (signUpModel.status == "true") {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          _scaffoldKey.currentState.showSnackBar(
              new SnackBar(content: new Text(signUpModel.message)));
          Future.delayed(const Duration(milliseconds: 1000), () {
            setState(() {
              Navigator.pushReplacement(context, MaterialPageRoute(
                  builder: (BuildContext context) =>
                      OtpVerification(
                        phoneNmbr: model.countryCode + model.mobileNo,)));
            });
          });
        }
        else {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          _scaffoldKey.currentState.showSnackBar(
              new SnackBar(content: new Text(signUpModel.message)));
        }
      }

  }

  circularProgress() {
    return Center(
      child: const CircularProgressIndicator(),
    );
  }

  Widget _dropDownType(){
    return FutureBuilder<CountryModel>(
      future: countryModel,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text('Error ${snapshot.error}');
        }
        if (snapshot.hasData) {
          dropMediData = snapshot.data.data;
          return Container(
            margin: EdgeInsets.symmetric(vertical: 5),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Select Country ',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  DropdownButtonFormField<Data>(
                    isExpanded: true,
                    hint: new Text("Select Country"),
                    value: selectedCountry,
                    validator: (value){
                      if(value == null){
                        return "Select Country";
                      }
                      return null;
                    },
                    onChanged: (Data value){
                      setState(() {
                        selectedCountry = value;
                        model.countryCode = selectedCountry.isdcode;
                      });
                    },
                    items: dropMediData.map((Data mdl) {
                      return new DropdownMenuItem<Data>(
                        value: mdl,
                        child: new Text(mdl.name,
                            style: new TextStyle(color: Colors.black)),
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          );
      }
        return circularProgress();
      },
    );
  }

  Widget _submitButton() {
    return InkWell(
      onTap: _submit,
      child: Container(
        width: MediaQuery.of(context).size.width /3,
        height: 50.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: PrimaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [buttonGradientEnd, buttonGradientStart])
        ),
        child: Text(
          'Register',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('SignUp'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        color: Colors.white,
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  InputTextField(
                    hintText: 'First Name',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter First Name';
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      print(value);
                      model.firstName = value;
                    },
                    isEmail: false,
                    isPassword: false,
                    isLast: false,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  InputTextField(
                    hintText: 'Last Name',
                    onSaved: (String value) {
                      print(value);
                      model.lastName = value;
                    },
                    isEmail: false,
                    isPassword: false,
                    isLast: false,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  _dropDownType(),
                  InputTextField(
                    hintText: 'Email id',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter Email id';
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      model.emailId = value;
                    },
                    isEmail: true,
                    isPassword: false,
                    isLast: false,
                  ),
                  InputTextField(
                    hintText: 'Mobile Number',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter Mobile Number';
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      model.mobileNo = value;
                    },
                    isEmail: false,
                    isPassword: false,
                    isLast: false,
                  ),
                  InputTextField(
                    hintText: 'Password',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter Password';
                      }
                      else{
                        model.password = value;
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      passwrd = value;
                      model.password = value;
                    },
                    isEmail: false,
                    isPassword: true,
                    isLast: false,
                  ),
                  InputTextField(
                    hintText: 'Confirm Password',
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter Password';
                      }
                      if(value != model.password) {
                        return 'Password not matching';
                      }
                      return null;
                    },
                    onSaved: (String value) {
                      model.confirmPassword = value;
                    },
                    isEmail: false,
                    isPassword: true,
                    isLast: true,
                  ),
                  SizedBox(height: 20.0,),
                  _submitButton(),
                  SizedBox(height: 20.0,)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

}
