import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/dialogs.dart';

class OtpVerification extends StatefulWidget{
  OtpVerification({Key key, this.phoneNmbr}) : super(key: key);

  final String phoneNmbr;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OtpVerifyState(phoneNmbr);
  }

}

class _OtpVerifyState extends State<OtpVerification>{
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  TextEditingController controller1 = new TextEditingController();
  TextEditingController controller2 = new TextEditingController();
  TextEditingController controller3 = new TextEditingController();
  TextEditingController controller4 = new TextEditingController();
  TextEditingController controller5 = new TextEditingController();
  TextEditingController controller6 = new TextEditingController();

  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  CommonModel model;
  String phoneNmbr;

  _OtpVerifyState(this.phoneNmbr);

  Widget _submitButton() {
    return InkWell(
      onTap: _submit,
      child: Container(
        width: 150.0,
        height: 50.0,
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: PrimaryColor,
          borderRadius: BorderRadius.all(Radius.circular(40)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.shade200,
                offset: Offset(2, 4),
                blurRadius: 5,
                spreadRadius: 2)
          ],
        ),
        child: Text(
          'Submit',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Future<void> _submit() async {
  if(controller1.text.isEmpty && controller2.text.isEmpty && controller3.text.isEmpty && controller4.text.isEmpty && controller5.text.isEmpty && controller6.text.isEmpty){
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(content: new Text('Fill all the fields')));
  }
  else{
    String otp = controller1.text+controller2.text+controller3.text+controller4.text+controller5.text+controller6.text;
    Dialogs.showLoadingDialog(context, _keyLoader);
    model = await Services.doOTPVerify(phoneNmbr, otp).catchError((error){
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      print(error);});

    if(model.status == "true"){
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(model.message)));
      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          Navigator.pop(context);
        });
      });
    }
    else{
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(model.message)));
    }
  }

  }


  Widget otpField(BuildContext context, TextEditingController controller){
    return Padding(
      padding: const EdgeInsets.only(right: 2.0, left: 2.0),
      child: new Container(
        width: 40.0,
          alignment: Alignment.center,
          decoration: new BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.1),
              border: new Border.all(
                  width: 1.0,
                  color: Color.fromRGBO(0, 0, 0, 0.1)
              ),
              borderRadius: new BorderRadius.circular(4.0)
          ),
          child: new TextField(
            inputFormatters: [
              LengthLimitingTextInputFormatter(1),
            ],
            textInputAction: TextInputAction.next,
            onEditingComplete: () => FocusScope.of(context).unfocus(),
            keyboardType: TextInputType.number,
            enabled: true,

            controller: controller,
            autofocus: true,
            textAlign: TextAlign.center,
            onSubmitted: (_) => FocusScope.of(context).nextFocus(),
            style: TextStyle(fontSize: 24.0, color: Colors.black),

          )

      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('OTP'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),),
      body: Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            color: Colors.white,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 10.0,),
              Image.asset("assets/images/img_otp_sav.png"),
              SizedBox(height: 10.0,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text('Enter OTP', style: TextStyle(fontSize: 20.0, color: PrimaryColor, fontWeight: FontWeight.bold),)),
              ),
              SizedBox(height: 10.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  otpField(context, controller1),
                  otpField(context, controller2),
                  otpField(context, controller3),
                  otpField(context, controller4),
                  otpField(context, controller5),
                  otpField(context, controller6)
                ],
              ),
              SizedBox(height: 15.0,),
              GestureDetector(
                  onTap: (){
                    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => SignUp()));
                  },
                  child: Center(child: Text('Resend OTP', style: TextStyle(fontSize:20.0, color: PrimaryColor, decoration: TextDecoration.underline),))
              ),
              SizedBox(height: 20.0,),
              _submitButton()
            ],
          )
        ],
      ),
    );



  }

  @override
  void dispose() {
    super.dispose();
    controller1.dispose();
    controller2.dispose();
    controller3.dispose();
    controller4.dispose();
    controller5.dispose();
    controller6.dispose();
  }

/*

  void matchOtp() {
    showDialog(context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Successfully"),
            content: Text("Otp matched successfully."),
            actions: <Widget>[IconButton(
                icon: Icon(Icons.check), onPressed: () {
              Navigator.of(context).pop();
            })
            ],);
        }
    );
  }
*/




}