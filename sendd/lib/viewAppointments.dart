import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/CompletedAppointmentList.dart';
import 'package:sendd/pendingAppointments.dart';
import 'package:sendd/utils/custColors.dart';

class ViewAppointments extends StatelessWidget{
  ViewAppointments({Key key, this.mrn, this.clinicId, this.userId, this.token}) : super(key: key);

  final mrn;
  final clinicId;
  final userId;
  final token;

  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: 'Pending'),
                Tab(text: 'Completed')
              ],
            ),
            title: Text('View Appointments'),
            centerTitle: true,
            backgroundColor: PrimaryColor,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              PendingAppointments(mrn: mrn, clinicId: clinicId,),
              CompletedAppointmentList(mrn: mrn, clinicId: clinicId, token: token, userId: userId,)
            ],
          ),
        ),
      ),
    );
  }

}
