import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/common/dialogs.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/utils/custColors.dart';

import 'data/services.dart';
import 'model/forgotPasswordModel.dart';

class ForgotPassword extends StatelessWidget{

  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _usernameController = TextEditingController();
  final _countryCodeController = TextEditingController(text: "91");

  Future<void> _submit(BuildContext context) async{
    ForgotPasswordModel model;
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Dialogs.showLoadingDialog(context, _keyLoader);

      model =
      await Services.doForgotPassword(_countryCodeController.text+_usernameController.text).catchError((error) {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        print(error);
      });
      if (model.data.status == "true") {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text(model.data.message)));
        Future.delayed(const Duration(milliseconds: 2000), () {
            Navigator.of(context).pop();
        });
      }
      else {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text(model.data.message)));
      }
    }

  }

  Widget _entryFieldPhoneNumber() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Mobile Number ",
              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15),
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: TextFormField(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      textAlign: TextAlign.center,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter country code';
                        }
                        return null;
                      },
                      controller: _countryCodeController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        focusColor: PrimaryColor,
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)
                        ),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color : Colors.black)
                        ),
                      )),
                ),
                SizedBox(width: 10.0,),
                Expanded(
                  flex: 5,
                  child: TextFormField(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter Mobile Number';
                        }
                        return null;
                      },
                      controller: _usernameController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        focusColor: PrimaryColor,
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)
                        ),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color : Colors.black)
                        ),
                      )),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _submitButton(BuildContext context) {
    return InkWell(
      onTap: ()=> _submit(context),
      child: Container(
        width: MediaQuery.of(context).size.width /3,
        height: 50.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: PrimaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [buttonGradientEnd, buttonGradientStart])
        ),
        child: Text(
          'Submit',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('Forgot Password'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          height: double.infinity,
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/images/forgot_password.png', height: MediaQuery.of(context).size.height/4,),
              SizedBox(height: 30.0,),
              _entryFieldPhoneNumber(),
              SizedBox(height: 20.0,),
              _submitButton(context)
            ],
          ),
        ),
      ),
    );
  }

}