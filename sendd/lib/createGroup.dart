import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/createGroupProfile.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/circularProgress.dart';
import 'data/databaseHelper.dart';
import 'data/services.dart';
import 'model/contactsModel.dart';

class CreateGroup extends StatefulWidget{
  CreateGroup({Key key, this.id}) : super(key: key);

  final id;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CreateGroupState();
  }

}

class _CreateGroupState extends State<CreateGroup>{
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<List<CData>> model;
  var db = DatabaseHelper();
  
  List selectedContact = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocalData();
    syncData();
  }

  Future<void> syncData() async{
    var result = await Services.getUserContacts();
    if(result.data.length > 0){
      print('total length is ${result.data.length}');
      var result1 = await db.saveContacts(result.data);
      getLocalData();
    }
  }

  void getLocalData() async {
    setState(() {
      model = db.getContactList();
    });
  }

  void _onSelected(bool selected, String id) {
    if (selected == true) {
      setState(() {
        selectedContact.add(id);
      });
    } else {
      setState(() {
        selectedContact.remove(id);
      });
    }
  }

  void _onSubmit(){
    if(selectedContact.length>0){
      Navigator.push(context, MaterialPageRoute(
          builder: (BuildContext context) =>
              CreateGroupProfile(selectedContact: selectedContact, id: widget.id,)));
    }
    else{
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text('Please select group members')));
    }
  }

  Widget _myListView(
      BuildContext context, AsyncSnapshot<List<CData>> snapshot) {
    return Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: snapshot.data.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      ClipOval(child: CachedNetworkImage(imageUrl: snapshot.data[index].icon, width: 50.0, height: 50.0, fit:BoxFit.cover,),),
                      SizedBox(width: 10.0,),
                      Text(snapshot.data[index].firstName)
                    ],
                  ),
                  Checkbox(
                    value: selectedContact.contains(snapshot.data[index].id),
                    activeColor: PrimaryColor,
                    onChanged: (bool value) {
                      _onSelected(value, snapshot.data[index].id);
                    }
                  )
                ],
              ),
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('Add Members'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
        actions: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(right:8.0),
              child: InkWell(
                onTap: ()=> _onSubmit(),
                child: Text('Next', style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),),
              ),
            ),
          )
        ],
      ),
      body: Container(
          height: double.infinity,
          child: FutureBuilder<List<CData>>(
              future: model,
              builder: (context, snapshot){
                if(snapshot.hasError){
                  return Text('Error ${snapshot.error}');
                }
                if(snapshot.hasData){
                  return Stack(
                    children: <Widget>[
                      Container(
                        height: double.infinity,
                        color: Colors.white,
                      ),
                      _myListView(context, snapshot)
                    ],
                  );
                }
                return CircularProgress();
              }
          )
      ),
    );
  }

}