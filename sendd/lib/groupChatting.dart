import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sendd/common/chatPdfLeftBubble.dart';
import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/groupInfo.dart';
import 'package:sendd/model/eventObject.dart';
import 'package:sendd/presenter/chatPresenter.dart';
import 'package:sendd/presenter/chatPresenterIView.dart';
import 'package:sendd/utils/constants.dart';
import 'package:sendd/utils/custColors.dart';
import 'package:sendd/utils/customDialog.dart';
import 'package:sendd/utils/dataEncrypt.dart';
import 'package:sendd/utils/imagePreviewPage.dart';
import 'package:web_socket_channel/io.dart';

import 'common/chatImageLeftBubble.dart';
import 'common/chatImageRightBubble.dart';
import 'common/chatPdfRightBubble.dart';
import 'common/chatTextLeftBubble.dart';
import 'common/chatTextRightBubble.dart';
import 'common/chatThreadRightBubble.dart';
import 'data/services.dart';
import 'model/ChatDbModel.dart';
import 'model/chatMessageModel.dart';
import 'dart:math' as math;

import 'model/contactsModel.dart';

class GroupChatting extends StatefulWidget{
  GroupChatting({Key key, this.channelId, this.grpTitle, this.userID, this.token, this.grpIcon})
      : super(key: key);

  final String channelId;
  final String grpTitle;
  final String userID;
  final String token;
  final String grpIcon;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GroupChattingState(channelId, userID, token);
  }

}

class _GroupChattingState extends State<GroupChatting>{
  List<ChatDbModel> model = [];
  String _currentMessage;
  final TextEditingController _textController = TextEditingController();
  final ScrollController _controller = ScrollController();
  var db = DatabaseHelper();
  String userID;
  String token;
  Timer timer;
  File _image;
  final picker = ImagePicker();
  ChatPresenter presenter;
  List<CData> myData = List();

  String channelId;
  bool reCall = true;


  _GroupChattingState( this.channelId, this.userID, this.token);

  @override
  void initState() {
    super.initState();
      getLocalChat();
      getChats();
      timer = Timer.periodic(
          Duration(seconds: 10), (Timer t) => reCall ? getChats() : null);
  }


  Future getImage() async {
//    var result = await FilePickingUtils().getImage(context);
//    print('my picking result is $result');
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    _image = File(pickedFile.path);
    final result = await Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ImagePreviewPage(imageFile: _image,)));
    print('result from $result');
    if(result != null){
      if(result != '1002')
      saveImageToLocaldatabase(result, '1001');
    }

  }

  Future saveImageToLocaldatabase(String msg, String category) async {

    if(_image != null){
      final Directory directory = await getApplicationDocumentsDirectory();
      String path = directory.path;
      String fileName = _image.path.split('/').last;
      String fullFileName = '$path/$fileName';
      final File localImage = await _image.copy('$fullFileName');
      if(msg == "1001"){
        msg = "";
      }
      saveChatToDB( msg, category, fullFileName, fileName);
    }

  }

  Future getImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    _image = File(pickedFile.path);
    final result = await Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ImagePreviewPage(imageFile: _image,)));
    print('result from $result');
    if(result != null){
      saveImageToLocaldatabase(result, '1001');
    }
  }

  Future getAttachments() async {
    _image = await FilePicker.getFile(type: FileType.custom, allowedExtensions: ['pdf']);
    savePdfToLocal();
  }

  Future<void> savePdfToLocal() async{

    final Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path;
    String fileName = _image.path.split('/').last;
    String fullFileName = '$path/$fileName';
    File localFile = File(fullFileName);
    await localFile.writeAsBytes(_image.readAsBytesSync(), flush: true);
    saveChatToDB('', '1006', fullFileName, fileName);

  }

  Future getAudio() async {

    File file = await FilePicker.getFile(type: FileType.custom, allowedExtensions: ['wav']);

  }

  Future<void> getChats() async {
    reCall = false;
    var result = await Services.getChat(channelId, token);
    reCall = result != null ? true : false;
    if(result.data.length > 0) {
      var result1 = await db.saveChatFromServer(result.data, channelId, false);
      getLocalChat();
    }
  }

  Future<void> getLocalChat() async {
    EventObject eventObject = await db.getChat(channelId, '');
    if (this.mounted) {
      switch (eventObject.id) {
        case Events.READ_LOGS_SUCCESSFUL:
            setState(() {
              model = eventObject.object;
              print("my length is ${model.length}");
              _controller.jumpTo(_controller.position.maxScrollExtent);
            });
          break;

        case Events.NO_CHATS:
          break;
      }
    }
  }

  Future<void> saveChatToDB(String currentMessage, String category, String localpath, String fileName) async {

    ChatMessageModel value = new ChatMessageModel();

    value.senderId = userID;
    value.message = currentMessage;
    value.category = category;
    value.rootId = '';
    value.noOfReply = 0;
    value.setLocalFile = localpath;
    value.channelId = channelId;
    value.createAt = new DateTime.now().millisecondsSinceEpoch;
    value.mId = new DateTime.now().millisecondsSinceEpoch.toString();
    value.id = value.mId;
    value.attachmentName = fileName;

    dynamic result1 = await db.saveChatAfterPost(value, userID);
    getLocalChat();
    print('local saving data $result1');

    if(category == '1000') {
      var result = await Services.postMessage(channelId, currentMessage, category, value.mId, token, userID);
      if (result.status == "true") {
        var result1 = await db.saveChatAfterPost(result.data, userID);
        getLocalChat();
      }
    }
    else if(category == '1001'){
      var bytes = await _image.readAsBytes();
      sendImageToWebSocket(bytes, value);
    }
    else if(category == '1006'){
      var bytes = await _image.readAsBytes();
      sendImageToWebSocket(bytes, value);
    }
  }

  Future<void> sendImageToWebSocket(Uint8List bytes, ChatMessageModel value) async {
    var channel = await IOWebSocketChannel.connect("ws://125.99.242.202:6032/filedata");
    channel.sink.add(bytes);
    channel.stream.listen((message) async {
      print('from websocket $message');
      if(message.toString().contains('http'))
      {
        value.filenames = message;
        var result = await Services.postImageAndMessage(value, token, userID);
        channel.sink.close();
        if (result.status == "true") {
          var result1 = await db.saveChatAfterPost(result.data, userID);
          setState(() {
            getLocalChat();
          });
        }
      }
    });
  }


  void onSubChatClick(ChatDbModel model){
    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => SubChatMessage(msgModel: model,)));
  }

  void onATMention(){
    String value = _textController.text;
    int fd = value.indexOf('@');
    print('my index is $fd');
    //print(value.substring(0,fd));

    if(value.contains('@')) {
      if (value.length == 1) {
        print('am singele');
      }
      else if(value.contains(" @") && ((value.length) - 1 == value.indexOf("@"))) {
        print('my before char ');
        CData dt = new CData();
        dt.title = 'sasa';

        setState(() {
          myData.add(dt);
        });

      }
      else{
        print('my lenght ${value.length}');
        print('my index ${value.indexOf("@")}');
      }
    }

}

  Widget _myListView(List<CData> myData) {
    return Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: myData.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                ListTile(
                  onTap: (){
                    //createChannelId(snapshot.data.data[index].id);
                  },
                  leading: ClipOval(child: CachedNetworkImage(imageUrl: "https://picsum.photos/250?image=9", width: 50.0, height: 50.0, fit:BoxFit.cover,),),
                  title: (myData[index].title == null) ? Text('') : Text(myData[index].title),
                  trailing: Icon(Icons.chat_bubble_outline, color: PrimaryColor,),
                ),
                Divider(),
              ],
            );
          }),
    );
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (builder) {
          return new Container(
            height: 150.0,
            color: Colors.transparent, //could change this to Color(0xFF737373),
            //so you don't have to change MaterialApp canvasColor
            child: new Container(
                child: new Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            onTap: (){
                              Navigator.pop(context);
                              getImage();
                            },
                            child: CircleAvatar(
                              child: Icon(Icons.photo, color: Colors.white,),
                              backgroundColor: PrimaryColor,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                          ),
                          SizedBox(height: 10.0,),
                          Text('Gallery')
                        ],
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                          getImageFromCamera();
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              child: Icon(Icons.camera_alt, color: Colors.white,),
                              backgroundColor: PrimaryColor,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                            SizedBox(height: 10.0,),
                            Text('Camera')
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                          getAttachments();
                          //sendImageToWebSocket();
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              child: Icon(Icons.attach_file, color: Colors.white,),
                              backgroundColor: PrimaryColor,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                            SizedBox(height: 10.0,),
                            Text('PDF')
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                          getAudio();
                          //sendImageToWebSocket();
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              child: Icon(Icons.audiotrack, color: Colors.white,),
                              backgroundColor: PrimaryColor,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                            SizedBox(height: 10.0,),
                            Text('Audio')
                          ],
                        ),
                      )
                    ],
                  ),
                )),
          );
        });
  }



  Expanded messageList() {
    return Expanded(
      flex: 2,
      child: ListView.builder(
          padding: EdgeInsets.all(8.0),
          controller: _controller,
          itemCount: model.length,
          itemBuilder: (context, index) {
            if(index>0){
              //checkMsgDate(index);
            }
            return buildTile(model[index]);
          }),
    );
  }

  Padding messageInputArea() {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 16.0,
        top: 16.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: PrimaryColor,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      //pickImageFromGallery(ImageSource.gallery);
                      _modalBottomSheetMenu();
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Column(
              children: [
                _myListView(myData),
                TextField(
                  controller: _textController,
                  onChanged: (value) {
                    onATMention();
                    _currentMessage = value;
                  },
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 4,
                  cursorColor: Colors.black,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Enter Message",
                    hintStyle: TextStyle(color: Colors.black),
                    labelStyle: TextStyle(color: Colors.black),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 2.0,
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: PrimaryColor,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      _textController.clear();
                      //_cometChat.sendMessage(userMessage: _currentMessage);
                      saveChatToDB(_currentMessage, '1000','', '');
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: InkWell(
          onTap: ()=> Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => GroupInformation(groupName: widget.grpTitle, channelId: channelId,))),
          child: Text(
            widget.grpTitle,
          ),
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Container(
        child: SafeArea(
          child: Column(
            children: <Widget>[messageList(), messageInputArea(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTile(ChatDbModel model) {

    DateFormat dateFormat = DateFormat("hh:mm a");
    var dt = dateFormat.format(new DateTime.fromMillisecondsSinceEpoch(model.createAt));

    switch (model.category){
      case '1000'://( normal text message category 1000)
        if(userID == model.senderId){
          //return ChatThreadRightBubble();
          if(model.noOfReply>0) {
            return ChatThreadRightBubble(model: model, time: dt);
          }
          else {
            return ChatTextRightBubble(
              model: model, time: dt, onSubChat: onSubChatClick,);
          }
        }
        else {
          return ChatTextLeftBubble(model: model, time: dt, from: true,);
        }
        break;

      case '1001': // (Image Category 1001)
        if(userID == model.senderId){
          return ChatImageRightBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames, status: model.msgReadBy, time: dt,);
        }
        else {
          return ChatImageLeftBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames, status: model.msgReadBy, time: dt,);
        }
        break;

      case '1006': // (PDF Category 1006)
        if(userID == model.senderId){
          return ChatPdfRightBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames,fileName: model.attachmentName,);
        }
        else {
          return ChatPdfLeftBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames, fileName: model.attachmentName,);
        }
        break;
    }

  }

  void checkMsgDate(int index){
    var status;
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    DateTime indexDate = new DateTime.fromMillisecondsSinceEpoch(index);
    DateTime previousDate = new DateTime.fromMillisecondsSinceEpoch(model[index-1].createAt);
    int current = new DateTime.now().millisecondsSinceEpoch;
    DateTime cDate = new DateTime.fromMillisecondsSinceEpoch(current);
    var difference = indexDate.difference(previousDate).inDays;
    if(difference != 0){
      int nDate = cDate.difference(indexDate).inDays;

      if(nDate == 0){
        status = 'Today';
      }
      else if( nDate == 1){
        status = 'Yesterday';
      }
      else{
        status = dateFormat.format(indexDate);
      }

    }
    else{
      status = ' same day';
    }

    print('my date is $status');

  }

  @override
  void dispose() {
    timer.cancel();
    db.close();
    super.dispose();
  }

  @override
  void onRefreshChat(EventObject eventObject) {
    // TODO: implement onRefreshChat
    if (this.mounted) {
      switch (eventObject.id) {
        case Events.READ_LOGS_SUCCESSFUL:
          setState(() {
            model = eventObject.object;
            _controller.jumpTo(_controller.position.maxScrollExtent);
          });
          break;

        case Events.NO_CHATS:
          break;
      }
    }
  }
/*  showDialog(
  context: context,
  builder: (BuildContext context) => CustomDialog(
  title: widget.grpTitle,
  description:
  "${widget.adminName} wants to add you to this group",
  buttonTextAccept: "Join",
  buttonTextDeny: "Cancel",
  image: widget.grpIcon,
  ),
  )*/

}