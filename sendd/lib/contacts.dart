import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/contactsModel.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/circularProgress.dart';
import 'data/databaseHelper.dart';
import 'data/services.dart';
import 'doctorPatientChat.dart';
import 'model/createChannelIdModel.dart';

class Contacts extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ContactState();
  }

}

class _ContactState extends State<Contacts>{

  Future<List<CData>> model;
  var db = DatabaseHelper();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocalData();
    syncData();
  }

  Future<void> syncData() async{
      var result = await Services.getUserContacts();
      if(result.data.length > 0){
        print('total length is ${result.data.length}');
        var result1 = await db.saveContacts(result.data);
        getLocalData();
    }
  }

  void getLocalData() async {
    setState(() {
      model = db.getContactList();
    });

  }

  void createChannelId(id) async{

    String userId = await SharedPreferencesHelper.getUserId();
    String token = await SharedPreferencesHelper.getToken();
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();

    SharedPreferencesHelper.setChannelId(dynamicUrl);

    CreateChannelIdModel cModel = await Services.createChannelIdForConverse(id);
    if(cModel.data.length>0) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  DoctorPatientChat(
                    channelId: cModel.data[0].id,
                    doctorName: cModel.data[0].title,
                    userID: userId,
                    token: token,
                  )));
    }

  }

  Widget _myListView(
      BuildContext context, AsyncSnapshot<List<CData>> snapshot) {
    return Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: snapshot.data.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                ListTile(
                  onTap: (){
                    createChannelId(snapshot.data[index].id);
                  },
                  leading: ClipOval(child: CachedNetworkImage(imageUrl: snapshot.data[index].icon, width: 50.0, height: 50.0, fit:BoxFit.cover,),),
                  title: (snapshot.data[index].title == null) ? Text('') : Text(snapshot.data[index].title),
                  trailing: Icon(Icons.chat_bubble_outline, color: PrimaryColor,),
                ),
                Divider(),
              ],
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('Contacts'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          child: FutureBuilder<List<CData>>(
              future: model,
              builder: (context, snapshot){
                if(snapshot.hasError){
                  return Text('Error ${snapshot.error}');
                }
                if(snapshot.hasData){
                  return Stack(
                    children: <Widget>[
                      Container(
                        height: double.infinity,
                        color: Colors.white,
                      ),
                      _myListView(context, snapshot)
                    ],
                  );
                }
                return CircularProgress();
              }
          )
      ),
    );
  }



}