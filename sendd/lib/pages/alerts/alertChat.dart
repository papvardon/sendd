import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sendd/common/chatImageLeftBubble.dart';
import 'package:sendd/common/chatImageRightBubble.dart';
import 'package:sendd/common/chatPdfLeftBubble.dart';
import 'package:sendd/common/chatPdfRightBubble.dart';
import 'package:sendd/common/chatTextLeftBubble.dart';
import 'package:sendd/common/chatTextRightBubble.dart';
import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/ChatDbModel.dart';
import 'package:sendd/model/eventObject.dart';
import 'package:sendd/utils/constants.dart';
import 'package:sendd/utils/custColors.dart';

class AlertChat extends StatefulWidget{
  AlertChat({Key key, this.channelId})
      : super(key: key);

  final channelId;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AlertChatState();
  }

}

class _AlertChatState extends State<AlertChat>{

  List<ChatDbModel> model = [];
  String _currentMessage;
  final TextEditingController _textController = TextEditingController();
  final ScrollController _controller = ScrollController();
  var db = DatabaseHelper();
  String userID;
  Timer timer;

  String patientName;
  bool reCall = true;

  @override
  void initState() {
    super.initState();
    getUserId();
    getLocalChat();
    getChats();
    timer = Timer.periodic(Duration(seconds: 10), (Timer t) => reCall ? getChats() : null);
  }

  Future<void> getUserId() async {
    userID = await SharedPreferencesHelper.getUserId();
  }

  Future<void> getChats() async {
    reCall = false;
    var result = await Services.getChatForAlert(widget.channelId);
    reCall = result != null ? true : false;
    if(result.data.length > 0) {
      var result1 = await db.saveChatFromServer(result.data, widget.channelId, true);
      print('my saving resilt is $result1');
      getLocalChat();
      print('from result get chat $result.toString()');
    }
  }

  Future<void> getLocalChat() async {
    EventObject eventObject = await db.getChat(widget.channelId, '');
    if (this.mounted) {
      switch (eventObject.id) {
        case Events.READ_LOGS_SUCCESSFUL:
          setState(() {
            model = eventObject.object;
            _controller.jumpTo(_controller.position.maxScrollExtent);
          });
          break;

        case Events.NO_CHATS:
          break;
      }
    }
  }

  void onSubChatClick(ChatDbModel model){
    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => SubChatMessage(msgModel: model,)));
  }

  Expanded messageList() {
    return Expanded(
      flex: 2,
      child: ListView.builder(
          padding: EdgeInsets.all(8.0),
          controller: _controller,
          itemCount: model.length,
          itemBuilder: (context, index) {
            if(index>0){
              //checkMsgDate(index);
            }
            return buildTile(model[index]);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          "Alerts",
        ),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Container(
        child: SafeArea(
          child: messageList(),
        ),
      ),
    );
  }

  Widget buildTile(ChatDbModel model) {

    DateFormat dateFormat = DateFormat("hh:mm a");
    var dt = dateFormat.format(new DateTime.fromMillisecondsSinceEpoch(model.createAt));

    switch (model.category){
      case '1000'://( normal text message category 1000)
        if(userID == model.senderId){
          //return ChatThreadRightBubble();
            return ChatTextRightBubble(model: model, time: dt, onSubChat: onSubChatClick,);
        }
        else {
          return ChatTextLeftBubble(model: model, time: dt, from: false,);
        }
        break;

      case '1001': // (Image Category 1001)
        if(userID == model.senderId){
          return ChatImageRightBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames, status: model.msgReadBy, time: dt,);
        }
        else {
          return ChatImageLeftBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames, status: model.msgReadBy, time: dt,);
        }
        break;

      case '1006': // (PDF Category 1006)
        if(userID == model.senderId){
          return ChatPdfRightBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames,fileName: model.attachmentName,);
        }
        else {
          return ChatPdfLeftBubble(filePathLocal: model.setLocalFile, filePathServer : model.filenames, fileName: model.attachmentName,);
        }
        break;
    }

  }


}