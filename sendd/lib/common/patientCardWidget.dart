import 'package:flutter/cupertino.dart';
import 'package:sendd/model/cardFrontSideModel.dart';

class PatientCardWidget extends StatelessWidget{
  PatientCardWidget(this.context, this.model);

  @required
  final CardFrontSideModel model;
  final BuildContext context;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
      child: Stack(
        children: <Widget>[
          Image.network(
            model.bg,
            fit: BoxFit.fill,
            width: 1000.0,
            height: 200.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Image.network(
                              model.contentText.photo,
                          width: 40.0,
                          height: 40.0,
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(model.contentText.name),
                            Text(model.contentText.mobile)
                          ],
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                model.contentText.title2,
                                textAlign: TextAlign.left,
                              ),
                              SizedBox(
                                width: 8.0,
                              ),
                              Text(model.contentText.value2),
                            ],
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                model.contentText.title2,
                                textAlign: TextAlign.left,
                              ),
                              SizedBox(
                                width: 8.0,
                              ),
                              Text(model.contentText.value2),
                            ],
                          )),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

}