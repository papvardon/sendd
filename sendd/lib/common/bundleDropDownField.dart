import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/model/dropDownValueModel.dart';

class BundleDropDownField extends StatefulWidget {

  final List<DropDownValueModel> data;
  final Function onSaved;
  final String heading;

  BundleDropDownField({this.data, this.onSaved, this.heading});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DropState(data, onSaved, heading);
  }

}

class _DropState extends State<BundleDropDownField> {

  DropDownValueModel selectedName;
  List<DropDownValueModel> data;
  Function onSaved;
  String heading;

  _DropState(this.data, this.onSaved, this.heading);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              heading,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
            DropdownButton<DropDownValueModel>(
              isExpanded: true,
              hint: new Text("Select $heading"),
              value: selectedName,
              onChanged: (DropDownValueModel value){
                onSaved(value.value);
                setState(() {
                  selectedName = value;
                });
              },
              items: data.map((DropDownValueModel mdl) {
                return new DropdownMenuItem<DropDownValueModel>(
                  value: mdl,
                  child: new Text(mdl.heading,
                      style: new TextStyle(color: Colors.black)),
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }

}

