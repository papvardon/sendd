import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ChatInputArea extends StatelessWidget {
  ChatInputArea(this.onSaved, this.context);

  final Function onSaved;
  final BuildContext context;

  String _currentMessage;
  TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 16.0,
        top: 16.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: Colors.blue,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      //pickImageFromGallery(ImageSource.gallery);
                      _modalBottomSheetMenu();
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: TextField(
              controller: _textController,
              onChanged: (value) {
                _currentMessage = value;
              },
              keyboardType: TextInputType.multiline,
              minLines: 1,
              maxLines: 4,
              cursorColor: Colors.black,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                border: new OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                hintText: "Enter Message",
                hintStyle: TextStyle(color: Colors.black),
                labelStyle: TextStyle(color: Colors.black),
              ),
            ),
          ),
          SizedBox(
            width: 2.0,
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: Colors.blue,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      _textController.clear();
                      //_cometChat.sendMessage(userMessage: _currentMessage);
                      //saveChatToDB(_currentMessage, '1000','', '');
                      onSaved(_currentMessage);
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (builder) {
          return new Container(
            height: 150.0,
            color: Colors.transparent, //could change this to Color(0xFF737373),
            //so you don't have to change MaterialApp canvasColor
            child: new Container(
                child: new Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            onTap: (){
                              Navigator.pop(context);
                              //getImage();
                            },
                            child: CircleAvatar(
                              child: Icon(Icons.photo, color: Colors.white,),
                              backgroundColor: Colors.blue,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                          ),
                          SizedBox(height: 10.0,),
                          Text('Gallery')
                        ],
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                          //getImageFromCamera();
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              child: Icon(Icons.camera_alt, color: Colors.white,),
                              backgroundColor: Colors.blue,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                            SizedBox(height: 10.0,),
                            Text('Camera')
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                          //getAttachments();
                          //sendImageToWebSocket();
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              child: Icon(Icons.attach_file, color: Colors.white,),
                              backgroundColor: Colors.blue,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                            SizedBox(height: 10.0,),
                            Text('PDF')
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                          //getAudio();
                          //sendImageToWebSocket();
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              child: Icon(Icons.audiotrack, color: Colors.white,),
                              backgroundColor: Colors.blue,
                              foregroundColor: Colors.grey,
                              radius: 25.0,
                            ),
                            SizedBox(height: 10.0,),
                            Text('Audio')
                          ],
                        ),
                      )
                    ],
                  ),
                )),
          );
        });
  }

}

/*
class _ChatInputState extends State<ChatInputArea> {

  String _currentMessage;
  final TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 16.0,
        top: 16.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: Colors.blue,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      //pickImageFromGallery(ImageSource.gallery);
                      //_modalBottomSheetMenu();
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: TextField(
              controller: _textController,
              onChanged: (value) {
                _currentMessage = value;
              },
              keyboardType: TextInputType.multiline,
              minLines: 1,
              maxLines: 4,
              cursorColor: Colors.black,
              style: TextStyle(color: Colors.black),
              decoration: InputDecoration(
                border: new OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                hintText: "Enter Message",
                hintStyle: TextStyle(color: Colors.black),
                labelStyle: TextStyle(color: Colors.black),
              ),
            ),
          ),
          SizedBox(
            width: 2.0,
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: ClipOval(
                child: Material(
                  color: Colors.blue,
                  child: InkWell(
                    splashColor: Colors.red,
                    onTap: () {
                      _textController.clear();
                      //_cometChat.sendMessage(userMessage: _currentMessage);
                      saveChatToDB(_currentMessage, '1000','', '');
                    },
                    child: SizedBox(
                      height: 40.0,
                      width: 40.0,
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

}*/
