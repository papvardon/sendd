import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BundleInputTextField extends StatelessWidget{

  final String heading;
  final Function onSaved;
  final bool editable;
  final String value;

  BundleInputTextField({
    this.heading,
    this.onSaved,
    this.editable,
    this.value
  });
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              heading,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
            TextFormField(
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color : Colors.grey)
                ),
              ),
              keyboardType: TextInputType.text,
              onChanged: onSaved,
              onSaved: onSaved,
              enabled: editable,
              initialValue: value,
            )
          ],
        ),
      ),
    );
  }
  
}