import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyScreen extends StatelessWidget{
  EmptyScreen({Key key, this.message}) : super(key: key);

  final String message;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
              radius: 60.0,
              backgroundColor: Colors.indigo[400],
              child: Image.asset("assets/images/checklist.png", height: 50.0,),),
          SizedBox(height: 10.0,),
          Text(message, style: TextStyle(fontSize: 18.0, color: Colors.black),)
        ],
      ),
    );
  }

}