import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/model/ChatDbModel.dart';

import 'chatTimeAndReadStatus.dart';

class ChatTextRightBubble extends StatelessWidget{
  ChatTextRightBubble({Key key, this.model, this.time, this.onSubChat}) : super(key: key);

  final ChatDbModel model;
  final String time;
  final Function onSubChat;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onLongPress: () async {
        final position = buttonMenuPosition(context);
          final result = await showMenu(
            context: context,
            position: position,
            items: <PopupMenuEntry>[
              PopupMenuItem(
                value: '12',
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: (){
                        Navigator.pop(context);
                        onSubChat(model);
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.reply, color: Colors.grey,),
                          SizedBox(width: 10.0,),
                          Text("Reply"),
                        ],
                      ),
                    ),
                   /* Row(
                      children: <Widget>[
                        Icon(Icons., color: Colors.grey,),
                        SizedBox(width: 10.0,),
                        Text("Forward"),
                      ],
                    )*/
                  ],
                ),
              )
            ],
          );

      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Bubble(
              shadowColor: Colors.blue,
              elevation: 2,
              alignment: Alignment.topRight,
              nip: BubbleNip.rightTop,
              color: Color.fromARGB(255, 225, 255, 199),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                  Text(
                  model.message,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                  ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 10.0,),
            ChatTimeAndReadStatus(status: model.msgReadBy, time: time,)
          ],
        ),
      ),
    );
  }

  RelativeRect buttonMenuPosition(BuildContext c) {
    final RenderBox bar = c.findRenderObject();
    final RenderBox overlay = Overlay.of(c).context.findRenderObject();
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.center(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );
    return position;
  }

}