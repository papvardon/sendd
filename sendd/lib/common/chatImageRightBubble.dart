import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'chatTimeAndReadStatus.dart';

class ChatImageRightBubble extends StatelessWidget{
  ChatImageRightBubble({Key key, this.filePathLocal, this.filePathServer, this.time, this.status}) : super(key: key);

  final String filePathLocal;
  final String filePathServer;
  final String time;
  final String status;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Bubble(
            shadowColor: Colors.blue,
            elevation: 2,
            alignment: Alignment.topRight,
            nip: BubbleNip.rightTop,
            color: Color.fromARGB(255, 225, 255, 199),
            child: Padding(
              padding: const EdgeInsets.all(3.0),
                  child:imageDisplay(),
            ),
          ),
          SizedBox(height: 10.0,),
          ChatTimeAndReadStatus(status: status, time: time,)
        ],
      ),
    );
  }

  Widget imageDisplay()  {
    print(filePathServer);
    /*if(filePathLocal != null && filePathLocal.isNotEmpty){
      File path = File(filePathLocal);
      return Image.file(path, width: 250.0, height: 200.0,);
    }
    else{*/
      return CachedNetworkImage(
        imageUrl: filePathServer,
        imageBuilder: (context, imageProvider) => Container(
          width: 250.0, height: 200.0,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
                colorFilter:
                ColorFilter.mode(Colors.red, BlendMode.colorBurn)),
          ),
        ),
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    //}
  }

}