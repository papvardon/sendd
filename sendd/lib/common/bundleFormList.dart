import 'package:flutter/material.dart';
import 'package:sendd/model/dataBundle.dart';

import 'bundleDatePickerField.dart';
import 'bundleDropDownField.dart';
import 'bundleInputTextField.dart';

class BundleFormList extends StatelessWidget{
  BundleFormList(this.context, this.model, this.onSaved);

  final List<DataBundle> model;
  final BuildContext context;
  final Function onSaved;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: model.length,
        itemBuilder: (context, index){

          if(model[index].category == '0013')
            {
              bool editable = true;
              if(model[index].editable == 'true'){
                editable = true;
              }
              else
              {
                editable = false;
              }
              return BundleInputTextField(heading: model[index].heading, onSaved: (String value) {
                  model[index].value = value;
                  onSaved(value, index);
                  //print(value);
                },editable: editable,
                  value: model[index].value);
            }
          else if(model[index].category == '0018')
          {
            return BundleDropDownField(data: model[index].list[0].data, onSaved: (String value) {
              model[index].value = value;
              onSaved(value, index);
            }, heading: model[index].heading);
          }
          else if(model[index].category == '0028')
          {
          return BundleDatePickerField(model: model[index], onSaved: (String value) {
          model[index].value = value;
          onSaved(value, index);
          });
          }
          else
          return Text("sarath not implemented this category "+model[index].category);
        }
    );
  }

}