import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:url_launcher/url_launcher.dart';

class ChatPdfRightBubble extends StatelessWidget{
  ChatPdfRightBubble({Key key, this.filePathLocal, this.filePathServer, this.fileName}) : super(key: key);

  final String filePathLocal;
  final String filePathServer;
  final String fileName;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Wrap(
            children: <Widget>[
              InkWell(
                onTap: (){
                  print('am clicked server path is $filePathServer');
                  _launchURL();
                },
                child: Bubble(
                  shadowColor: Colors.blue,
                  elevation: 2,
                  alignment: Alignment.topRight,
                  nip: BubbleNip.rightTop,
                  color: Color.fromARGB(255, 225, 255, 199),
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.picture_as_pdf),
                          Text(fileName)
                        ],
                      )
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

 /* void onClick(){
    OpenFile.open(filePathServer);
  }*/

 _launchURL() async {
      print('am clicked server path is $filePathServer');
      if (await canLaunch(filePathServer)) {
        await launch(filePathServer);
      } else {
        throw 'Could not launch $filePathServer';
      }
    }



}