import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'chatTimeAndReadStatus.dart';

class ChatImageLeftBubble extends StatelessWidget{
  ChatImageLeftBubble({Key key, this.filePathLocal, this.filePathServer, this.time, this.status}) : super(key: key);

  final String filePathLocal;
  final String filePathServer;
  final String time;
  final String status;

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Bubble(
            margin: BubbleEdges.only(top: 10),
            shadowColor: Colors.blue,
            elevation: 2,
            alignment: Alignment.topLeft,
            nip: BubbleNip.leftTop,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: imageDisplay(),
            ),
          ),
          SizedBox(height: 10.0,),
          ChatTimeAndReadStatus(status: 'none', time: time,)
        ],
      ),
    );

  }

  Widget imageDisplay()  {
    return CachedNetworkImage(
      imageUrl: filePathServer.isEmpty ? filePathLocal : filePathServer,
      imageBuilder: (context, imageProvider) => Container(
        width: 250.0, height: 200.0,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
              colorFilter:
              ColorFilter.mode(Colors.red, BlendMode.colorBurn)),
        ),
      ),
      placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}