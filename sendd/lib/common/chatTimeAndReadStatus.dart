import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatTimeAndReadStatus extends StatelessWidget{
  ChatTimeAndReadStatus({Key key, this.status, this.time}) : super(key: key);

  final String status;
  final String time;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: 100.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            time,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 12.0,
            ),
          ),
          msgReadStatus()
        ],
      ),
    );
  }

  Widget msgReadStatus(){
    print("my status msg is $status");
    if(status == '1'){
      return Icon(Icons.done, color: Colors.grey, size: 15.0,);
    }
    else if(status == '2'){
      return Icon(Icons.done_all, color: Colors.blue, size: 15.0,);
    }
    else if(status == 'none'){
      return Container();
    }
    else {
      return Icon(Icons.access_time, color: Colors.grey, size: 15.0,);
    }
  }

}