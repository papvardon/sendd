import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/model/ChatDbModel.dart';
import 'dart:math' as math;

import 'chatTimeAndReadStatus.dart';

/// from key denotes it come from normal ( false ) chat or group (true)


class ChatTextLeftBubble extends StatelessWidget{
  ChatTextLeftBubble({Key key, this.model, this.time, this.from}) : super(key: key);

  final String time;
  final ChatDbModel model;
  final from ;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Bubble(
            margin: BubbleEdges.only(top: 10),
            shadowColor: Colors.blue,
            elevation: 2,
            alignment: Alignment.topLeft,
            nip: BubbleNip.leftTop,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  from ? Text(
                    model.sender,
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Color(model.color).withOpacity(1.0)
                    ),
                  ) :
                      Container(width: 0.0,),
                  SizedBox(height: 8.0,),
                  Text(
                    model.message,
                    style: TextStyle(
                      fontSize: 16.0,
                    ),
                  ),

                ],
              ),
            ),
          ),
          SizedBox(height: 10.0,),
          Padding(
            padding: const EdgeInsets.only(left:8.0),
            child: Text(
              time,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 12.0,
              ),
            ),
          ),
        ],
      ),
    );

  }
}