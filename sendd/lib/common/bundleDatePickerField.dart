import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sendd/model/dataBundle.dart';

class BundleDatePickerField extends StatefulWidget {
  BundleDatePickerField({this.model, this.onSaved});

  final DataBundle model;
  final Function onSaved;


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BundleDatePickerState(onSaved: onSaved);
  }
}

class _BundleDatePickerState extends State<BundleDatePickerField> {
  _BundleDatePickerState({this.onSaved});

  Function onSaved;

  String dates;
  DateTime selectedDate = DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  Future<DateTime> _selectDate(BuildContext context) async {
    DateTime startDat = formatter.parse(widget.model.startdate);
    DateTime endDate = formatter.parse(widget.model.enddate);
    if(endDate.isBefore(selectedDate)){
      selectedDate = endDate;
    }
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: startDat,
        lastDate: endDate);
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dates = formatter.format(selectedDate);
        onSaved(dates);
      });
  }

  void getFirstDate(){

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.model.heading,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
            ),
            SizedBox(height: 20.0,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(dates ?? ''),
                InkWell(
                    onTap: () => _selectDate(context),
                    child: Icon(Icons.calendar_today, color: Colors.blue,)),
              ],
            ),
            new Divider(color: Colors.grey,),
          ],
        ),
      ),
    );
  }
}
