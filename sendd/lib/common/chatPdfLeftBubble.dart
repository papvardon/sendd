import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ChatPdfLeftBubble extends StatelessWidget{
  ChatPdfLeftBubble({Key key, this.filePathLocal, this.filePathServer, this.fileName}) : super(key: key);

  final String filePathLocal;
  final String filePathServer;
  final String fileName;

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Wrap(
            children: <Widget>[
              InkWell(
                onTap: (){
                  _launchURL();
                },
                child: Bubble(
                  margin: BubbleEdges.only(top: 10),
                  shadowColor: Colors.blue,
                  elevation: 2,
                  alignment: Alignment.topLeft,
                  nip: BubbleNip.leftTop,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.picture_as_pdf),
                          Text(fileName)
                        ],
                      )
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );

  }

    _launchURL() async {
      if (await canLaunch(filePathServer)) {
        await launch(filePathServer);
      } else {
        throw 'Could not launch $filePathServer';
      }
    }

}