import 'package:bubble/bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/model/ChatDbModel.dart';

import 'chatTimeAndReadStatus.dart';

class ChatThreadRightBubble extends StatelessWidget {
  ChatThreadRightBubble({Key key, this.model, this.time}) : super(key: key);

  final ChatDbModel model;
  final String time;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onLongPress: (){

      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Bubble(
              shadowColor: Colors.blue,
              elevation: 2,
              alignment: Alignment.topRight,
              nip: BubbleNip.rightTop,
              color: Color.fromARGB(255, 225, 255, 199),
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right:25.0),
                                  child: Text(
                                    model.sender.isNotEmpty? model.sender : '',
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                ),
                                SizedBox(height: 8,),
                                Text(model.message.isNotEmpty? model.message:'')
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 0,
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                            decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                            alignment: Alignment.center,
                            child: Text('${(model.noOfReply)}', style: TextStyle(color: Colors.white),),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 10.0,),
                    Text(
                      model.repliedMessage,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    ),

                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            ChatTimeAndReadStatus(
              status: model.msgReadBy,
              time: time,
            )
          ],
        ),
      ),
    );
  }
}
