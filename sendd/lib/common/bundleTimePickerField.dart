import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BundleTimePickerField extends StatefulWidget{
  BundleTimePickerField({Key key, this.heading, this.onSaved, this.previousTime}) : super(key: key);

  final String heading;
  final String previousTime;
  final Function onSaved;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BundleTimePickerFiledState(heading,onSaved,previousTime);
  }

}

class _BundleTimePickerFiledState extends State<BundleTimePickerField>{

  String heading;
  Function onSaved;

  String selectedTime;

  _BundleTimePickerFiledState(this.heading, this.onSaved, this.selectedTime);

  Future<TimeOfDay> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (BuildContext context, Widget child) {
        return Directionality(
          textDirection: TextDirection.ltr,
          child: child,
        );
      },);
    if (picked != null && picked.format(context) != selectedTime)
      setState(() {
        selectedTime = picked.format(context);
        onSaved(selectedTime);
      });
    return null;
  }

  @override
  Widget build(BuildContext context) {
      // TODO: implement build
      return Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                heading,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
              ),
              SizedBox(height: 20.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(selectedTime ?? ''),
                  InkWell(
                      onTap: () => _selectTime(context),
                      child: Icon(Icons.timer, color: Colors.blue,)),
                ],
              ),
              new Divider(color: Colors.grey,),
            ],
          ),
        ),
      );
    }

  }

