
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InputTextField extends StatelessWidget{

  final String hintText;
  final Function validator;
  final Function onSaved;
  final bool isPassword;
  final bool isEmail;
  final bool isLast;

  InputTextField({
    this.hintText,
    this.validator,
    this.onSaved,
    this.isPassword = false,
    this.isEmail = false,
    this.isLast = false,
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              hintText,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
            SizedBox(
              height: 3,
            ),
            TextFormField(
                decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color : Colors.grey)
                  ),
                ),
                obscureText: isPassword ? true : false,
              keyboardType: isEmail ? TextInputType.emailAddress : TextInputType.text,
              textInputAction: isLast ? TextInputAction.done : TextInputAction.next,
              onEditingComplete: () => FocusScope.of(context).unfocus(),
              onSaved: onSaved,
              validator: validator,
            )
          ],
        ),
      ),
    );
  }

}
