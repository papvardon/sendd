import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SubMenuInChat extends StatelessWidget{

  RelativeRect buttonMenuPosition(BuildContext c) {
    final RenderBox bar = c.findRenderObject();
    final RenderBox overlay = Overlay.of(c).context.findRenderObject();
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.center(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );
    return position;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return popData(context);
  }

  Widget popData(BuildContext context){
    final position = buttonMenuPosition(context);
    final result = showMenu(
      context: context,
      position: position,
      items: <PopupMenuEntry>[
        PopupMenuItem(
          value: '12',
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.reply, color: Colors.grey,),
                  SizedBox(width: 10.0,),
                  Text("Replay"),
                ],
              ),
              /* Row(
                      children: <Widget>[
                        Icon(Icons., color: Colors.grey,),
                        SizedBox(width: 10.0,),
                        Text("Forward"),
                      ],
                    )*/
            ],
          ),
        )
      ],
    );
  }

}