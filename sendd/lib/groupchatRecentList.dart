import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:sendd/createGroup.dart';
import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/model/groupRecentChatListModel.dart';
import 'package:sendd/model/ouListModel.dart';
import 'package:sendd/utils/customDialog.dart';

import 'common/circularProgress.dart';
import 'common/dialogs.dart';
import 'data/services.dart';
import 'doctorPatientChat.dart';
import 'groupChatting.dart';

class GroupChatRecentList extends StatefulWidget {
  GroupChatRecentList({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return GroupChatRecentState();
  }
}

class GroupChatRecentState extends State<GroupChatRecentList> {
  GroupRecentChatListModel dataModel;
  Future<List<Organisation>> model;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  var db = DatabaseHelper();
  bool dialVisible = true;
  OuListModel ouModel;


  @override
  void initState() {
    super.initState();
    syncData();
    getOuList();
  }

  void getOuList() async {
    ouModel = await Services.getOUList();
  }

  void fabClick(String title) async {
    for(int i = 0; i < ouModel.organisationunit.length; i++){
      print(title);
      print(ouModel.organisationunit[i].title);
      if(ouModel.organisationunit[i].title == title){
        print('my id ${ouModel.organisationunit[i].id}');
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CreateGroup(id: ouModel.organisationunit[i].id))).
          then((value) => syncData());
        break;
      }
    }
  }

  void _navigateToChat(Groups data) async{
    String userId = await SharedPreferencesHelper.getUserId();
    String token = await SharedPreferencesHelper.getToken();
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();

    SharedPreferencesHelper.setChannelId(dynamicUrl);
    print("my groups is ${data.acceptStatus}");

    if(data.acceptStatus == "true") {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  GroupChatting(
                    channelId: data.id,
                    grpTitle: data.title,
                    userID: userId,
                    token: token,
                    grpIcon: data.icon
                  ))).then((value) => syncData());
    }
    else
      {
        _groupPermissionCheck(data);
      }
  }

  Future<void> syncData() async {
    var deResult = await db.deleteGroupRecentChat();
    var result = await Services.getRecentChatForGroups();
    if (result.data.organisation.length > 0) {
      var result1 = await db.saveRecentGroupChat(result.data.organisation);
      getLocalData();
    }
  }

//  void saveToLocalDb(RecentChatListModel value) async{
//    print(value.data.length);
//    if(value.data.length>0){
//      var result = await db.saveRecentChatList(value.data);
//    }
//  }

  void getLocalData() async {
    String userId = await SharedPreferencesHelper.getUserId();
    setState(() {
      model = db.getRecentGroupChatList();
    });
  }

  void _groupPermissionCheck(Groups data){
    showDialog(
      context: context,
      builder: (BuildContext context) => CustomDialog(
        title: data.title,
        description:
        "${data.adminName} wants to add you to this group",
        buttonTextAccept: "Join",
        buttonTextDeny: "Cancel",
        image: data.icon,
        accept: _acceptChat,
        data: data,
      ),
    );
  }

  void _acceptChat(Groups data) async{

    Map<String, dynamic> map = new Map();
    map['token'] = await SharedPreferencesHelper.getToken();
    map['userId'] = await SharedPreferencesHelper.getUserId();
    map['channel_id'] = data.id;
    map['acceptstatus'] = "1";
    map['group'] = '1';

    Dialogs.showLoadingDialog(context, _keyLoader);
    String encodedJSON = jsonEncode(map);
    print("my json $encodedJSON");

    CommonModel mdl = await Services.acceptGroupChat(encodedJSON).catchError((error) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      print(error);
    });

    if(mdl.status == "true"){
      data.acceptStatus = "true";
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _navigateToChat(data);
    }
    else{
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      Navigator.of(context).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(mdl.message)));
    }


  }

  Widget _myListView(
      BuildContext context, AsyncSnapshot<List<Organisation>> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: snapshot.data.length,
        itemBuilder: (context, index) {
          return ExpansionPanelList(
            expansionCallback: (int item, bool isExpanded) {
              setState(() {
                snapshot.data[index].isExpanded = !isExpanded;
              });
            },
            children: [
              ExpansionPanel(
                headerBuilder: (BuildContext context, bool isExpanded) {
                  return ListTile(
                    title: Text(snapshot.data[index].title),
                    leading: CachedNetworkImage(imageUrl: snapshot.data[index].icon, width: 40.0, height: 40.0, fit:BoxFit.cover,),
                  );
                },
                body: _myListViewGroups(snapshot.data[index].id),
                isExpanded: snapshot.data[index].isExpanded,
              ),
            ],
          );
        });
  }

  Widget _myListViewGroups(String recentId) {
    return FutureBuilder<List<Groups>>(
        future: db.getGroupChatList(recentId),
        builder: (context, snapshotData) {
          if (snapshotData.hasError) {
            return Text('Error ${snapshotData.error}');
          }
          if (snapshotData.hasData) {
            return ListView.builder(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemCount: snapshotData.data.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(left: 15.0, right: 8.0),
                    child: ListTile(
                      onTap: ()=> _navigateToChat(snapshotData.data[index]),
                      leading: ClipOval(child: CachedNetworkImage(imageUrl: snapshotData.data[index].icon, height: 40.0, width: 40.0, fit: BoxFit.cover,)),
                      title: Text(snapshotData.data[index].title),
                    ),
                  );
                });
          }
          return CircularProgress();
        });
  }



  SpeedDial buildSpeedDial() {
    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      // child: Icon(Icons.add),
      onOpen: () => print('OPENING DIAL'),
      onClose: () => print('DIAL CLOSED'),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.group, color: Colors.white),
          backgroundColor: Colors.deepOrange,
          onTap: () => fabClick('Friends And Family'),
          label: "Friends And Family",
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.deepOrangeAccent,
        ),
        SpeedDialChild(
          child: Icon(Icons.person, color: Colors.white),
          backgroundColor: Colors.green,
          onTap: () => fabClick('Work'),
          label: 'Work',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.green,
        ),
        SpeedDialChild(
          child: Icon(Icons.work, color: Colors.white),
          backgroundColor: Colors.blue,
          onTap: () => fabClick('Other'),
          labelWidget: Container(
            color: Colors.blue,
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.all(6),
            child: Text('Other'),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: buildSpeedDial(),
      body: Container(
          height: double.infinity,
          child: FutureBuilder<List<Organisation>>(
              future: model,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text('Error ${snapshot.error}');
                }
                if (snapshot.hasData) {
                  return Stack(
                    children: <Widget>[
                      Container(
                        height: double.infinity,
                        color: Colors.white,
                      ),
                      _myListView(context, snapshot)
                    ],
                  );
                }
                return CircularProgress();
              })),
    );
  }
}
