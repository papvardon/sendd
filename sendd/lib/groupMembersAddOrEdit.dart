import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/common/dialogs.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/circularProgress.dart';
import 'data/databaseHelper.dart';
import 'data/services.dart';
import 'model/contactsModel.dart';

class GroupMembersAddOrEdit extends StatefulWidget{
  GroupMembersAddOrEdit({Key key, this.channelId}) : super(key: key);

  final channelId;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GroupMembersEditState();
  }

}

class _GroupMembersEditState extends State<GroupMembersAddOrEdit>{
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  Future<ContactsModel> model;
  var db = DatabaseHelper();

  List selectedContact = List();
  List removingContact = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    model = Services.getSenddContact(widget.channelId);
  }

  void _onSelected(bool selected, String id) {
    if (selected == true) {
      setState(() {
        selectedContact.add(id);
        if(removingContact.contains(id)){
          removingContact.remove(id);
        }
      });
    } else {
      setState(() {
        selectedContact.remove(id);
        removingContact.add(id);
      });
    }
  }

  void _onSubmit() async{
    if(selectedContact.length>0 || removingContact.length>0){
      print("my selected members is $selectedContact");
      print("my removing members is $removingContact");

      Map<String, dynamic> map = new Map();
      map['token'] = await SharedPreferencesHelper.getToken();
      map['userId'] = await SharedPreferencesHelper.getUserId();
      map['channel_id'] = widget.channelId;
      map['Ids'] = selectedContact;
      map['removeIds'] = removingContact;

      Dialogs.showLoadingDialog(context, _keyLoader);
      String encodedJSON = jsonEncode(map);

      CommonModel mdl = await Services.updateGrpMembers(encodedJSON).catchError((error) {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        print(error);
      });
      if(mdl.status == 'true'){
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text(mdl.message)));
        Future.delayed(const Duration(milliseconds: 1000), () {
          setState(() {
            Navigator.pop(context);
          });
        });
      }
      else {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text(mdl.message)));
      }
//      Navigator.push(context, MaterialPageRoute(
//          builder: (BuildContext context) =>
//              CreateGroupProfile(selectedContact: selectedContact, id: widget.id,)));
    }
    else{
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text('Please select group members')));
    }
  }

  Widget _myListView(List<CData> data
      ) {
    return Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: data.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      ClipOval(child: CachedNetworkImage(imageUrl: data[index].icon, width: 50.0, height: 50.0, fit:BoxFit.cover,),),
                      SizedBox(width: 10.0,),
                      Text(data[index].firstName)
                    ],
                  ),
                  Checkbox(
                      value: data[index].status == "1" ? true :false,
                      activeColor: PrimaryColor,
                      onChanged: (bool value) {
                        setState(() {
                          data[index].status == '1' ? data[index].status = "0" : data[index].status ="1";
                        });
                        _onSelected(value, data[index].id);
                      }
                  )
                ],
              ),
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('Add Members'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
        actions: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(right:8.0),
              child: InkWell(
                onTap: ()=> _onSubmit(),
                child: Text('Next', style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),),
              ),
            ),
          )
        ],
      ),
      body: Container(
          height: double.infinity,
          child: FutureBuilder<ContactsModel>(
              future: model,
              builder: (context, snapshot){
                if(snapshot.hasError){
                  return Text('Error ${snapshot.error}');
                }
                if(snapshot.hasData){
                  return Stack(
                    children: <Widget>[
                      Container(
                        height: double.infinity,
                        color: Colors.white,
                      ),
                      _myListView(snapshot.data.data)
                    ],
                  );
                }
                return CircularProgress();
              }
          )
      ),
    );
  }

}