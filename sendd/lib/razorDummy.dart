import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/dialogs.dart';


class RazorDummy extends StatefulWidget {
  RazorDummy({Key key, this.amount, this.appointmentId, this.clinicId, this.appointmentFee}) : super(key: key);

  final amount;
  final appointmentFee;
  final appointmentId;
  final clinicId;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RazorState();
  }

}

class RazorState extends State<RazorDummy> {

  Razorpay _razorpay;
  String orderId;
  bool status = true;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
        appBar: AppBar(
          title: const Text('Payment'),
        ),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 40.0,
                    backgroundColor: PrimaryColor,
                    child: Text('\u20B9', style: TextStyle(color: Colors.white, fontSize: 35.0),),
                  ),
                  SizedBox(height: 20.0,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: status ? Text('Total amount you need to pay is ${widget.amount}') : Text('Your appointment booked succesfully, \nyour appointment number is ${widget.appointmentId} '),
                  ),
                  SizedBox(height: 20.0,),
                  status ? RaisedButton(onPressed: openCheckout, child: Text('Pay')) : RaisedButton(onPressed: () => Navigator.of(context).pop(true), child: Text('Home'))
                ])),
      );

  }

  @override
  void initState() {
    super.initState();
    intiateOrderId();
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  void intiateOrderId() async{
    await Services.getOrderIdForPayment(widget.amount).then((value) => orderId = value.orderid);
    print(orderId);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    int amt = int.parse(widget.amount)*100;
    var options = {
      'key': 'rzp_live_IyOdRTvXtAgYJe',
      'amount': amt,
      'name': 'SENDD',
      'description': 'Appointment',
      'order_id': orderId,
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    _paymentVerify(response);
  }

  void _paymentVerify(PaymentSuccessResponse response) async{
    Dialogs.showLoadingDialog(context, _keyLoader);
    CommonModel mdl = await Services.doPaymentVerify(widget.amount, response.paymentId, orderId).catchError((onError){
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    });
    if(mdl.status == 'true'){
      _doPaymentUpdate(response, mdl.message);
    }
  }

  void _doPaymentUpdate(PaymentSuccessResponse response, String message) async{
    Map<String, dynamic> map = new Map();

    map['Clinic_ID'] = widget.clinicId;
    map['Sendd_UserID'] = await SharedPreferencesHelper.getUserId();
    map['phone_number'] = await SharedPreferencesHelper.getMobileNumber();
    map['Payment_Id'] = response.paymentId;
    map['Razorpay_OrderId'] = response.orderId;
    map['AppointmentFee'] = widget.appointmentFee;
    map['TotalAmount'] = widget.amount;
    map['TransactionStatus'] = message;
    map['PaymentMode'] = "Online";
    map['AppointmentNo'] = widget.appointmentId;
    map['valetpay'] = "0";
    map['profileid'] = await SharedPreferencesHelper.getProfileId();

    String encodedJSON = jsonEncode(map);
    CommonModel mdl = await Services.doPaymentUpdateInServer(encodedJSON).catchError((error) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      print(error);
    });

    if(mdl.status == "true") {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      setState(() {
        status = false;
      });
    }
    else{
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text("Something went wrong ..... Please try again later")));
    }
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        timeInSecForIosWeb: 4);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, timeInSecForIosWeb: 4);
  }
}
