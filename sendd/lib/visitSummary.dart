import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/model/LabPrescriptionModel.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/utils/custColors.dart';
import 'package:web_socket_channel/io.dart';

import 'common/circularProgress.dart';
import 'model/medicinePrescriptionModel.dart';
import 'model/visitSummaryDetailModel.dart';

class VisitSummary extends StatefulWidget {
  VisitSummary({Key key, this.clinicId, this.mrn, this.visitId, this.visitnumber, this.userId, this.token}) : super(key: key);

  final clinicId;
  final visitId;
  final visitnumber;
  final mrn;
  final userId;
  final token;


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VisitState();
  }
}

class _VisitState extends State<VisitSummary> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<VisitSummaryDetailModel> model;
  Future<MedicinePrescriptionModel> mModel;
  Future<LabPrescriptionModel> lModel;
  File _image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    model = Services.getVisitSummaryDetails(widget.clinicId, widget.visitId, widget.mrn, widget.token);
    mModel = Services.getMedicinePrescriptions(widget.clinicId, widget.visitnumber, widget.token);
    lModel = Services.getLabPrescriptions(widget.clinicId, widget.visitnumber, widget.token);
  }

  Widget _myListView(BuildContext context, AsyncSnapshot<VisitSummaryDetailModel> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: snapshot.data.data[0].bottomcontent.content.length,
        itemBuilder: (context, index){
          return ListTile(
            title: Text(snapshot.data.data[0].bottomcontent.content[index].title),
            subtitle: Text(snapshot.data.data[0].bottomcontent.content[index].value),
          );
        }
    );
  }

  Widget _myListViewMedicine(
      BuildContext context, AsyncSnapshot<MedicinePrescriptionModel> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: snapshot.data.items.length,
        itemBuilder: (context, index) {
          return Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: _listCell(snapshot.data.items[index].details)
            ),
          );
        });
  }

  Widget _listCell(List<Details> details){
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: details.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child:Text(details[index].title)),
                Expanded(
                  child: Text(
                    details[index].value,
                    style: TextStyle(color: Colors.grey),
                  ),
                )
              ],
            ),
          );
        }
    );

  }

  Widget _myListViewLab(
      BuildContext context, AsyncSnapshot<LabPrescriptionModel> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: snapshot.data.items.length,
        itemBuilder: (context, index) {
          return Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: _listCellLab(snapshot.data.items[index].details, snapshot.data.labPrescriptionId)
            ),
          );
        });
  }

  Widget _listCellLab(List<ItemDetails> details, String labPrescriptionId){
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: details.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                      child: Text(details[index].title)),
                  Expanded(
                    child: Text(
                      details[index].value,
                      style: TextStyle(color: Colors.grey),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10.0,),
              _uploadButton(details[index], labPrescriptionId)
            ],
          );
        }
    );

  }

  Widget _uploadButton(ItemDetails detail, String labPrescriptionId) {
    return InkWell(
      onTap: (){getAttachments(detail, labPrescriptionId);},
      child: Container(
        width: 130,
        height: 35,
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: PrimaryColor,
            borderRadius: BorderRadius.all(Radius.circular(40)),
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [buttonGradientEnd, buttonGradientStart])

        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Upload Lab Report',
              style: TextStyle(fontSize: 10, color: Colors.white),
            ),
            Icon(Icons.file_upload, size: 20.0, color: Colors.white,)
          ],
        ),
      ),
    );
  }

  Future getAttachments(ItemDetails detail, String labPrescriptionId) async {
    _image = await FilePicker.getFile(type: FileType.custom, allowedExtensions: ['pdf']);

    var bytes = await _image.readAsBytes();
    sendImageToWebSocket(bytes, labPrescriptionId, detail.value);

  }

  Future<void> sendImageToWebSocket(Uint8List bytes, String labPrescriptionId, String value) async {
    var channel = await IOWebSocketChannel.connect("ws://125.99.242.202:6032/filedatagp");
    channel.sink.add(bytes);
    channel.stream.listen((message) async {
      print('from websocket $message');
      if(message.toString().contains('http'))
      {
        Map<String, dynamic> map = new Map();
        map['user_id'] = widget.userId;
        map['token'] = widget.token;
        map['lab_prescription_id'] = labPrescriptionId;
        map['test_name'] = value;
        map['file_path'] = message;

        String encodedJSON = jsonEncode(map);
        CommonModel mdl = await Services.uploadLabReport(encodedJSON).catchError((error) {
          print(error);
        });
        if(mdl.status == "true"){
          _scaffoldKey.currentState.showSnackBar(
              new SnackBar(content: new Text(mdl.message)));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text("Visit Summary"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              FutureBuilder<VisitSummaryDetailModel>(
                future: model,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Text('Error ${snapshot.error}');
                  }
                  if (snapshot.hasData) {
                    return SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.all(5.0),
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0),
                                child: ExpansionTile(
                                  backgroundColor: Colors.white,
                                  title: Text(snapshot.data.data[0].bottomcontent.heading, style: TextStyle(color: PrimaryColor, fontWeight: FontWeight.bold),),
                                  children: <Widget>[
                                    _myListView(context, snapshot)
                                  ],
                                ),
                              )
                          ),
                        ),
                      ),
                    );
                  }
                  return CircularProgress();
                },
              ),
          FutureBuilder<MedicinePrescriptionModel>(
            future: mModel,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Text('Error ${snapshot.error}');
              }
              if (snapshot.hasData) {
                return SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(5.0),
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0),
                            child: ExpansionTile(
                              title: Text('Medicine Prescription', style: TextStyle(color: PrimaryColor, fontWeight: FontWeight.bold),),
                              children: <Widget>[
                                _myListViewMedicine(context, snapshot)
                              ],
                            ),
                          )
                      ),
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
              FutureBuilder<LabPrescriptionModel>(
                future: lModel,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Text('Error ${snapshot.error}');
                  }
                  if (snapshot.hasData) {
                    return SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.all(5.0),
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0),
                                child: ExpansionTile(
                                  title: Text('Lab Prescription', style: TextStyle(color: PrimaryColor, fontWeight: FontWeight.bold),),
                                  children: <Widget>[
                                    _myListViewLab(context, snapshot)
                                  ],
                                ),
                              )
                          ),
                        ),
                      ),
                    );
                  }
                  return Container();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
