
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/dashboarddatamodel.dart';

class DashboardGridTile extends StatelessWidget{

  DashboardGridTile(this.context, this.model);

  @required
  final Dashboarddatamodel model;
  final BuildContext context;


  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        color: Colors.white,
        child: Padding(
          padding:
          EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0, top: 10.0),
          child: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Image.network(
                    model.icon,
                    width: 80,
                    height: 80,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Text(
                    model.title,
                    maxLines: 1,
                    softWrap: true,
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }



}