import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/common/circularProgress.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/razorDummy.dart';

import 'model/completedAppointmentModel.dart';

class PendingAppointments extends StatefulWidget{
  PendingAppointments({Key key, this.mrn, this.clinicId}) : super(key: key);

  final mrn;
  final clinicId;
  
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PendingState();
  }

}

class _PendingState extends State<PendingAppointments>{

  Future<CompletedAppointmentModel> model;

  @override
  void initState() {
    super.initState();
    model = Services.getPendingAppointments(widget.mrn, widget.clinicId);
  }

  void _initiatePayment(Data data){
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => RazorDummy(appointmentId: data.appointmentNo,
      amount: data.totalamount, appointmentFee: data.appointment_fees, clinicId: widget.clinicId,))).then((value) => setState((){
      model = Services.getPendingAppointments(widget.mrn, widget.clinicId);
    }));
  }

  Widget _myListView(List<Data> data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return InkWell(
          //onTap: () => _onPatientClick(snapshot.data.data[index]),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Padding(
                  padding: const EdgeInsets.only(top:10.0, bottom: 10.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ClipOval(child: Image.asset("assets/images/avatar_patient.png", width: 50.0, height: 50.0,)),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 10.0,
                          ),
                          Text("Dr. ${data[index].doctorName}"),
                          Row(
                            children: <Widget>[
                              Text("Date"),
                              SizedBox(
                                width: 20.0,
                              ),
                              Text("${data[index].appointmentDate} - ${data[index].appointmentSlotTime}" ,
                                  style: TextStyle(fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Text('Appointment No'),
                              SizedBox(
                                width: 20.0,
                              ),
                              Text(data[index].appointmentNo,
                                  style: TextStyle(fontWeight: FontWeight.bold))
                            ],
                          ),
                          SizedBox(
                            height: 7.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text('Payment Status'),
                              SizedBox(
                                width: 20.0,
                              ),
                              data[index].paymentstatus == 'true' ? Text('paid',
                                  style: TextStyle(fontWeight: FontWeight.bold))
                                  : InkWell(
                                    onTap: ()=> _initiatePayment(data[index]),
                                  child: Text("Pay", style: TextStyle(fontWeight: FontWeight.bold, decoration: TextDecoration.underline, color: Colors.red)))
                            ],
                          ),
                          SizedBox(
                            height: 7.0,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        height: double.infinity,
          child: FutureBuilder<CompletedAppointmentModel>(
              future: model,
              builder: (context, snapshot){
                if(snapshot.hasError){
                  return Text('Error ${snapshot.error}');
                }
                if (snapshot.hasData) {
                  if (snapshot.data.status == 'true') {
                    return Stack(
                      children: <Widget>[
                        Container(
                          height: double.infinity,
                          color: Colors.white,
                        ),
                        _myListView(snapshot.data.data)
                      ],
                    );
                  } else {
                    return Container(
                      child: Center(
                        child: Text('Something went wrong!!!'),
                      ),
                    );
                  }
                }
                return CircularProgress();
              }
          )
      ),
    );
  }

}