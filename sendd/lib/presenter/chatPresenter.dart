import 'dart:typed_data';

import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/model/eventObject.dart';
import 'package:sendd/presenter/chatPresenterIView.dart';
import 'package:sendd/presenter/iChatPresenter.dart';
import 'package:web_socket_channel/io.dart';

class ChatPresenter implements IChatPresenter{

  bool reCall = false;

  ChatPresenterIView _view;
  var db = DatabaseHelper();


  @override
  set chatView(ChatPresenterIView view) {
    // TODO: implement chatView
    this._view = view;
  }



  /*Future<void> getChats(String channelId) async {
    reCall = false;
    //var result = await Services.getChat(channelId, );
    reCall = result != null ? true : false;
    if(result.data.length > 0) {
      var result1 = await db.saveChatFromServer(result.data, channelId);
      print('my saving resilt is $result1');
      getLocalChat(channelId);
      print('from result get chat $result.toString()');
    }
  }*/

  @override
  Future<void> getLocalChat(String channelId) async {
    EventObject eventObject = await db.getChat(channelId, '');
    _view.onRefreshChat(eventObject);
  }

  /*Future<void> saveChatToDB(String currentMessage, String category, String localpath, String fileName) async {

    ChatMessageModel value = new ChatMessageModel();
    value.senderId = userID;
    if(currentMessage.isNotEmpty) {
      value.message =
      await DataEncrypt().encryptionFromChat(userID, currentMessage);
    }
    value.category = category;
    value.setLocalFile = localpath;
    value.channelId = channelId;
    value.createAt = new DateTime.now().millisecondsSinceEpoch;
    value.mId = new DateTime.now().millisecondsSinceEpoch.toString();
    value.attachmentName = fileName;

    dynamic result1 = await db.saveChatAfterPost(value);
    getLocalChat();
    print('local saving data $result1');

    if(category == '1000') {
      var result = await Services.postMessage(channelId, currentMessage, category, value.mId);
      if (result.status == "true") {
        var result1 = await db.saveChatAfterPost(result.data);
        getLocalChat();
      }
    }
    else if(category == '1001'){
      var bytes = await _image.readAsBytes();
      sendImageToWebSocket(bytes, value);
    }
    else if(category == '1006'){
      var bytes = await _image.readAsBytes();
      sendImageToWebSocket(bytes, value);
    }
  }

  Future<void> sendImageToWebSocket(Uint8List bytes, ChatMessageModel value) async {
    var channel = await IOWebSocketChannel.connect("ws://125.99.242.202:6032/filedata");
    channel.sink.add(bytes);
    channel.stream.listen((message) async {
      print('from websocket $message');
      if(message.toString().contains('http'))
      {
        value.filenames = message;
        var result = await Services.postImageAndMessage(value);
        channel.sink.close();
        if (result.status == "true") {
          var result1 = await db.saveChatAfterPost(result.data);
          setState(() {
            getLocalChat();
          });
        }
      }
    });
  }
*/
}