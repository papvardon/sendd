
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/model/heading.dart';

class CarousalCell extends StatelessWidget{

  const CarousalCell(this.context, this.model);

  @required
  final Heading model;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.transparent,
      margin: EdgeInsets.all(5.0),
      child: ClipRRect(

          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          child: CachedNetworkImage(imageUrl: model.icon, fit: BoxFit.cover, width: 1000,)
      ),
    );
  }




}