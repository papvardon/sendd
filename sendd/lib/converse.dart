import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/privateRecentChatList.dart';
import 'package:sendd/utils/constants.dart';
import 'package:sendd/utils/custColors.dart';

import 'groupchatRecentList.dart';


class Converse extends StatefulWidget {
  Converse({Key key, this.index}) : super(key: key);

  @required final index;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ConverseState();
  }
}


class ConverseState extends State<Converse> with TickerProviderStateMixin {

  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this, initialIndex: widget.index);
    super.initState();
  }

  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              controller: _tabController,
              tabs: [
                Tab(text: 'One - One ', icon: Icon(Icons.chat_bubble_outline),),
                //Tab(text: 'Private Chat', icon:  Icon(Icons.lock_open),),
                Tab(text: 'Group Chat', icon:  Icon(Icons.group),)
              ],
            ),
            title: Text('Converse'),
            centerTitle: true,
            backgroundColor: PrimaryColor,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              PrivateRecentChatList(fromPrivate: false,),
              //PrivateRecentChatList(fromPrivate: true,),
              GroupChatRecentList()
            ],
            controller: _tabController,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

}
