import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:sendd/carousal_cell.dart';
import 'package:sendd/clinicsListing.dart';
import 'package:sendd/common/circularProgress.dart';
import 'package:sendd/converse.dart';
import 'package:sendd/dashboard_grid_link.dart';
import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/dashboarddatamodel.dart';
import 'package:sendd/model/dashboardmodel.dart';
import 'package:sendd/model/heading.dart';
import 'package:sendd/pages/alerts/alertChat.dart';
import 'package:sendd/utils/custColors.dart';
import 'package:sendd/utils/customDialog.dart';
import 'dart:async';
import 'login.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _DashboardState();
  }
}

class _DashboardState extends State<Dashboard> {
  final GlobalKey<FabCircularMenuState> fabKey = GlobalKey();
  StreamController<int> streamController = new StreamController<int>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _currentIndex = 0;
  DashboardModel dModel;
  List<Heading> hModel;
  bool loadingStatus = false;
  var db = DatabaseHelper();
  Future<List<Heading>> model;
  DateTime currentBackPressTime;

  @override
  void initState() {
    super.initState();
    fetchData();
    syncData();
  }

  void fetchData() async {
    setState(() {
      model = db.getDashBoard();
    });
  }

  void syncData() async {
    //var delteData = await db.deleteDashboard();
    var result = await Services.getData()
        .then((value) {
      db.saveDashBoard(value.heading);
      SharedPreferencesHelper.setAlertChannel(value.alertChannel);
    });
    //var result1 = await Services.getDynamicData().then((value) => db.saveDashBoard(value.heading));
    print('main result is $result');
    fetchData();
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new GestureDetector(
                  onTap: () => Navigator.of(context).pop(false),
                  child: Text(
                    "NO",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new GestureDetector(
                  onTap: () => Navigator.of(context).pop(true),
                  child: Text(
                    "YES",
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ),
            ],
          ),
        ) ??
        false;
  }

  Future<void> _itemClick(Dashboarddatamodel item, String id) async {
    if (id == '1') {
      /// 31 for chat. 32 fro group chat, 33 for private chat, 34 suggetion, 35 synch contact
      if (item.linkid == '31') {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => Converse(index: 0,)));
      }
      else if (item.linkid == '32') {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => Converse(index: 1,)));
      }
    }
    else if (id == '4') {
      if (item.linkid == '41') {
        await SharedPreferencesHelper.setChannelId(item.channelId);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ClinicListing()));
      }
      /*else if(item.title.toUpperCase() == 'FINANCE') {
        //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => RazorDummy()));
      }
      else{
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ClinicProfile(clinicId: item.id,)));
      }*/
    }
  }


  gridview(AsyncSnapshot<List<Heading>> snapshot) {
    if (snapshot.hasData) {
      if (snapshot.data.length > 0) {
        return FutureBuilder<List<Dashboarddatamodel>>(
          future: db.getDashBoardLinks(snapshot.data[_currentIndex].id),
          builder: (context, snapshotData) {
            if (snapshotData.hasError) {
              return Text('Error ${snapshotData.error}');
            }
            if (snapshotData.hasData) {
              return Padding(
                padding: EdgeInsets.all(5.0),
                child: GridView.count(
                  physics: ScrollPhysics(),
                  crossAxisCount: 2,
                  childAspectRatio: 1.0 / .5,
                  shrinkWrap: true,
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  children: snapshotData.data.map(
                    (item) {
                      return GestureDetector(
                        child: GridTile(
                          child: DashboardGridTile(context, item),
                        ),
                        onTap: () {
                          _itemClick(item, snapshot.data[_currentIndex].id);
                        },
                      );
                    },
                  ).toList(),
                ),
              );
            }
            return CircularProgress();
          },
        );
      }
      return Container();
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: PrimaryColor,
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: StreamBuilder(
            initialData: 0,
            stream: streamController.stream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Text('Home');
            },
          ),
        ),
        floatingActionButton: FabCircularMenu(
            key: fabKey,
            fabColor: PrimaryColor,
            fabCloseIcon: Icon(
              Icons.close,
              color: Colors.white,
            ),
            fabOpenIcon: Icon(
              Icons.menu,
              color: Colors.white,
            ),
            fabElevation: 8,
            ringDiameter: MediaQuery.of(context).size.width * 1,
            ringColor: Colors.white,
            ringWidth: (MediaQuery.of(context).size.width * 1) * 0.2,
            children: <Widget>[
              IconButton(
                  icon: Icon(
                    Icons.notifications,
                    color: buttonGradientStart,
                  ),
                  onPressed: () {
                    fabKey.currentState.close();
                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(content: new Text('Coming Soon')));
                  }),
              IconButton(icon: Image.asset("assets/images/alert.png", color: Colors.blue, height: 25,), onPressed: () async {
                fabKey.currentState.close();
                String channelId = await SharedPreferencesHelper.getAlertChannel();
                Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) =>
                        AlertChat(channelId: channelId,)));
              }),
              IconButton(
                  icon: Icon(
                    Icons.chat,
                    color: buttonGradientStart,
                  ),
                  onPressed: () {
                    fabKey.currentState.close();
                    Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) =>
                        Converse(index: 0,)));
                  }),
              IconButton(
                  icon: Icon(
                    Icons.person,
                    color: buttonGradientStart,
                  ),
                  onPressed: () {
                    fabKey.currentState.close();
                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(content: new Text('Coming Soon')));
                  }),
              IconButton(
                  icon: Icon(
                    Icons.power_settings_new,
                    color: buttonGradientStart,
                  ),
                  onPressed: () async {
                    fabKey.currentState.close();
                    await SharedPreferencesHelper.setLogdinStatus('false');
                    await SharedPreferencesHelper.setDynamicUrl('');
                    await SharedPreferencesHelper.setProfileId('');
                    await SharedPreferencesHelper.setRoleType('');
                    await SharedPreferencesHelper.setToken('');
                    await SharedPreferencesHelper.setUserId('');
                    await db.deleteUser();
                    await db.deleteDashboard();
                    await db.deleteAllChats();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => Login()));
                  }),
            ]),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                child: Image.asset('assets/images/backgrounddark.png'),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Welcome back",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25, color: Colors.white),
                    ),
                  ),
                  FutureBuilder<List<Heading>>(
                    future: model,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return Text('Error ${snapshot.error}');
                      }
                      if (snapshot.hasData) {
                        return Container(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left:10.0, right: 10.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(
                                      color: Colors.transparent,
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: CarouselSlider(
                                      options: CarouselOptions(
                                          autoPlay: false,
                                          enlargeCenterPage: true,
                                          enlargeStrategy:
                                          CenterPageEnlargeStrategy.height,
                                          onPageChanged: (index, reason) {
                                            setState(() {
                                              _currentIndex = index;
                                            });
                                          }),
                                      items: snapshot.data
                                          .map((item) => Container(
                                        child: Center(
                                            child: CarousalCell(context, item)),
                                        color: Colors.transparent,
                                      ))
                                          .toList(),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              gridview(snapshot),
                            ],
                          ),
                        );
                      } else if (!snapshot.hasData) {
                        return Container();
                      }
                      return CircularProgress();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }
}
