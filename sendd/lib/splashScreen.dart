import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/dashboard.dart';
import 'package:sendd/login.dart';
import 'package:sendd/utils/custColors.dart';

import 'data/sharedpreferenceshelper.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 5), () =>
      navigateToPage()
    );
  }

  Future<void> navigateToPage() async {
    String logdinStatus = await SharedPreferencesHelper.getLogdinStatus();
    if(logdinStatus == 'true')
      {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Dashboard()));
      }
    else{
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Login()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [loginGradient, loginBottomGradient])
            ),
          ),
          Center(
            child: Image.asset('assets/images/ic_sendd_icon.png', height: MediaQuery.of(context).size.height/5),
          )
        ],
      ),
    );
  }
}