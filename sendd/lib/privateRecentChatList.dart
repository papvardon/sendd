import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/recentChatListModel.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/circularProgress.dart';
import 'contacts.dart';
import 'data/services.dart';
import 'doctorPatientChat.dart';

class PrivateRecentChatList extends StatefulWidget {
  PrivateRecentChatList({Key key, this.fromPrivate}) : super(key: key);

  final bool fromPrivate;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PrivateRecentChatState();
  }

}

class PrivateRecentChatState extends State<PrivateRecentChatList>{

  RecentChatListModel dataModel;
  Future<List<Data>> model;
  var db = DatabaseHelper();

  @override
  void initState() {
    super.initState();
    getLocalData();
    syncData();
  }

  Future<void> syncData() async{
    if(widget.fromPrivate) {
      var result = await Services.getRecentChatPrivate();
      if(result.data.length > 0){
        print('total length is ${result.data.length}');
        var result1 = await db.saveRecentChatList(result.data, widget.fromPrivate.toString());
        getLocalData();
      }
    } else {
      var result = await Services.getRecentChatOnetoOne();
      if(result.data.length > 0){
        print('total length is ${result.data.length}');
        var result1 = await db.saveRecentChatList(result.data, widget.fromPrivate.toString());
        getLocalData();
      }
    }

  }

//  void saveToLocalDb(RecentChatListModel value) async{
//    print(value.data.length);
//    if(value.data.length>0){
//      var result = await db.saveRecentChatList(value.data);
//    }
//  }

  void getLocalData() async {
    String userId = await SharedPreferencesHelper.getUserId();
    setState(() {
      print('my userid $userId');
      model = db.getRecentChatList(widget.fromPrivate.toString());
    });

  }

  void _navigateToChat(Data data) async{
    String userId = await SharedPreferencesHelper.getUserId();
    String token = await SharedPreferencesHelper.getToken();
    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();

    SharedPreferencesHelper.setChannelId(dynamicUrl);

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => DoctorPatientChat(
              channelId: data.channelId,
              doctorName: data.title,
              userID: userId,
              token: token,
            ))).then((value) => syncData());
  }

  Widget _myListView(
      BuildContext context, AsyncSnapshot<List<Data>> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: snapshot.data.length,
        itemBuilder: (context, index) {
          return ListTile(
            onTap: ()=> _navigateToChat(snapshot.data[index]),
            leading: ClipOval(child: CachedNetworkImage(imageUrl: snapshot.data[index].icon, width: 50.0, height: 50.0, fit:BoxFit.cover,),),
            title: (snapshot.data[index].title == null) ? Text('') : Text(snapshot.data[index].title),
            subtitle: Text(snapshot.data[index].message),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.person),
            backgroundColor: PrimaryColor,
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Contacts()));
            }
        ),
      ),
      body: Container(
          height: double.infinity,
          child: FutureBuilder<List<Data>>(
              future: model,
              builder: (context, snapshot){
                if(snapshot.hasError){
                  return Text('Error ${snapshot.error}');
                }
                if(snapshot.hasData){
                  return Stack(
                    children: <Widget>[
                      Container(
                        height: double.infinity,
                        color: Colors.white,
                      ),
                      _myListView(context, snapshot)
                    ],
                  );
                }
                return CircularProgress();
              }
          )
      ),
    );
  }

}