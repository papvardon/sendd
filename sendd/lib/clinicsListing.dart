import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/clinicSubMenu.dart';
import 'package:sendd/common/circularProgress.dart';
import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/model/clinicListModel.dart';
import 'package:sendd/utils/custColors.dart';

class ClinicListing extends StatefulWidget{
  ClinicListing({Key key}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ClinicState();
  }

}

class _ClinicState extends State<ClinicListing>{
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<ClinicListModel> model;

  @override
  void initState() {
    super.initState();
    model = Services.getMyClinics();
  }

  void _itemClick(Data item){
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ClinicSubMenu(mrn: item.mrn, clinicId: item.clinicId, clinicName: item.clinicName,)));
  }

  Widget _clinicGrid(BuildContext context, String title) {
    return InkWell(
      child: Card(
        color: Colors.white,
        child: Padding(
          padding:
          EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0, top: 10.0),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              title,
              softWrap: true,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text("My Clinics"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),),
    body: Container(
      height: double.infinity,
      color: Colors.white,
      child: FutureBuilder<ClinicListModel>(
        future: model,
        builder: (context, snapshot){
          if(snapshot.hasError){
            return Text('Error ${snapshot.error}');
          }
          if(snapshot.hasData){
            if(snapshot.data.data.length>0){
                return Padding(
                  padding: EdgeInsets.all(5.0),
                  child: GridView.count(
                    physics: ScrollPhysics(),
                    crossAxisCount: 2,
                    childAspectRatio: 1.0 / .5,
                    shrinkWrap: true,
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    children: snapshot.data.data.map(
                        (item) {
                          return GestureDetector(
                            child: GridTile(
                              child: _clinicGrid(context, item.clinicName),
                            ),
                            onTap: () {
                              _itemClick(item);
                            },
                          );
                        },
                    ).toList(),
                  ),
                );
            }
            else{
              return Center(
                child: Text('Your clinic list is eempty'),
              );
            }
          }
          return CircularProgress();
        },
      ),
    ),

    );
  }

}
