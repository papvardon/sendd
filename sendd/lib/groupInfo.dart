import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sendd/common/circularProgress.dart';
import 'package:sendd/common/dialogs.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/groupMembersAddOrEdit.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/model/groupInfoModel.dart';
import 'package:sendd/utils/custColors.dart';

class GroupInformation extends StatefulWidget{
  GroupInformation({Key key, this.groupName, this.channelId})
      : super(key: key);
  final groupName;
  final channelId;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GroupInfoState();
  }

}

class _GroupInfoState extends State<GroupInformation>{
  Future<GroupInfoModel> model;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    syncData();
  }

  void syncData() async{
    setState(() {
      model = Services.getGroupInformation(widget.channelId);
    });
  }

  void _exitGroup() async{
    Dialogs.showLoadingDialog(context, _keyLoader);
    CommonModel mdl = await Services.exitGroup(widget.channelId).catchError((error) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      print(error);
    });

    if(mdl.status == 'true'){
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(mdl.message)));
      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 2;
          });
          //Navigator.pop(context);
        });
      });
    }
    else {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(mdl.message)));
    }
  }


  void _addOrEditMember() async{
    Navigator.push(context, MaterialPageRoute(
          builder: (BuildContext context) =>
              GroupMembersAddOrEdit(channelId: widget.channelId))).then((value) => syncData);
  }


  Widget _myListView(
      BuildContext context, List<MostActive> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: snapshot.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: ClipOval(
                child: Image.network(snapshot[index].icon, height: 50.0, width: 50.0, fit: BoxFit.cover,)),
            title: Text(snapshot[index].title),
          );
        });
  }

  Widget _myListViewAllMembers(
      BuildContext context, List<AllMembers> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: snapshot.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: ClipOval(
                child: Image.network(snapshot[index].icon, height: 50.0, width: 50.0, fit: BoxFit.cover,)),
            title: Text(snapshot[index].title),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
        body: SingleChildScrollView(
          child: FutureBuilder<GroupInfoModel>(
            future: model,
            builder: (context, snapshot){
              if(snapshot.hasError){
                return Text('Error ${snapshot.error}');
              }
              if(snapshot.hasData){
                return Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CachedNetworkImage(imageUrl: snapshot.data.groupIcon != null ? snapshot.data.groupIcon : '', height: 300.0, width: MediaQuery.of(context).size.width, fit: BoxFit.cover ,),
                        SizedBox(height:10.0),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(widget.groupName, style: TextStyle(color: PrimaryColor, fontWeight: FontWeight.bold, fontSize: 18.0),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left:8.0, right: 8.0),
                          child: RichText(
                            text: TextSpan(
                                text: 'Created by   ', style: TextStyle(color: Colors.black, fontSize: 18.0),
                                children: [
                                  TextSpan(
                                      text: "${snapshot.data.createdBy} ,  ", style: TextStyle(color: PrimaryColor, fontWeight: FontWeight.bold, fontSize: 18.0)
                                  ),
                                  TextSpan(
                                      text: snapshot.data.createdTime != null ?
                                      DateFormat.yMMMd().format(new DateTime.fromMillisecondsSinceEpoch(int.parse(snapshot.data.createdTime))) : ''
                                      , style: TextStyle(color: PrimaryColor, fontWeight: FontWeight.bold, fontSize: 18.0)
                                  )
                                ]
                            ),
                          ),
                        ),
                        SizedBox(height:10.0),
                        Padding(
                          padding: const EdgeInsets.only(left:8.0, right: 8.0),
                          child: RichText(
                            text: TextSpan(
                                text: 'Total users   ', style: TextStyle(color: Colors.black, fontSize: 18.0),
                                children: [
                                  TextSpan(
                                      text: "${snapshot.data.usersCount}", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 18.0)
                                  )
                                ]
                            ),
                          ),
                        ),
                        SizedBox(height: 10.0,),
                        Padding(
                          padding: const EdgeInsets.only(top:8.0, left: 8.0),
                          child: Text('Last Active Members', style: TextStyle(color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.bold),),
                        ),
                        _myListView(context,snapshot.data.mostActive),
                        Padding(
                          padding: const EdgeInsets.only(top:8.0, left: 8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('All Members', style: TextStyle(color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.bold) ,),
                              Padding(
                                padding: const EdgeInsets.only(left:8.0, right: 8.0),
                                child: (snapshot.data.adminstatus == "1") ? InkWell(
                                  onTap: (){
                                    _addOrEditMember();
                                  },
                                  child: Chip(
                                    label: Text("Add/Edit"),
                                    avatar: CircleAvatar(
                                      backgroundColor: PrimaryColor,
                                      child: Icon(Icons.add),
                                    ),
                                  ),
                                ) : Container()
                              )
                            ],
                          ),
                        ),
                        _myListViewAllMembers(context, snapshot.data.allMembers),
                        Align(
                          alignment: Alignment.center,
                          child: RaisedButton(
                            child: Text('Exit Group'),
                            color: Colors.red,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(16.0))),
                            onPressed: () {_exitGroup();},
                          ),
                        ),
                        SizedBox(height: 20.0,)
                      ],
                    ),
                    Positioned(
                      top: 20.0,
                      left: 10.0,
                      child: IconButton(
                        icon: new Icon(Icons.arrow_back),
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                      ),
                    ),
                  ],
                );
              }
              return CircularProgress();
            },
          ),
        )
    );
  }

}