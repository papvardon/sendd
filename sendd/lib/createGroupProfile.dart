import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/bundleDatePickerField.dart';
import 'common/dialogs.dart';
import 'data/databaseHelper.dart';
import 'data/services.dart';
import 'data/sharedpreferenceshelper.dart';
import 'model/dataBundle.dart';

class CreateGroupProfile extends StatefulWidget{
  CreateGroupProfile({Key key, this.selectedContact, this.id}) : super(key: key);

  final List selectedContact;
  final id;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CreateGroupProfileState();
  }

}

class _CreateGroupProfileState extends State<CreateGroupProfile> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  var db = DatabaseHelper();
  File _image;
  final picker = ImagePicker();
  DataBundle bdModel = new DataBundle();
  String dates;
  var formatter = new DateFormat('dd-MM-yyyy');
  CommonModel model;

  final _grpName = TextEditingController();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    var date = new DateTime.now();
    bdModel.startdate = formatter.format(new DateTime(date.year, date.month, date.day));
    bdModel.enddate = formatter.format(new DateTime(date.year + 25, date.month, date.day));
    bdModel.heading = "Group Validity Date";
  }


  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  void _createGroup() async{

    String token = await SharedPreferencesHelper.getToken();
    String userId = await SharedPreferencesHelper.getUserId();
    String pic;
    if(_image != null){
      final bytes = _image.readAsBytesSync();
      pic = base64Encode(bytes);
    }


    Map<String, dynamic> map = new Map();
    map['userId'] = userId;
    map['token'] = token;
    map['Ids'] = widget.selectedContact;
    map['groupName'] = _grpName.text;
    map['expiresAt'] = dates;
    map['icon'] = pic;
    map['BoBGroup'] = '';
    map['ouid'] = widget.id;

    Dialogs.showLoadingDialog(context, _keyLoader);
    String encodedJSON = jsonEncode(map);
    print(encodedJSON);

    model = await Services.doGroupCreation(encodedJSON).catchError((error) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      print(error);
    });

    if(model.status == 'true'){
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(model.message)));
      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 2;
          });
          //Navigator.pop(context);
        });
      });
    }
    else {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(model.message)));
    }


  }

  Widget _entryField() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Group Name",
              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12),
            ),
            TextFormField(
                style: TextStyle(
                  color: Colors.black,
                ),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Enter Group Name';
                  }
                  return null;
                },
                controller: _grpName,
                obscureText: false,
                decoration: InputDecoration(
                  focusColor: PrimaryColor,
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color : Colors.grey)
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _submitButton() {
    return InkWell(
      onTap: () {
        if(_grpName.text.isNotEmpty){
          if(dates.isNotEmpty){
              _createGroup();
          }
          else
            _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text('Please give a validity date')));
        }
        else
          _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text('Please enter group name')));
      },
      child: Container(
        width: MediaQuery.of(context).size.width/3,
        height: MediaQuery.of(context).size.height/15,
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: PrimaryColor,
            borderRadius: BorderRadius.all(Radius.circular(40)),
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [buttonGradientEnd, buttonGradientStart])
          /*boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.blue.shade500,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],*/
        ),
        child: Text(
          'Create',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('Create Group'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            color: Colors.white,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () => getImage(),
                child: Center(
                  child: _image != null ? ClipOval(child: Image.file(_image, width: 150.0, height: 150.0, fit:BoxFit.cover),) : CircleAvatar(child: Icon(Icons.photo, size: 80.0,), radius: 100, backgroundColor: Colors.grey,)
                ),
              ),
              _entryField(),
              BundleDatePickerField(
                model: bdModel,
                onSaved: (String value) {
                  DateTime tm = formatter.parse(value);
                  dates = (tm.millisecondsSinceEpoch/1000).round().toString();
                },
              ),
              _submitButton()
            ],
          )
        ],
      ),
    );
  }
}