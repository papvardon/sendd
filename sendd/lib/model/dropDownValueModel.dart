class DropDownValueModel{
  String heading;
  String value;

  DropDownValueModel({this.heading, this.value});

  DropDownValueModel.fromJson(Map<String, dynamic> json) {
    heading = json['heading'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['heading'] = this.heading;
    data['value'] = this.value;
    return data;
  }
}