class CreateChannelIdModel {
  List<Datas> data;

  CreateChannelIdModel({this.data});

  CreateChannelIdModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Datas>();
      json['data'].forEach((v) {
        data.add(new Datas.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Datas {
  String id;
  String title;
  String icon;

  Datas({this.id, this.title, this.icon});

  Datas.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['icon'] = this.icon;
    return data;
  }
}

