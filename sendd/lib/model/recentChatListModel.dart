import 'package:sendd/utils/dataEncrypt.dart';

class RecentChatListModel {
  List<Data> data;

  RecentChatListModel({this.data});

  RecentChatListModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String title;
  String icon;
  String channelId;
  String message;
  String createAt;
  String userId;
  String unreadCount;
  String available;
  String category;
  String senderId;
  String acceptStatus;
  String blockStatus;
  String phoneno;
  String profileId;

  Data(
      {this.title,
        this.icon,
        this.channelId,
        this.message,
        this.createAt,
        this.userId,
        this.unreadCount,
        this.available,
        this.category,
        this.senderId,
        this.acceptStatus,
        this.blockStatus,
        this.phoneno,
        this.profileId});

  Data.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    icon = json['icon'];
    channelId = json['ChannelId'];
    message = json['Message'];
    createAt = json['CreateAt'];
    userId = json['userId'];
    unreadCount = json['unreadCount'];
    available = json['Available'];
    category = json['Category'];
    senderId = json['senderId'];
    acceptStatus = json['acceptStatus'];
    blockStatus = json['blockStatus'];
    phoneno = json['phoneno'];
    profileId = json['profileId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['icon'] = this.icon;
    data['ChannelId'] = this.channelId;
    data['Message'] = this.message;
    data['CreateAt'] = this.createAt;
    data['userId'] = this.userId;
    data['unreadCount'] = this.unreadCount;
    data['Available'] = this.available;
    data['Category'] = this.category;
    data['senderId'] = this.senderId;
    data['acceptStatus'] = this.acceptStatus;
    data['blockStatus'] = this.blockStatus;
    data['phoneno'] = this.phoneno;
    data['profileId'] = this.profileId;
    return data;
  }

}

