
class Dashboarddatamodel {

  String icon;
  String title;
  String channelId;
  String id;
  String linkid;


	Dashboarddatamodel(this.icon, this.title, this.channelId, this.id);

	Dashboarddatamodel.fromJsonMap(Map<String, dynamic> map):
		icon = map["icon"],
		title = map["title"],
		channelId = map["channelId"],
		id = map["id"],
		linkid = map["linkid"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['icon'] = icon;
		data['title'] = title;
		data['channelId'] = channelId;
		data['id'] = id;
		data['linkid'] = linkid;
		return data;
	}
}
