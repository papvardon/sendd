class ClinicListModel {
  bool status;
  List<Data> data;

  ClinicListModel({this.status, this.data});

  ClinicListModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String clinicId;
  String mrn;
  String clinicName;

  Data({this.clinicId, this.mrn, this.clinicName});

  Data.fromJson(Map<String, dynamic> json) {
    clinicId = json['clinic_id'];
    mrn = json['mrn'];
    clinicName = json['clinic_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['clinic_id'] = this.clinicId;
    data['mrn'] = this.mrn;
    data['clinic_name'] = this.clinicName;
    return data;
  }
}

