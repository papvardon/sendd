import 'cardFrontSideModel.dart';

class VisitSummaryDetailModel {
  String status;
  List<Data> data;

  VisitSummaryDetailModel({this.status, this.data});

  VisitSummaryDetailModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String informationType;
  String path;
  CardFrontSideModel frontSide;
  Bottomcontent bottomcontent;

  Data({this.informationType, this.path, this.frontSide, this.bottomcontent});

  Data.fromJson(Map<String, dynamic> json) {
    informationType = json['information_type'];
    path = json['path'];
    frontSide = json['front_side'] != null
        ? new CardFrontSideModel.fromJson(json['front_side'])
        : null;
    bottomcontent = json['bottomcontent'] != null
        ? new Bottomcontent.fromJson(json['bottomcontent'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['information_type'] = this.informationType;
    data['path'] = this.path;
    if (this.frontSide != null) {
      data['front_side'] = this.frontSide.toJson();
    }
    if (this.bottomcontent != null) {
      data['bottomcontent'] = this.bottomcontent.toJson();
    }
    return data;
  }
}

/*class FrontSide {
  String bg;
  ContentText contentText;

  FrontSide({this.bg, this.contentText});

  FrontSide.fromJson(Map<String, dynamic> json) {
    bg = json['bg'];
    contentText = json['content_text'] != null
        ? new ContentText.fromJson(json['content_text'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bg'] = this.bg;
    if (this.contentText != null) {
      data['content_text'] = this.contentText.toJson();
    }
    return data;
  }
}

class ContentText {
  String name;
  String mobile;
  String title2;
  String value2;
  String photo;

  ContentText({this.name, this.mobile, this.title2, this.value2, this.photo});

  ContentText.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    mobile = json['Mobile'];
    title2 = json['title2'];
    value2 = json['value2'];
    photo = json['Photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['Mobile'] = this.mobile;
    data['title2'] = this.title2;
    data['value2'] = this.value2;
    data['Photo'] = this.photo;
    return data;
  }
}*/

class Bottomcontent {
  String heading;
  List<Content> content;

  Bottomcontent({this.heading, this.content});

  Bottomcontent.fromJson(Map<String, dynamic> json) {
    heading = json['heading'];
    if (json['content'] != null) {
      content = new List<Content>();
      json['content'].forEach((v) {
        content.add(new Content.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['heading'] = this.heading;
    if (this.content != null) {
      data['content'] = this.content.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Content {
  String title;
  String type;
  String value;

  Content({this.title, this.type, this.value});

  Content.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    type = json['type'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['type'] = this.type;
    data['value'] = this.value;
    return data;
  }
}