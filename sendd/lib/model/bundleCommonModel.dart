
import 'package:sendd/model/tabBundleModel.dart';

class BundleCommonModel {
  String status;
  String message;
  List<TabBundleModel> tabs;

  BundleCommonModel({this.status, this.tabs});

  BundleCommonModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['tabs'] != null) {
      tabs = new List<TabBundleModel>();
      json['tabs'].forEach((v) {
        tabs.add(new TabBundleModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.tabs != null) {
      data['tabs'] = this.tabs.map((v) => v.toJson()).toList();
    }
    print(data);
    return data;
  }
}