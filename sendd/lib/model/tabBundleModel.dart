import 'dataBundle.dart';

class TabBundleModel{

  String tab;
  String heading;
  String editable;
  String buttonBackground;
  String topbarBackground;
  List<DataBundle> data;

  TabBundleModel(
      {this.tab,
        this.heading,
        this.editable,
        this.buttonBackground,
        this.topbarBackground,
        this.data});

  TabBundleModel.fromJson(Map<String, dynamic> json) {
    tab = json['tab'];
    heading = json['heading'];
    editable = json['editable'];
    buttonBackground = json['buttonBackground'];
    topbarBackground = json['topbarBackground'];
    if (json['data'] != null) {
      data = new List<DataBundle>();
      json['data'].forEach((v) {
        data.add(new DataBundle.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tab'] = this.tab;
    data['heading'] = this.heading;
    data['editable'] = this.editable;
    data['buttonBackground'] = this.buttonBackground;
    data['topbarBackground'] = this.topbarBackground;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}