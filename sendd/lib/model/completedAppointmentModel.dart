class CompletedAppointmentModel {
  String status;
  List<Data> data;

  CompletedAppointmentModel({this.status, this.data});

  CompletedAppointmentModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String appointmentNo;
  String appointmentDate;
  String appointmentSlotTime;
  String mrn;
  String patientName;
  String doctorName;
  String patientContact;
  String visitNumber;
  String visitId;
  String paymentstatus;
  String appointment_fees;
  String totalamount;

  Data(
      {this.appointmentNo,
        this.appointmentDate,
        this.appointmentSlotTime,
        this.mrn,
        this.patientName,
        this.doctorName,
        this.patientContact,
        this.visitNumber,
        this.paymentstatus,
        this.appointment_fees,
        this.totalamount,
        this.visitId});

  Data.fromJson(Map<String, dynamic> json) {
    appointmentNo = json['appointment_no'];
    appointmentDate = json['appointment_date'];
    appointmentSlotTime = json['appointment_slot_time'];
    mrn = json['mrn'];
    patientName = json['patient_name'];
    doctorName = json['doctor_name'];
    patientContact = json['patient_contact'];
    visitNumber = json['visit_number'];
    visitId = json['visit_id'];
    appointment_fees = json['appointment_fees'];
    totalamount = json['totalamount'];
    paymentstatus = json['payment_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['appointment_no'] = this.appointmentNo;
    data['appointment_date'] = this.appointmentDate;
    data['appointment_slot_time'] = this.appointmentSlotTime;
    data['mrn'] = this.mrn;
    data['patient_name'] = this.patientName;
    data['doctor_name'] = this.doctorName;
    data['patient_contact'] = this.patientContact;
    data['visit_number'] = this.visitNumber;
    data['visit_id'] = this.visitId;
    data['payment_status'] = this.paymentstatus;
    return data;
  }
}

