class LabPrescriptionModel {
  bool status;
  String labPrescriptionId;
  String orderNumber;
  List<Items> items;
  int orderDate;
  String visitNumber;

  LabPrescriptionModel(
      {this.status,
        this.labPrescriptionId,
        this.orderNumber,
        this.items,
        this.orderDate,
        this.visitNumber});

  LabPrescriptionModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    labPrescriptionId = json['lab_prescription_id'];
    orderNumber = json['order_number'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
    orderDate = json['order_date'];
    visitNumber = json['visit_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['lab_prescription_id'] = this.labPrescriptionId;
    data['order_number'] = this.orderNumber;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    data['order_date'] = this.orderDate;
    data['visit_number'] = this.visitNumber;
    return data;
  }
}

class Items {
  List<ItemDetails> details;

  Items({this.details});

  Items.fromJson(Map<String, dynamic> json) {
    if (json['details'] != null) {
      details = new List<ItemDetails>();
      json['details'].forEach((v) {
        details.add(new ItemDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.details != null) {
      data['details'] = this.details.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemDetails {
  String title;
  String type;
  String value;

  ItemDetails({this.title, this.type, this.value});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    type = json['type'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['type'] = this.type;
    data['value'] = this.value;
    return data;
  }
}

