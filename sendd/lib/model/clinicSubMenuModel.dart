class ClinicSubMenuModel {
  bool status;
  String clinicId;
  String userId;
  String token;
  List<Data> data;

  ClinicSubMenuModel(
      {this.status, this.clinicId, this.userId, this.token, this.data});

  ClinicSubMenuModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    clinicId = json['clinic_id'];
    userId = json['user_id'];
    token = json['token'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['clinic_id'] = this.clinicId;
    data['user_id'] = this.userId;
    data['token'] = this.token;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String id;
  String title;
  String icon;
  String channelId;
  String path;

  Data({this.id, this.title, this.icon, this.channelId, this.path});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    icon = json['icon'];
    channelId = json['channelId'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['icon'] = this.icon;
    data['channelId'] = this.channelId;
    data['path'] = this.path;
    return data;
  }
}

