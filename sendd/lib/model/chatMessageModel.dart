class ChatMessageModel{
  String id;
  String sender;
  String senderId;
  int noOfReply;
  int noOfLikes;
  int createAt;
  int updateAt;
  int deleteAt;
  String channelId;
  String rootId;
  String parentId;
  String originalId;
  String message;
  String type;
  String hashtags;
  String filenames;
  String icon;
  String msgReadBy;
  String category;
  String statStatus;
  String attachmentName;
  String userStatus;
  String setLocalFile;
  String mId;
  List<ChatMessageModel> subchatmessages;
  String replyMsg;

  ChatMessageModel(
  {this.id,
  this.sender,
  this.senderId,
  this.noOfReply,
  this.noOfLikes,
  this.createAt,
  this.updateAt,
  this.deleteAt,
  this.channelId,
  this.rootId,
  this.parentId,
  this.originalId,
  this.message,
  this.type,
  this.hashtags,
  this.filenames,
  this.icon,
  this.msgReadBy,
  this.category,
  this.statStatus,
  this.attachmentName,
  this.setLocalFile,
  this.userStatus,
  this.mId,
    this.subchatmessages,
  this.replyMsg});

  ChatMessageModel.fromJson(Map<String, dynamic> json) {
  id = json['id'];
  sender = json['sender'];
  senderId = json['sender_id'];
  noOfReply = json['no_of_reply'];
  noOfLikes = json['no_of_likes'];
  createAt = json['create_at'];
  updateAt = json['update_at'];
  deleteAt = json['delete_at'];
  channelId = json['channel_id'];
  rootId = json['root_id'];
  parentId = json['parent_id'];
  originalId = json['original_id'];
  message = json['message'];
  type = json['type'];
  hashtags = json['hashtags'];
  filenames = json['filenames'];
  icon = json['icon'];
  msgReadBy = json['msgReadBy'];
  category = json['Category'];
  statStatus = json['StatStatus'];
  attachmentName = json['AttachmentName'];
  setLocalFile = json['SetLocalFile'];
  userStatus = json['userStatus'];
  mId = json['mId'];
  if (json['subchatmessages'] != null) {
    subchatmessages = new List<ChatMessageModel>();
    json['subchatmessages'].forEach((v) { subchatmessages.add(new ChatMessageModel.fromJson(v)); });
  }
  replyMsg = json['replyMsg'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['id'] = this.id;
  data['sender'] = this.sender;
  data['sender_id'] = this.senderId;
  data['no_of_reply'] = this.noOfReply;
  data['no_of_likes'] = this.noOfLikes;
  data['create_at'] = this.createAt;
  data['update_at'] = this.updateAt;
  data['delete_at'] = this.deleteAt;
  data['channel_id'] = this.channelId;
  data['root_id'] = this.rootId;
  data['parent_id'] = this.parentId;
  data['original_id'] = this.originalId;
  data['message'] = this.message;
  data['type'] = this.type;
  data['hashtags'] = this.hashtags;
  data['filenames'] = this.filenames;
  data['icon'] = this.icon;
  data['msgReadBy'] = this.msgReadBy;
  data['Category'] = this.category;
  data['StatStatus'] = this.statStatus;
  data['AttachmentName'] = this.attachmentName;
  data['SetLocalFile'] = this.setLocalFile;
  data['userStatus'] = this.userStatus;
  data['mId'] = this.mId;
  if (this.subchatmessages != null) {
    data['subchatmessages'] = this.subchatmessages.map((v) => v.toJson()).toList();
  }
  data['replyMsg'] = this.replyMsg;
  return data;
  }
}