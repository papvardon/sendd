class MedicinePrescriptionModel {
  bool status;
  String orderNumber;
  List<Items> items;
  int orderDate;
  String visitNumber;

  MedicinePrescriptionModel(
      {this.status,
        this.orderNumber,
        this.items,
        this.orderDate,
        this.visitNumber});

  MedicinePrescriptionModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    orderNumber = json['order_number'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
    orderDate = json['order_date'];
    visitNumber = json['visit_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['order_number'] = this.orderNumber;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    data['order_date'] = this.orderDate;
    data['visit_number'] = this.visitNumber;
    return data;
  }
}

class Items {
  List<Details> details;

  Items({this.details});

  Items.fromJson(Map<String, dynamic> json) {
    if (json['details'] != null) {
      details = new List<Details>();
      json['details'].forEach((v) {
        details.add(new Details.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.details != null) {
      data['details'] = this.details.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Details {
  String title;
  String type;
  String value;

  Details({this.title, this.type, this.value});

  Details.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    type = json['type'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['type'] = this.type;
    data['value'] = this.value;
    return data;
  }
}

