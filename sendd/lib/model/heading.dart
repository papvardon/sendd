
import 'dashboarddatamodel.dart';

class Heading {

  String id;
  String name;
  String icon;
  List<Dashboarddatamodel> data;

	Heading(this.id, this.name, this.icon, this.data);

	Heading.fromJsonMap(Map<String, dynamic> map):
		id = map["id"],
		name = map["name"],
		icon = map["icon"],
		data = List<Dashboarddatamodel>.from(map["data"].map((it) => Dashboarddatamodel.fromJsonMap(it)));

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['name'] = name;
		data['icon'] = icon;
		data['data'] = data != null ? 
			this.data.map((v) => v.toJson()).toList()
			: null;
		return data;
	}
}
