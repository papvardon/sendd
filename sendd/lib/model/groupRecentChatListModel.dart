class GroupRecentChatListModel {
  GroupData data;

  GroupRecentChatListModel({this.data});

  GroupRecentChatListModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new GroupData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GroupData {
  List<Organisation> organisation;

  GroupData({this.organisation});

  GroupData.fromJson(Map<String, dynamic> json) {
    if (json['organisation'] != null) {
      organisation = new List<Organisation>();
      json['organisation'].forEach((v) {
        organisation.add(new Organisation.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.organisation != null) {
      data['organisation'] = this.organisation.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Organisation {
  String id;
  String title;
  String icon;
  String location;
  List<Groups> groups;
  bool isExpanded;

  Organisation({this.id, this.title, this.icon, this.location, this.groups, this.isExpanded = false});

  Organisation.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    icon = json['icon'];
    location = json['location'];
    if (json['groups'] != null) {
      groups = new List<Groups>();
      json['groups'].forEach((v) {
        groups.add(new Groups.fromJson(v));
      });
    }
    isExpanded = json['isExpanded'] !=null ? json['isExpanded'] : false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['icon'] = this.icon;
    data['location'] = this.location;
    if (this.groups != null) {
      data['groups'] = this.groups.map((v) => v.toJson()).toList();
    }
    data['isExpanded'] = this.isExpanded;
    return data;
  }
}

class Groups {
  String id;
  String title;
  dynamic membersCount;
  String icon;
  String unreadCount;
  String header;
  String purpose;
  String adminName;
  String acceptStatus;
  String adminIcon;

  Groups(
      {this.id,
        this.title,
        this.membersCount,
        this.icon,
        this.unreadCount,
        this.header,
        this.purpose,
        this.adminName,
        this.acceptStatus,
        this.adminIcon});

  Groups.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    membersCount = json['members_count'];
    icon = json['icon'];
    unreadCount = json['unreadCount'];
    header = json['Header'];
    purpose = json['Purpose'];
    adminName = json['adminName'];
    acceptStatus = json['acceptStatus'];
    adminIcon = json['adminIcon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['members_count'] = this.membersCount;
    data['icon'] = this.icon;
    data['unreadCount'] = this.unreadCount;
    data['Header'] = this.header;
    data['Purpose'] = this.purpose;
    data['adminName'] = this.adminName;
    data['acceptStatus'] = this.acceptStatus;
    data['adminIcon'] = this.adminIcon;
    return data;
  }
}

