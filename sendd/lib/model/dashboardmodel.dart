
import 'package:sendd/model/heading.dart';

class DashboardModel {

  bool status;
  String hospitaltitle;
  String hospitallogo;
  List<Heading> heading;
  String broadcastid;
	String alertChannel;

	DashboardModel({this.status, this.hospitaltitle, this.hospitallogo, this.heading, this.broadcastid, this.alertChannel});

	DashboardModel.fromJsonMap(Map<String, dynamic> map):
		status = map["status"],
		hospitaltitle = map["hospitaltitle"],
		hospitallogo = map["hospitallogo"],
		heading = List<Heading>.from(map["heading"].map((it) => Heading.fromJsonMap(it))),
		broadcastid = map["broadcastid"],
		alertChannel = map["alertChannel"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['hospitaltitle'] = hospitaltitle;
		data['hospitallogo'] = hospitallogo;
		data['heading'] = heading != null ? 
			this.heading.map((v) => v.toJson()).toList()
			: null;
		data['broadcastid'] = broadcastid;
		data['alertChannel'] = alertChannel;
		return data;
	}

}
