class OuListModel {
  String status;
  List<Organisationunit> organisationunit;

  OuListModel({this.status, this.organisationunit});

  OuListModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['organisationunit'] != null) {
      organisationunit = new List<Organisationunit>();
      json['organisationunit'].forEach((v) {
        organisationunit.add(new Organisationunit.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.organisationunit != null) {
      data['organisationunit'] =
          this.organisationunit.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Organisationunit {
  String title;
  String id;
  String icon;

  Organisationunit({this.title, this.id, this.icon});

  Organisationunit.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    id = json['id'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['id'] = this.id;
    data['icon'] = this.icon;
    return data;
  }
}

