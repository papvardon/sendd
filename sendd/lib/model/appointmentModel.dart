class AppointmentModel {
  String status;
  String message;
  String appointmentno;

  AppointmentModel({this.status, this.message, this.appointmentno});

  AppointmentModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    appointmentno = json['appointmentno'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['appointmentno'] = this.appointmentno;
    return data;
  }
}

