
import 'dropDownValueModel.dart';

class DataBundle {
  String category;
  String icon;
  String heading;
  String parameter;
  String value;
  String editable;
  String type;
  String latitude;
  String longitude;
  Null diagram;
  Null table;
  String startdate;
  String enddate;
  List<ListBundle> list;

  DataBundle(
      {this.category,
        this.icon,
        this.heading,
        this.parameter,
        this.value,
        this.editable,
        this.type,
        this.latitude,
        this.longitude,
        this.diagram,
        this.table,
        this.startdate,
        this.enddate,
        this.list});

  DataBundle.fromJson(Map<String, dynamic> json) {
    category = json['category'];
    icon = json['icon'];
    heading = json['heading'];
    parameter = json['parameter'];
    value = json['value'];
    editable = json['editable'];
    type = json['type'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    diagram = json['diagram'];
    table = json['table'];
    startdate = json['startdate'];
    enddate = json['enddate'];
    if (json['list'] != null) {
      list = new List<ListBundle>();
      json['list'].forEach((v) {
        list.add(new ListBundle.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    data['icon'] = this.icon;
    data['heading'] = this.heading;
    data['parameter'] = this.parameter;
    data['value'] = this.value;
    data['editable'] = this.editable;
    data['type'] = this.type;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['diagram'] = this.diagram;
    data['table'] = this.table;
    data['enddate'] = this.enddate;
    data['startdate'] = this.startdate;
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListBundle {
  String icon;
  List<DropDownValueModel> data;

  ListBundle({this.icon, this.data});

  ListBundle.fromJson(Map<String, dynamic> json) {
    icon = json['icon'];
    if (json['data'] != null) {
      data = new List<DropDownValueModel>();
      json['data'].forEach((v) {
        data.add(new DropDownValueModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['icon'] = this.icon;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

