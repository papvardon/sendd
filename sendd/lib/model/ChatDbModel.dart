class ChatDbModel{

  String id;
  String sender;
  String senderId;
  int noOfReply;
  int noOfLikes;
  int createAt;
  int updateAt;
  int deleteAt;
  String channelId;
  String rootId;
  String parentId;
  String originalId;
  String message;
  String type;
  String hashtags;
  String filenames;
  String icon;
  String msgReadBy;
  String category;
  String statStatus;
  String attachmentName;
  String userStatus;
  String setLocalFile;
  String mId;
  String repliedMessage;
  String repliedName;
  String repliedImage;
  String repliedSenderID;
  String repliedDate;
  String repliedCategory;
  String repliedPostImage;
  int color;


  ChatDbModel(
      {this.id,
        this.sender,
        this.senderId,
        this.noOfReply,
        this.noOfLikes,
        this.createAt,
        this.updateAt,
        this.deleteAt,
        this.channelId,
        this.rootId,
        this.parentId,
        this.originalId,
        this.message,
        this.type,
        this.hashtags,
        this.filenames,
        this.icon,
        this.msgReadBy,
        this.category,
        this.statStatus,
        this.attachmentName,
        this.setLocalFile,
        this.userStatus,
        this.mId,
        this.repliedMessage,
        this.repliedImage,
        this.repliedName,
        this.repliedCategory,
        this.repliedDate,
        this.repliedPostImage,
        this.repliedSenderID,
        this.color
      });

  ChatDbModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sender = json['sender'];
    senderId = json['sender_id'];
    noOfReply = json['no_of_reply'];
    noOfLikes = json['no_of_likes'];
    createAt = json['create_at'];
    updateAt = json['update_at'];
    deleteAt = json['delete_at'];
    channelId = json['channel_id'];
    rootId = json['root_id'];
    parentId = json['parent_id'];
    originalId = json['original_id'];
    message = json['message'];
    type = json['type'];
    hashtags = json['hashtags'];
    filenames = json['filenames'];
    icon = json['icon'];
    msgReadBy = json['msgReadBy'];
    category = json['Category'];
    statStatus = json['StatStatus'];
    attachmentName = json['AttachmentName'];
    setLocalFile = json['SetLocalFile'];
    userStatus = json['userStatus'];
    mId = json['mId'];
    repliedMessage = json['replied_message'];
    repliedSenderID = json['replied_sender_id'];
    repliedPostImage = json['replied_post_image'];
    repliedDate = json['replied_date'];
    repliedCategory = json['replied_category'];
    repliedName = json['replied_name'];
    color = json['color'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sender'] = this.sender;
    data['sender_id'] = this.senderId;
    data['no_of_reply'] = this.noOfReply;
    data['no_of_likes'] = this.noOfLikes;
    data['create_at'] = this.createAt;
    data['update_at'] = this.updateAt;
    data['delete_at'] = this.deleteAt;
    data['channel_id'] = this.channelId;
    data['root_id'] = this.rootId;
    data['parent_id'] = this.parentId;
    data['original_id'] = this.originalId;
    data['message'] = this.message;
    data['type'] = this.type;
    data['hashtags'] = this.hashtags;
    data['filenames'] = this.filenames;
    data['icon'] = this.icon;
    data['msgReadBy'] = this.msgReadBy;
    data['Category'] = this.category;
    data['StatStatus'] = this.statStatus;
    data['AttachmentName'] = this.attachmentName;
    data['SetLocalFile'] = this.setLocalFile;
    data['userStatus'] = this.userStatus;
    data['mId'] = this.mId;
    data['replied_message'] = this.repliedMessage;
    data['replied_sender_id'] = this.repliedSenderID;
    data['replied_post_image'] = this.repliedPostImage;
    data['replied_date'] = this.repliedDate;
    data['replied_category'] = this.repliedCategory;
    data['replied_name'] = this.repliedName;
    data['color'] = this.color;
    return data;
  }
}