class CountryModel {
  String status;
  List<Data> data;

  CountryModel({this.status, this.data});

  CountryModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String id;
  String name;
  String isocode;
  String isdcode;
  String icon;
  String enablestatus;

  Data(
      {this.id,
        this.name,
        this.isocode,
        this.isdcode,
        this.icon,
        this.enablestatus});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    isocode = json['isocode'];
    isdcode = json['isdcode'];
    icon = json['icon'];
    enablestatus = json['enablestatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['isocode'] = this.isocode;
    data['isdcode'] = this.isdcode;
    data['icon'] = this.icon;
    data['enablestatus'] = this.enablestatus;
    return data;
  }
}

