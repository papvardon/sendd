class InitiateOrderIdModel {
  String status;
  String orderid;

  InitiateOrderIdModel({this.status, this.orderid});

  InitiateOrderIdModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    orderid = json['orderid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['orderid'] = this.orderid;
    return data;
  }
}

