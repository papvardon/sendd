
import 'package:sendd/model/loginData.dart';

class Loginresponse {

  final List<LoginData> data;
  final String message;
  final String resCode;
  final String status;
  final String profile_id;

	Loginresponse.fromJsonMap(Map<String, dynamic> map):
		data = List<LoginData>.from(map["data"].map((it) => LoginData.fromJsonMap(it))),
		message = map["message"],
		resCode = map["resCode"],
		status = map["status"],
		profile_id = map["profile_id"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['data'] = data != null ? 
			this.data.map((v) => v.toJson()).toList()
			: null;
		data['message'] = message;
		data['resCode'] = resCode;
		data['status'] = status;
		data['profile_id'] = profile_id;
		return data;
	}


}
