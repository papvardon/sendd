class ContactsModel {
  List<CData> data;

  ContactsModel({this.data});

  ContactsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<CData>();
      json['data'].forEach((v) {
        data.add(new CData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CData {
  String title;
  String icon;
  String id;
  String firstName;
  String nickname;
  String lastName;
  String profileid;
  String status;
  String available;

  CData(
      {this.title,
        this.icon,
        this.id,
        this.firstName,
        this.nickname,
        this.lastName,
        this.profileid,
        this.status,
        this.available});

  CData.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    icon = json['icon'];
    id = json['id'];
    firstName = json['FirstName'];
    nickname = json['Nickname'];
    lastName = json['LastName'];
    profileid = json['profileid'];
    status = json['status'];
    available = json['Available'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['icon'] = this.icon;
    data['id'] = this.id;
    data['FirstName'] = this.firstName;
    data['Nickname'] = this.nickname;
    data['LastName'] = this.lastName;
    data['profileid'] = this.profileid;
    data['status'] = this.status;
    data['Available'] = this.available;
    return data;
  }
}

