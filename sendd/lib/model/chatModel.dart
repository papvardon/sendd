
import 'package:sendd/model/chatMessageModel.dart';

class ChatModel {
  List<ChatMessageModel> data;

  ChatModel({this.data});

  ChatModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<ChatMessageModel>();
      json['data'].forEach((v) {
        data.add(new ChatMessageModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}



