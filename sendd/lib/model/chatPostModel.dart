import 'chatMessageModel.dart';

class ChatPostModel{
  String status;
  ChatMessageModel data;

  ChatPostModel({this.status, this.data});

  ChatPostModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new ChatMessageModel.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}