
class LoginData {

  final String IP;
  final String SlaveLogo;
  final String SlaveName;
  final String create_at;
  final String email;
  final String icon;
  final String id;
  final String regId;
  final String roleType;
  final String roles;
  final String status;
  final String team_id;
  final String token;
  final String update_at;
  final String username;
  final String ftrset;
  final String supportEmail;
  final String supportNumber;
  final String UserStatus;
  final String focusstatus;

	LoginData.fromJsonMap(Map<String, dynamic> map):
		IP = map["IP"],
		SlaveLogo = map["SlaveLogo"],
		SlaveName = map["SlaveName"],
		create_at = map["create_at"],
		email = map["email"],
		icon = map["icon"],
		id = map["id"],
		regId = map["regId"],
		roleType = map["roleType"],
		roles = map["roles"],
		status = map["status"],
		team_id = map["team_id"],
		token = map["token"],
		update_at = map["update_at"],
		username = map["username"],
		ftrset = map["ftrset"],
		supportEmail = map["supportEmail"],
		supportNumber = map["supportNumber"],
		UserStatus = map["UserStatus"],
		focusstatus = map["focusstatus"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['IP'] = IP;
		data['SlaveLogo'] = SlaveLogo;
		data['SlaveName'] = SlaveName;
		data['create_at'] = create_at;
		data['email'] = email;
		data['icon'] = icon;
		data['id'] = id;
		data['regId'] = regId;
		data['roleType'] = roleType;
		data['roles'] = roles;
		data['status'] = status;
		data['team_id'] = team_id;
		data['token'] = token;
		data['update_at'] = update_at;
		data['username'] = username;
		data['ftrset'] = ftrset;
		data['supportEmail'] = supportEmail;
		data['supportNumber'] = supportNumber;
		data['UserStatus'] = UserStatus;
		data['focusstatus'] = focusstatus;
		return data;
	}
}
