class SignUpUserModel{

  String firstName;
  String lastName;
  String mobileNo;
  String emailId;
  String password;
  String confirmPassword;
  String countryCode;


  SignUpUserModel({this.firstName, this.lastName, this.emailId,
      this.mobileNo, this.password, this.confirmPassword, this.countryCode});

}