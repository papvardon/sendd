class GroupInfoModel {
  List<MostActive> mostActive;
  List<AllMembers> allMembers;
  String groupIcon;
  int usersCount;
  String lastActiveTime;
  String createdTime;
  String createdBy;
  String expireAt;
  String adminstatus;

  GroupInfoModel(
      {this.mostActive,
        this.allMembers,
        this.groupIcon,
        this.usersCount,
        this.lastActiveTime,
        this.createdTime,
        this.createdBy,
        this.expireAt,
        this.adminstatus});

  GroupInfoModel.fromJson(Map<String, dynamic> json) {
    if (json['mostActive'] != null) {
      mostActive = new List<MostActive>();
      json['mostActive'].forEach((v) {
        mostActive.add(new MostActive.fromJson(v));
      });
    }
    if (json['AllMembers'] != null) {
      allMembers = new List<AllMembers>();
      json['AllMembers'].forEach((v) {
        allMembers.add(new AllMembers.fromJson(v));
      });
    }
    groupIcon = json['groupIcon'];
    usersCount = json['usersCount'];
    lastActiveTime = json['LastActiveTime'];
    createdTime = json['CreatedTime'];
    createdBy = json['CreatedBy'];
    expireAt = json['ExpireAt'];
    adminstatus = json['adminstatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mostActive != null) {
      data['mostActive'] = this.mostActive.map((v) => v.toJson()).toList();
    }
    if (this.allMembers != null) {
      data['AllMembers'] = this.allMembers.map((v) => v.toJson()).toList();
    }
    data['groupIcon'] = this.groupIcon;
    data['usersCount'] = this.usersCount;
    data['LastActiveTime'] = this.lastActiveTime;
    data['CreatedTime'] = this.createdTime;
    data['CreatedBy'] = this.createdBy;
    data['ExpireAt'] = this.expireAt;
    data['adminstatus'] = this.adminstatus;
    return data;
  }
}

class MostActive {
  String title;
  String icon;
  String id;
  String status;
  String qualification;
  String speciality;
  String lastViewedAt;

  MostActive(
      {this.title,
        this.icon,
        this.id,
        this.status,
        this.qualification,
        this.speciality,
        this.lastViewedAt});

  MostActive.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    icon = json['icon'];
    id = json['id'];
    status = json['Status'];
    qualification = json['Qualification'];
    speciality = json['Speciality'];
    lastViewedAt = json['LastViewedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['icon'] = this.icon;
    data['id'] = this.id;
    data['Status'] = this.status;
    data['Qualification'] = this.qualification;
    data['Speciality'] = this.speciality;
    data['LastViewedAt'] = this.lastViewedAt;
    return data;
  }
}

class AllMembers {
  String title;
  String icon;
  String id;
  String status;
  String qualification;
  String speciality;
  String lastViewedAt;
  String profileId;

  AllMembers(
      {this.title,
        this.icon,
        this.id,
        this.status,
        this.qualification,
        this.speciality,
        this.lastViewedAt,
        this.profileId});

  AllMembers.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    icon = json['icon'];
    id = json['id'];
    status = json['Status'];
    qualification = json['Qualification'];
    speciality = json['Speciality'];
    lastViewedAt = json['LastViewedAt'];
    profileId = json['profileId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['icon'] = this.icon;
    data['id'] = this.id;
    data['Status'] = this.status;
    data['Qualification'] = this.qualification;
    data['Speciality'] = this.speciality;
    data['LastViewedAt'] = this.lastViewedAt;
    data['profileId'] = this.profileId;
    return data;
  }
}

