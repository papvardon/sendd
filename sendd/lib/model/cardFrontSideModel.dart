class CardFrontSideModel{
  String bg;
  ContentText contentText;

  CardFrontSideModel({this.bg, this.contentText});

  CardFrontSideModel.fromJson(Map<String, dynamic> json) {
    bg = json['bg'];
    contentText = json['content_text'] != null
        ? new ContentText.fromJson(json['content_text'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bg'] = this.bg;
    if (this.contentText != null) {
      data['content_text'] = this.contentText.toJson();
    }
    return data;
  }
}

class ContentText {
  String name;
  String mobile;
  String title2;
  String value2;
  String photo;

  ContentText({this.name, this.mobile, this.title2, this.value2, this.photo});

  ContentText.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    mobile = json['Mobile'];
    title2 = json['title2'];
    value2 = json['value2'];
    photo = json['Photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['Mobile'] = this.mobile;
    data['title2'] = this.title2;
    data['value2'] = this.value2;
    data['Photo'] = this.photo;
    return data;
  }
}