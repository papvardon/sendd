import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sendd/common/circularProgress.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/visitSummary.dart';

import 'model/completedAppointmentModel.dart';

class CompletedAppointmentList extends StatefulWidget {
  CompletedAppointmentList({Key key, this.mrn, this.clinicId, this.userId, this.token})
      : super(key: key);

  final mrn;
  final clinicId;
  final userId;
  final token;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CompletedState();
  }
}

class _CompletedState extends State<CompletedAppointmentList> {
  Future<CompletedAppointmentModel> model;

  @override
  void initState() {
    super.initState();
    model = Services.getCompletedAppointments(widget.mrn, widget.clinicId, widget.token);
  }

  void _onClick(Data data){
    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) =>
            VisitSummary(
              mrn: widget.mrn, clinicId: widget.clinicId, visitId: data.visitId, visitnumber: data.visitNumber, token: widget.token, userId: widget.userId,)));
  }

  Widget _myListView(List<Data> data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () => _onClick(data[index]),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Padding(
                  padding: const EdgeInsets.only(top:10.0, bottom: 10.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ClipOval(child: Image.asset("assets/images/avatar_patient.png", width: 50.0, height: 50.0,)),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 10.0,
                          ),
                          Text("Dr. ${data[index].doctorName}"),
                          Row(
                            children: <Widget>[
                              Text("Date"),
                              SizedBox(
                                width: 20.0,
                              ),
                              Text("${data[index].appointmentDate} - ${data[index].appointmentSlotTime}" ,
                                  style: TextStyle(fontWeight: FontWeight.bold))
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Text('Visit No'),
                              SizedBox(
                                width: 20.0,
                              ),
                              Text(data[index].visitNumber,
                                  style: TextStyle(fontWeight: FontWeight.bold))
                            ],
                          ),
                          SizedBox(
                            height: 7.0,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
          height: double.infinity,
          child: FutureBuilder<CompletedAppointmentModel>(
              future: model,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text('Error ${snapshot.error}');
                }
                if (snapshot.hasData) {
                  if (snapshot.data.status == 'true') {
                    return Stack(
                      children: <Widget>[
                        Container(
                          height: double.infinity,
                          color: Colors.white,
                        ),
                        _myListView(snapshot.data.data)
                      ],
                    );
                  } else {
                    return Container(
                      child: Center(
                        child: Text('Something went wrong!!!'),
                      ),
                    );
                  }
                }
                return CircularProgress();
              })),
    );
  }
}
