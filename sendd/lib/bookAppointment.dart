import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/common/circularProgress.dart';
import 'package:sendd/common/dialogs.dart';
import 'package:sendd/model/appointmentModel.dart';
import 'package:sendd/model/commonModel.dart';
import 'package:sendd/razorDummy.dart';
import 'package:sendd/utils/custColors.dart';

import 'common/bundleFormList.dart';
import 'data/services.dart';
import 'data/sharedpreferenceshelper.dart';
import 'model/bundleCommonModel.dart';

class BookAppointment extends StatefulWidget {
  BookAppointment({Key key, this.clinicId, this.mrn}) : super(key: key);

  final clinicId;
  final mrn;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BookAppointmentState();
  }
}

class _BookAppointmentState extends State<BookAppointment> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  BundleCommonModel cModel;
  AppointmentModel model;
  Future<BundleCommonModel> bModel;
  String amount;
  String appointmnetFee;


  @override
  void initState() {
    super.initState();
    bModel = Services.getPatientData(widget.clinicId, widget.mrn);
  }

  void onSaved(String value, int index){
    print(value);
    cModel.tabs[0].data[index].value = value;
  }


  Future<void> _submit() async {

      bool status = true;

      String userId = await SharedPreferencesHelper.getUserId();

      Map<String, dynamic> map = new Map();
      map['user_id'] = userId;
      map['clinic_id'] = widget.clinicId;
      for (int i = 0; i < cModel.tabs[0].data.length; i++) {
        if(cModel.tabs[0].data[i].category == "0018"){
          print("${cModel.tabs[0].data[i].value} and ${cModel.tabs[0].data[i].heading}");
          if(cModel.tabs[0].data[i].value.trim().isEmpty){
            status = false;
            _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text('Please provide the Valid details to this field ${cModel.tabs[0].data[i].heading}')));
            break;
          }
          else {
            map[cModel.tabs[0].data[i].parameter] = cModel.tabs[0].data[i].value;
          }
        }
        else {
          if(cModel.tabs[0].data[i].parameter == 'total_fee')
          {
            amount = cModel.tabs[0].data[i].value;
            print("my amnt $amount");
          }
          else if(cModel.tabs[0].data[i].parameter == 'appointment_fee'){
            appointmnetFee = cModel.tabs[0].data[i].value;
            print(appointmnetFee);
          }
          map[cModel.tabs[0].data[i].parameter] = cModel.tabs[0].data[i].value;
        }
      }

      if(status){
        Dialogs.showLoadingDialog(context, _keyLoader);
      String encodedJSON = jsonEncode(map);
      model = await Services.doAppointmentBooking(encodedJSON).catchError((error) {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        print(error);
      });
      if(model.status == 'true'){
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text(model.message)));
        Future.delayed(const Duration(milliseconds: 1000), () {
          setState(() {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => RazorDummy(amount: amount,
              appointmentId: model.appointmentno, clinicId: widget.clinicId, appointmentFee: appointmnetFee,)));
            //Navigator.pop(context);
          });
        });
      }
      else {
        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text(model.message)));
      }

      //
    }
  }

  Future <bool> validationPart() async{
    bool status;
    if(cModel.tabs.length>0) {
      for (int i = 0; i < cModel.tabs[0].data.length; i++) {
        if(cModel.tabs[0].data[i].category == "0018"){
          if(cModel.tabs[0].data[i].heading.trim().isEmpty){
            status = false;
            _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(cModel.tabs[0].data[i].heading)));
            break;
          }
          else
          {
            status = true;
          }
        }
      }
    }

    return status;
  }

  Widget _submitButton() {
    return InkWell(
      onTap: _submit,
      child: Container(
        width: 150.0,
        height: 50.0,
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: PrimaryColor,
          borderRadius: BorderRadius.all(Radius.circular(40)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.shade200,
                offset: Offset(2, 4),
                blurRadius: 5,
                spreadRadius: 2)
          ],
        ),
        child: Text(
          'Book',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text('Book Appointment'),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: FutureBuilder<BundleCommonModel>(
          future: bModel,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Text('Error ${snapshot.error}');
            }
            if(snapshot.hasData){
              cModel = snapshot.data;
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    //Text(cModel.tabs[0].heading, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 20.0),),
                    BundleFormList(context, cModel.tabs[0].data, onSaved),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: _submitButton(),
                      ),
                    )
                  ],
                ),
              );
            }
            return CircularProgress();
          }
      )
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
