import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendd/bookAppointment.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/doctorPatientChat.dart';
import 'package:sendd/model/createChannelIdModel.dart';
import 'package:sendd/utils/custColors.dart';
import 'package:sendd/viewAppointments.dart';

import 'common/circularProgress.dart';
import 'model/clinicSubMenuModel.dart';

class ClinicSubMenu extends StatefulWidget {
  ClinicSubMenu({Key key, this.mrn, this.clinicId, this.clinicName}) : super(key: key);

  final String mrn;
  final String clinicId;
  final clinicName;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ClinicSubState();
  }
}

class _ClinicSubState extends State<ClinicSubMenu> {
  Future<ClinicSubMenuModel> model;


  @override
  void initState() {
    super.initState();
    model = Services.getMyClinicsSubMenu(widget.mrn, widget.clinicId);
  }

  void _itemClick(Data item, String userId, String token){
    if(item.id == '1'){
      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => BookAppointment(mrn: widget.mrn, clinicId: widget.clinicId,)));
    }
    else if(item.id == '2'){
      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ViewAppointments(mrn: widget.mrn,
        clinicId: widget.clinicId, userId: userId, token: token,)));
    }
    else if(item.id == '3'){
      createChannelId(userId, token);
    }

  }

  Future<void> createChannelId(String userId, String token) async {
    CreateChannelIdModel cModel =
    await Services.getChannelID(widget.clinicId, userId, token);
    if (cModel.data.length > 0) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => DoctorPatientChat(
                clinicId: widget.clinicId,
                channelId: cModel.data[0].id,
                doctorName: cModel.data[0].title,
                userID: userId,
                token: token,
              )));
    }
  }

  Widget _clinicGrid(BuildContext context, String title) {
    return InkWell(
      child: Card(
        color: Colors.white,
        child: Padding(
          padding:
          EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0, top: 10.0),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              title,
              softWrap: true,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PrimaryColor,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(widget.clinicName),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: Container(
          height: double.infinity,
          color: Colors.white,
          child: FutureBuilder<ClinicSubMenuModel>(
              future: model,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text('Error ${snapshot.error}');
                }
                if (snapshot.hasData) {
                  if(snapshot.data.data.length>0){
                    return Padding(
                      padding: EdgeInsets.all(5.0),
                      child: GridView.count(
                        physics: ScrollPhysics(),
                        crossAxisCount: 2,
                        childAspectRatio: 1.0 / .5,
                        shrinkWrap: true,
                        mainAxisSpacing: 4.0,
                        crossAxisSpacing: 4.0,
                        children: snapshot.data.data.map(
                              (item) {
                            return GestureDetector(
                              child: GridTile(
                                child: _clinicGrid(context, item.title),
                              ),
                              onTap: () {
                                _itemClick(item, snapshot.data.userId, snapshot.data.token);
                              },
                            );
                          },
                        ).toList(),
                      ),
                    );
                  }
                  else{
                    return Center(child: Text('Something went wrong'),);
                  }
                }
                return CircularProgress();
              })),
    );
  }
}
