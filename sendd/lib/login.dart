import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sendd/dashboard.dart';
import 'package:sendd/data/databaseHelper.dart';
import 'package:sendd/data/services.dart';
import 'package:sendd/data/sharedpreferenceshelper.dart';
import 'package:sendd/forgotPassword.dart';
import 'package:sendd/signUp.dart';
import 'package:sendd/utils/constants.dart';
import 'package:sendd/utils/custColors.dart';
import 'package:url_launcher/url_launcher.dart';

import 'common/dialogs.dart';
import 'model/commonModel.dart';
import 'model/eventObject.dart';
import 'model/loginresponse.dart';

class Login extends StatefulWidget {
  Login({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _LoginState();
  }
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  StreamController<int> streamController = new StreamController<int>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  Loginresponse model;
  var db = DatabaseHelper();

  final _usernameController = TextEditingController();
  final _countryCodeController = TextEditingController(text: "91");
  final _passwordController = TextEditingController();


  @override
  void initState() {
    super.initState();
  }

  Future<void> _submit() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Dialogs.showLoadingDialog(context, _keyLoader);

        EventObject eventObject = await Services.doLogin(
            _countryCodeController.text+_usernameController.text, _passwordController.text)
            .catchError((error) {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          print(error);
        });

        if (this.mounted) {
          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
          switch (eventObject.id) {
            case Events.READ_LOGS_SUCCESSFUL:
              model = eventObject.object;
              navigatetoDashboard();
              break;
            case Events.NO_LOGS_FOUND:
              CommonModel md = eventObject.object;
              if (md.message == 'Please Verify OTP') {
                /*Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) =>
                        OtpVerification(phoneNmbr: userName,)));*/
              }
              else {
                _scaffoldKey.currentState.showSnackBar(
                    new SnackBar(content: new Text(md.message)));
              }
              break;
          }
        }

    }
  }

  Future<void> navigatetoDashboard() async {
    if (model.status == "true") {
      if (model.data.length > 0) {
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text(model.message)));
        await db.saveLogin(model.data[0]);
        await addDatatoSharedPref();
        Future.delayed(const Duration(milliseconds: 1000), () {
          setState(() {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (BuildContext context) => Dashboard()));
          });
        });
      }
      else {
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text('Something went wrong !!! Please try again later')));
      }
    }
    else {
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: new Text(model.message)));
    }
  }

  addDatatoSharedPref() async{

    await SharedPreferencesHelper.setUserId(model.data[0].id);
    await SharedPreferencesHelper.setDynamicUrl(model.data[0].IP);
    await SharedPreferencesHelper.setProfileId(model.profile_id);
    await SharedPreferencesHelper.setRoleType(model.data[0].roleType);
    await SharedPreferencesHelper.setToken(model.data[0].token);
    await SharedPreferencesHelper.setLogdinStatus('true');
    await SharedPreferencesHelper.setMobileNumber(_usernameController.text);

    String dynamicUrl = await SharedPreferencesHelper.getDynamicUrl();
    print(dynamicUrl);
    
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<bool> showPrivacy() {
    return showDialog(
      context: context,
      builder: (context) => myDialog()
    ) ??
        false;
  }

  Widget myDialog(){
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Container(
        height: 220,
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 15.0,),
                Align(alignment: Alignment.topCenter,child: Text('Welcome', style: TextStyle(fontSize: 20, color: PrimaryColor),)),
                SizedBox(height: 10.0,),
                Text('Please read and accept the ',
                    style: TextStyle(fontSize: 15, color: Colors.black)
                ),
                SizedBox(height: 10.0,),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: ' 1 . ',
                        style: TextStyle(fontSize: 15, color: Colors.black,
                            ),
                      ),
                      TextSpan(
                        text: 'Terms And Conditions',
                          style: TextStyle(fontSize: 15, color: PrimaryColor,
                              decoration: TextDecoration.underline),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              _launchURL('https://sendd.ai/termsconditions.html');
                            }
                      )
                    ]
                  ),
                ),
                RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                          text: ' 2 . ',
                          style: TextStyle(fontSize: 15, color: Colors.black,
                          ),
                        ),
                        TextSpan(
                            text: 'Privacy Policy',
                            style: TextStyle(fontSize: 15, color: PrimaryColor,
                                decoration: TextDecoration.underline),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                _launchURL('https://sendd.ai/privacypolicy.html');
                              }
                        )
                      ]
                  ),
                ),
                RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                          text: ' 3 . ',
                          style: TextStyle(fontSize: 15, color: Colors.black,
                          ),
                        ),
                        TextSpan(
                            text: 'End User License Agreement',
                            style: TextStyle(fontSize: 15, color: PrimaryColor,
                                decoration: TextDecoration.underline),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                _launchURL('https://sendd.ai/eula');
                              }
                        )
                      ]
                  ),
                ),
                SizedBox(height: 10.0,),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                          child: Text('Cancel', style: TextStyle(fontSize: 18.0, color: Colors.red),),
                      onTap: () => {
                            SharedPreferencesHelper.setTermsStatus('false'),
                            Navigator.of(context).pop(true),}),
                      SizedBox(width: 10.0,),
                      InkWell(
                          onTap: () => {
                             Navigator.of(context).pop(true),
                            SharedPreferencesHelper.setTermsStatus('true'),
                          },
                          child: Text('Accept', style: TextStyle(fontSize: 18.0, color: PrimaryColor),)),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _entryFieldPhoneNumber() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Mobile Number ",
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: TextFormField(
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter country code';
                        }
                        return null;
                      },
                      controller: _countryCodeController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        focusColor: PrimaryColor,
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)
                        ),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color : Colors.white)
                        ),
                      )),
                ),
                SizedBox(width: 10.0,),
                Expanded(
                  flex: 5,
                  child: TextFormField(
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter Mobile Number';
                        }
                        return null;
                      },
                      controller: _usernameController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        focusColor: PrimaryColor,
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)
                        ),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color : Colors.white)
                        ),
                      )),
                )
              ],
            )
          ],
        ),
      ),
    );
  }


  Widget _entryFieldPassword() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Password",
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
            ),
            TextFormField(
                style: TextStyle(
                  color: Colors.white,
                ),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Enter Password';
                  }
                  return null;
                },
              controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  focusColor: PrimaryColor,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white)
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color : Colors.white)
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _submitButton() {
    return InkWell(
      onTap: () async {
        String logdinStatus = await SharedPreferencesHelper.getTermsStatus();
        if(logdinStatus == null || logdinStatus == 'false'){
          showPrivacy();
        }
        else{
          _submit();
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width/3,
        height: MediaQuery.of(context).size.height/15,
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: PrimaryColor,
            borderRadius: BorderRadius.all(Radius.circular(40)),
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [buttonGradientEnd, buttonGradientStart])
            /*boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.blue.shade500,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],*/
            ),
        child: Text(
          'Sign In',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  circularProgress() {
    return Center(
      child: const CircularProgressIndicator(),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      body: new Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [loginGradient, loginBottomGradient])),
          ),
            Center(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top:30.0),
                        child: Image.asset("assets/images/ic_sendd_icon.png", height: MediaQuery.of(context).size.height/8,),
                      ),
                        SizedBox(height: 30.0,),
                        _entryFieldPhoneNumber(),
                        _entryFieldPassword(),
                        SizedBox(height: 30.0,),
                        _submitButton(),
                        SizedBox(height: 20.0,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("New to Sendd ?", style: TextStyle(color: PrimaryColor, fontSize: 15.0),),
                            SizedBox(width: 10.0,),
                            GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => SignUp()));
                              },
                              child: Text('Sign Up', style: TextStyle(color: PrimaryColor, decoration: TextDecoration.underline, fontSize: 15.0),)
                            )
                          ],
                        ),
                      SizedBox(height: 5.0,),
                      GestureDetector(
                          onTap: (){Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ForgotPassword()));},
                          child: Text('Forgot Password?', style: TextStyle(color: PrimaryColor, decoration: TextDecoration.underline),)
                      ),
                      SizedBox(height: 15.0,)
                    ],
                  ),
                ),
              ),
            ),
    
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    streamController.close();
    db.close();
  }
}

